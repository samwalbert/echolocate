# -*- coding: utf-8 -*-
"""
HW3_test.py:
    debugger script
Created on Mon Jan 31 20:07:55 2022

@author: Samuel Albert
"""

import numpy as np
import pandas as pd
from scipy.stats import norm
import matplotlib.pyplot as plt

from src import astrodynamics as astro
from src import Jacobians
from src import filters

randSeed = 42
USE_Q_RIC = False

obs = pd.read_pickle('data/HW2_J3_measurements.pkl') # generate via code adapted from HW1
with open('data/HW2_J3_xxTruth.pkl', 'rb') as f:
    xxTruth = np.load(f)

# constants 
mu = 3.986004415e5      # km^3/s^2
J2 = 0.0010826269
J3 = 0
R = 6378.1363           # km
constParams = {
    'mu'    : mu,
    'J2'    : J2,
    'J3'    : J3,
    'R'     : R
    }

# define truth orbit
SMA = 10e3              # km
ECC = 0.001
INC = 40                # deg
AOP = 80                # deg
RAAN = 40               # deg
TA0 = 0                 # deg
Period = 2 * np.pi * np.sqrt(SMA**3/mu)

# convert initial state to inertial cartesian state
[r0vec, v0vec] = astro.kep2cart_TA(SMA, ECC, INC, AOP, RAAN, TA0, mu)
xx0 = np.append(r0vec, v0vec)

# initial conditions for filter
t0 = 0
sig0list = np.array([1, 1, 1, 1e-3, 1e-3, 1e-3])
sig0list = np.append(sig0list, np.array([1e-9, 1e-9, 1e-9]))
P0 = np.diag(sig0list)**2 # km^2, (km/s)^2
R0 = np.diag([1e-3, 1e-6])**2 # km^2, (km/s)^2

# add a perturbation to the true xx0 to make the filter work for it
delxx0 = np.zeros(6)
for i in range(6):
    sig0 = sig0list[i]
    delxx0[i] = norm.rvs(loc = 0, scale = sig0, size = 1,
                         random_state = randSeed + i)[0]
# delxx0 = np.array([0.25, -0.25, 0.5, -7e-4, -5e-4, 5e-4])
# delxx0 = np.array([1.6095e-01,
#   -6.1512e-01,
#   -2.3899e-01,
#     6.1498e-04,
#   -9.0140e-04,
#     3.4808e-04])

xx0hat = xx0 + delxx0
xx0hat = np.append(xx0hat, np.zeros(3))

ICParams_CKF = {
    't0'    : t0,
    'xx0'   : xx0hat,
    'P0'    : P0,
    'R0'    : R0,
    'tau'   : Period/30
    }

ICParams_EKF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0,
    'tau'   : Period/30
    }

# process noise variance
sig2SNC = (5e-10)**2 # km^2/s^4
# sig2SNC = 0

# generate Jacobians and Q matrix eval functions
Asym, evalA = Jacobians.getdfdZ()
_, evalH = Jacobians.getHtilde_sc_rho_rhod()
if USE_Q_RIC:
    evalQ = filters.getQ_HW3_RIC()
    Qct = sig2SNC * np.eye(3)
else:
    evalQ = filters.getQ_HW3_inertial()
    Qct = sig2SNC * np.eye(3)

# define plotting function
def plots(thist, xxHat, xxTruth, Phist, rhist):
    xxErr = xxHat - xxTruth
    STDhist = np.empty(xxErr.shape)
    for i in range(xxErr.shape[0]):
        STDhist[i,:] = np.sqrt(np.diag(Phist[i,:,:]))

    fig, ax = plt.subplots(3,1,sharex = True, sharey = True)
    fig.suptitle('Position State Errors')
    for i in range(3):
        ax[i].plot(thist/3600, xxErr[:,i], marker = '.', linestyle = '-', label = 'err')
        ax[i].plot(thist/3600, 3*STDhist[:,i], 'C1--', label = r'$\pm 3\sigma')
        ax[i].plot(thist/3600, -3*STDhist[:,i], 'C1--')
        ax[i].grid()
        if i < 2:
            ax[i].tick_params('x', labelbottom=False)
    ax[0].set_ylabel(r'$\Delta x$, km')
    ax[1].set_ylabel(r'$\Delta y$, km')
    ax[2].set_ylabel(r'$\Delta z$, km')
    ax[2].set_xlabel('time, hours')
    plt.tight_layout()
    
    fig, ax = plt.subplots(3,1,sharex = True, sharey = True)
    fig.suptitle('Velocity State Errors')
    for i in range(3):
        ax[i].plot(thist/3600, xxErr[:,i+3], marker = '.', linestyle = '-', label = 'err')
        ax[i].plot(thist/3600, 3*STDhist[:,i+3], 'C1--', label = r'$\pm 3\sigma')
        ax[i].plot(thist/3600, -3*STDhist[:,i+3], 'C1--')
        ax[i].grid()
        if i < 2:
            ax[i].tick_params('x', labelbottom=False)
    ax[0].set_ylabel(r'$\Delta v_x$, km/s')
    ax[1].set_ylabel(r'$\Delta v_y$, km/s')
    ax[2].set_ylabel(r'$\Delta v_z$, km/s')
    ax[2].set_xlabel('time, hours')
    plt.tight_layout()
    
    fig, ax = plt.subplots(2,1, sharex = True)
    fig.suptitle('Post-Update Measurement Residuals')
    for i in range(2):
        ax[i].plot(thist/3600, rhist[:,i], '+')
        ax[i].grid()

    ax[0].set_ylabel('range residuals, km')
    ax[0].tick_params('x', labelbottom=False)
    ax[1].set_ylabel('range rate residuals, km/s')
    ax[1].set_xlabel('time, hr')
    plt.tight_layout()

#%%
# =============================================================================
# Run CKF
# =============================================================================
# thist, xxHat, Phist, rhist, RMSList = filters.CKF_HW3_DMC(obs.copy(),
#                                                           xxTruth,
#                                                           ICParams_CKF,
#                                                           constParams,
#                                                           Qct,
#                                                           evalA = evalA,
#                                                           evalH = evalH,
#                                                           maxits = 1)

# %%
# =============================================================================
# Run EKF
# =============================================================================
thist, xxHat, Phist, rhist, RMSList = filters.EKF_HW3_DMC(obs.copy(),
                                                          xxTruth,
                                                          ICParams_EKF,
                                                          constParams,
                                                          Qct,
                                                          evalA = evalA,
                                                          evalH = evalH,
                                                          NCKF = 0)

#%%
# =============================================================================
# Run NLBF
# =============================================================================
# thist, xxHat, Phist, rhist = filters.NLBF_J2J3(obs.copy(), xxTruth, ICParams_NLBF,
#                                                         constParams, coords,
#                                                         evalA = evalA,
#                                                         evalH = evalH,
#                                                         tol = 1e-7, maxits = 15)

#%%
# =============================================================================
# Plots
# =============================================================================
plt.close('all')
plots(thist, xxHat[:,:6], xxTruth, Phist[:,:6,:6], rhist)