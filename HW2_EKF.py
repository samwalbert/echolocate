# -*- coding: utf-8 -*-
"""
HW2_EKF.py:
    extended Kalman filter script for Stat OD HW2
Created on Fri Jan 28 17:19:45 2022

@author: Samuel Albert
"""

import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

from src import dynamics as dyn
from src import measurements as mes
from src import astrodynamics as astro
from src import Jacobians

tic = time.time()

# =============================================================================
# Options
# =============================================================================
NOISEON = True
INITIALDELTAX = True

#%%
# =============================================================================
# Load measurement data
# =============================================================================
obs = pd.read_pickle('data/HW2_measurements.pkl')

#%%
# =============================================================================
# Set up ground station data for measurement predictions
# =============================================================================
# define ground stations
Nstations = 3 # number of stations
coords = np.empty((Nstations,2))
coords[:] = np.NaN
masks = np.empty(Nstations)
masks[:] = np.NaN
IDs = [None]*Nstations

# Station 1
coords[0,0] = 148.981944
coords[0,1] = -35.398333
masks[0] = 10
IDs[0] = 'Canberra'

# Station 2
coords[1,0] = 355.749444
coords[1,1] = 40.427222
masks[1] = 10
IDs[1] = 'Madrid'

# Station 3
coords[2,0] = 243.205
coords[2,1] = 35.247164
masks[2] = 10
IDs[2] = 'Goldstone'

#%%
# =============================================================================
# Initialize filter
# =============================================================================
# initialize xx0vec to truth
# use true dynamics to propagate xxvec

# use truth orbit to generate initial state
# constants 
mu = 3.986004415e5      # km^3/s^2
J2 = 0.0010826269
J3 = 0
R = 6378.1363           # km
D = 0.99726968 * 86400  # s
theta0 = 122            # deg, rotation of central body at time t = 0
constParams = {
    'mu'    : mu,
    'J2'    : J2,
    'J3'    : J3,
    'R'     : R,
    'D'     : D,
    'theta0': theta0
    }

# define truth orbit
SMA = 10e3              # km
ECC = 0.001
INC = 40                # deg
AOP = 80                # deg
RAAN = 40               # deg
TA0 = 0                 # deg
Period = 2 * np.pi * np.sqrt(SMA**3/mu)

# convert initial state to inertial cartesian state
[r0vec, v0vec] = astro.kep2cart_TA(SMA, ECC, INC, AOP, RAAN, TA0, mu)
xx0 = np.append(r0vec, v0vec)

t0 = 0

P0 = np.diag([1, 1, 1, 1e-3, 1e-3, 1e-3])**2 # km^2, (km/s)^2, given values

if INITIALDELTAX:
    delxx0 = np.array([0.5, 1.5, -1, 1e-3, -1e-3, -1.5e-3]) # within above cov.
else:
    delxx0 = np.zeros(6)
xx0hat = xx0 + delxx0

R0 = np.diag([1e-3, 1e-6])**2 # km^2, (km/s)^2

ICParams = {
    't0'    : t0,
    'xx0'   : xx0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

Nobs = len(obs)
Nstate = len(xx0)
Ny = 2 # range, range rate

thist = np.empty(Nobs)
thist[:] = np.NaN
thist[0] = t0

xxhathistP = np.empty((Nobs, Nstate))
xxhathistP[:] = np.NaN
xxhathistP[0,:] = xx0hat

xxhathistM = np.empty((Nobs, Nstate))
xxhathistM[:] = np.NaN

PhistP = np.empty((Nobs, Nstate, Nstate))
PhistP[:] = np.NaN
PhistP[0,:,:] = P0

PhistM = np.empty((Nobs, Nstate, Nstate))
PhistM[:] = np.NaN

yhist = np.empty((Nobs, Ny))
yhist[:] = np.NaN # note, yhist[0] stays equal to NaN

Rhist = np.empty((Nobs, Ny, Ny))
Rhist[:] = np.NaN
Rhist[0,:,:] = R0

rMhist = np.empty((Nobs, Ny))
rMhist[:] = np.NaN

rPhist = np.empty((Nobs, Ny))
rPhist[:] = np.NaN

Khist = np.empty((Nobs, Nstate, Ny))
Khist[:] = np.NaN

#%%
# =============================================================================
# Generate necessary jacobian function handles
# =============================================================================
_, evalA = Jacobians.getdfdZ()
_, evalH = Jacobians.getHtilde_sc_rho_rhod()

#%%
# =============================================================================
# # ===========================================================================
# # Minor iterations, i
# # ===========================================================================
# =============================================================================
# NOTE TO SELF: index "i" corresponds to the update based on measurement with
# index "i-1", such that index i = 0 corresponds to initialization, i = 1
# is the result of the first measurement update.

for i in range(1,len(obs)):
    # print(i)
    # =========================================================================
    # Read next observation
    # =========================================================================
    thist[i] = obs['t'][i-1]
    if NOISEON:
        yhist[i,0] = obs['noisyrange'][i-1]
        yhist[i,1] = obs['noisyrate'][i-1]
    else:
        yhist[i,0] = obs['range'][i-1]
        yhist[i,1] = obs['rangerate'][i-1]
    Rhist[i,:,:] = R0
    
    rvecS = np.array([obs['x'][i-1], obs['y'][i-1], obs['z'][i-1]])
    vvecS = np.array([obs['vx'][i-1], obs['vy'][i-1], obs['vz'][i-1]])
    
    # =========================================================================
    # Propagate and time update from t[i-1] to t[i] (or equate)
    # =========================================================================
    if (thist[i] == thist[i-1]):
        # don't update xxhat or PM
        print('skipping propagate step at index {0:d}'.format(i))
        xxhathistM[i,:] = xxhathistP[i-1,:]
        PhistM[i,:,:] = PhistP[i-1,:,:]
    else:
        # numerically propagate nonlinear state, STM
        IC = np.append(xxhathistP[i-1,:],
                       np.reshape(np.eye(Nstate), (Nstate**2,)))
        
        sol = solve_ivp(lambda t, yy: dyn.keplerJ2J3_wPhi_ODErv(t, yy, mu, 
                                                                J2, J3, R, 
                                                                evalA),
                        (thist[i-1], thist[i]), IC,
                        rtol = 1e-12, atol = 1e-12)
        xxhathistM[i,:] = sol.y[:6,-1]
        Phi = np.reshape(sol.y[6:,-1], (Nstate,Nstate))
        
        # linearly update covariance
        PhistM[i,:,:] = Phi @ PhistP[i-1,:,:] @ Phi.T # + process noise
        
    # =========================================================================
    # Process observations
    # =========================================================================
    # predict range and range rate measurements for curent state, pre-update
    indObs = obs['ind'][i-1]
    rho, rhodot = mes.compute_range_rangerate_single(xxhathistM[i,:],
                                                     coords[indObs],
                                                     0, theta0, thist[i], R, D)
    yPredM = np.array([rho, rhodot])
    
    # compute measurement sentivity matrix at this time
    Hi = evalH(xxhathistM[i,:3], xxhathistM[i,3:], rvecS, vvecS)
    
    # compute pre-update residual
    rMhist[i,:] = yhist[i,:] - yPredM
    
    # compute Kalman gain matrix
    Khist[i,:,:] = PhistM[i,:,:] @ Hi.T @ np.linalg.inv((Hi @ PhistM[i,:,:] @ Hi.T + Rhist[i,:,:]))
    
    # =========================================================================
    # Measurement update
    # =========================================================================
    # update estimate of state
    xxhathistP[i,:] = xxhathistM[i,:] + Khist[i,:,:] @ rMhist[i,:]
    
    # update covariance using Jordan-Busy form
    PhistP[i,:,:] = (np.eye(Nstate) - Khist[i,:,:] @ Hi)\
        @ PhistM[i,:,:] @ (np.eye(Nstate) - Khist[i,:,:] @ Hi).T\
            + Khist[i,:,:] @ Rhist[i,:,:] @ Khist[i,:,:].T
    
    # predict range and range rate measurements for curent state, post-update
    rho, rhodot = mes.compute_range_rangerate_single(xxhathistP[i,:],
                                                     coords[indObs],
                                                     0, theta0, thist[i], R, D)
    yPredP = np.array([rho, rhodot])
    
    # compute post-update residual
    rPhist[i,:] = yhist[i,:] - yPredP
            
#%%
# =============================================================================
# Generate truth data at desired timesteps
# =============================================================================
sol = solve_ivp(lambda t, yy: dyn.keplerJ2_ODErv(t, yy, mu, J2, R),
                (t0, thist[-1]), xx0, t_eval = np.unique(thist),
                rtol = 1e-12, atol = 1e-12)
# pad with duplicate time values
xxTruth = np.empty((Nobs, Nstate))
xxTruth[0,:] = sol.y[:,0]
j = 0
for i in range(1, Nobs):
    if (thist[i] == thist[i-1]):
        print('shifted time index at i = {0:d}'.format(i))
    else:
        j += 1
    xxTruth[i,:] = sol.y[:,j]

#%%
# =============================================================================
# Compute difference between truth and estimated
# =============================================================================
totalErr = np.empty((Nobs, Nstate))
for i in range(Nobs):
    totalErr[i,:] = xxTruth[i,:] - xxhathistP[i,:]

#%%
# =============================================================================
# Plots
# =============================================================================
plt.close('all')

fig1, ax1 = plt.subplots(2,1)
fig1.suptitle('measurement residuals')
for i in range(2):
    # ax1[i].plot(thist, rMhist[:,i], marker = '.', linestyle = '-', label = 'pre-update')
    ax1[i].plot(thist, rPhist[:,i], marker = '.', linestyle = '-', label = 'post-update')
    ax1[i].grid()
    # ax1[i].legend()
ax1[0].set_ylabel('range residuals, km')
ax1[1].set_ylabel('range rate residuals, km/s')
ax1[1].set_xlabel('time, s')
plt.tight_layout()

fig2, ax2 = plt.subplots(2,1)
fig2.suptitle('state error')
ax2[0].plot(thist, np.linalg.norm(totalErr[:,:3], axis = 1), marker = '.', linestyle = '-')
ax2[0].set_ylabel('total position error, km')
ax2[0].grid()
# ax2[0].set_ylabel('')
ax2[1].plot(thist, np.linalg.norm(totalErr[:,3:], axis = 1), marker = '.', linestyle = '-')
ax2[1].set_ylabel('total velocity error, km/s')
ax2[1].set_xlabel('time, s')
ax2[1].grid()

# fig3, ax3 = plt.subplots(3, 1)
# for i in range(3):
#     ax3[i].plot(thist, DELxxhistP[:,i], marker = '.', linestyle = '-')
#     ax3[i].grid()
    
# fig4, ax4 = plt.subplots(3, 1)
# for i in range(3):
#     ax4[i].plot(thist, DELxxhistP[:,i+3], marker = '.', linestyle = '-')
#     ax4[i].grid()




#%%
toc = time.time()
print('script took {0:.3f} seconds to execute'.format(toc-tic))





