# -*- coding: utf-8 -*-
"""
HW2_genData.py:
    script to generate measurement data for Stat OD HW 2
Created on Fri Jan 28 15:51:50 2022

@author: Samuel Albert
"""

import numpy as np
import pandas as pd
from scipy.stats import norm
from scipy.integrate import solve_ivp
import matplotlib.pyplot as plt

from src import Jacobians
from src import dynamics as dyn
from src import measurements as mes
from src import astrodynamics as astro

USEJ3 = False

#%%
# =============================================================================
# Generate necessary Jacobian function handle
# =============================================================================
_, evalA = Jacobians.getdfdZ()

#%%
# =============================================================================
# Generate measurements
# =============================================================================
# constants 
mu = 3.986004415e5      # km^3/s^2
J2 = 0.0010826269
if USEJ3:
    J3 = -0.0000025323
else:
    J3 = 0
R = 6378.1363           # km
D = 0.99726968 * 86400  # s
theta0 = 122            # deg, rotation of central body at time t = 0

# define truth orbit
SMA = 10e3              # km
ECC = 0.001
INC = 40                # deg
AOP = 80                # deg
RAAN = 40               # deg
TA0 = 0                 # deg
P = 2 * np.pi * np.sqrt(SMA**3/mu)

# convert initial state to inertial cartesian state
[r0vec, v0vec] = astro.kep2cart_TA(SMA, ECC, INC, AOP, RAAN, TA0, mu)

# define ground stations
n = 3 # number of stations
coords = np.empty((n,2))
coords[:] = np.NaN
masks = np.empty(n)
masks[:] = np.NaN
IDs = [None]*n

# Station 1
coords[0,0] = 148.981944
coords[0,1] = -35.398333
masks[0] = 10
IDs[0] = 'Canberra'

# Station 2
coords[1,0] = 355.749444
coords[1,1] = 40.427222
masks[1] = 10
IDs[1] = 'Madrid'

# Station 3
coords[2,0] = 243.205
coords[2,1] = 35.247164
masks[2] = 10
IDs[2] = 'Goldstone'

# simulate truth orbit
t0 = 0
tf = P * 15 + 10
t_eval = np.arange(0, tf, 10)
xx0vec = np.append(r0vec, v0vec)
xx0vec = np.append(xx0vec, np.reshape(np.eye(6), (36,)))

sol = solve_ivp(lambda t, yy: dyn.keplerJ2J3_wPhi_ODErv(t, yy, mu, J2, J3, R, evalA),
                (t0, tf), xx0vec, t_eval = t_eval,
                rtol = 1e-12, atol = 1e-12)
print(sol.message)
xxTruth_OG = sol.y[:6,:].T # unmodified truth trajectory

# produce raw observations and save truth trajectory with every time step
obsList = []
IDs = ['Canberra', 'Madrid', 'Goldstone']
xxTruth = np.empty((1, 6)) # truth traj at obs times
xxTruth[0,:] = xxTruth_OG[0,:]
for i, t in enumerate(sol.t):
    Nobsi = mes.compute_range_rangerate(sol.y[:6,i], coords, masks, theta0,
                                        t, R, D, obsList, IDs)
    for k in range(Nobsi):
        xxTruth = np.append(xxTruth, xxTruth_OG[i,:][None,:], axis = 0)
obsData = pd.DataFrame(obsList)

# add noise to measurements
obsData['rnoise'] = norm.rvs(loc = 0, scale = 1e-3, size = len(obsData)) # km
obsData['ranoise'] = norm.rvs(loc = 0, scale = 1e-6, size = len(obsData)) # km/s
obsData['noisyrange'] = obsData['range'] + obsData['rnoise']
obsData['noisyrate'] = obsData['rangerate'] + obsData['ranoise']

#%%
# =============================================================================
# Save to file (note, csv is more readable but less precise)
# =============================================================================
if USEJ3:
    filename = 'data/HW2_J3'
else:
    filename = 'data/HW2'
obsData.to_csv(filename + '_measurements.csv', index = False)
obsData.to_pickle(filename + '_measurements.pkl')

with open(filename + '_xxTruth.pkl', 'wb') as f:
    np.save(f, xxTruth)

# # test reading from file
# df2 = pd.read_csv('data/HW2_measurements.csv')
# df3 = pd.read_pickle(filename + '_measurements.pkl')
# with open(filename + '_xxTruth.pkl', 'rb') as f:
#     xxTruth_read = np.load(f)
    




#%%
# =============================================================================
# Plots
# =============================================================================
plt.close('all')
fig1 = plt.figure()
ax1 = plt.subplot(111)
ax1.plot(obsData[obsData['ind'] == 0]['t']/P,
         obsData[obsData['ind'] == 0]['range'], '.', label = 'Canberra') 
ax1.plot(obsData[obsData['ind'] == 1]['t']/P,
         obsData[obsData['ind'] == 1]['range'], '.', label = 'Madrid')
ax1.plot(obsData[obsData['ind'] == 2]['t']/P,
         obsData[obsData['ind'] == 2]['range'], '.', label = 'Goldstone')
ax1.grid()
ax1.legend()
ax1.set_xlabel('# orbits')
ax1.set_ylabel('range, km')

fig2 = plt.figure()
ax2 = plt.subplot(111)
ax2.plot(obsData[obsData['ind'] == 0]['t']/P,
         obsData[obsData['ind'] == 0]['rangerate'], '.', label = 'Canberra') 
ax2.plot(obsData[obsData['ind'] == 1]['t']/P,
         obsData[obsData['ind'] == 1]['rangerate'], '.', label = 'Madrid')
ax2.plot(obsData[obsData['ind'] == 2]['t']/P,
         obsData[obsData['ind'] == 2]['rangerate'], '.', label = 'Goldstone')
ax2.grid()
ax2.legend()
ax2.set_xlabel('# orbits')
ax2.set_ylabel('range-rate, km/s')

fig3 = plt.figure()
ax3 = plt.subplot(111)
ax3.plot(obsData[obsData['ind'] == 0]['t']/P,
         obsData[obsData['ind'] == 0]['EL'], '.', label = 'Canberra') 
ax3.plot(obsData[obsData['ind'] == 1]['t']/P,
         obsData[obsData['ind'] == 1]['EL'], '.', label = 'Madrid')
ax3.plot(obsData[obsData['ind'] == 2]['t']/P,
         obsData[obsData['ind'] == 2]['EL'], '.', label = 'Goldstone')
ax3.grid()
ax3.legend()
ax3.set_xlabel('# orbits')
ax3.set_ylabel('EL, deg')

fig4, ax4 = plt.subplots()
ax4.plot(sol.y[0,:], sol.y[1,:], '-.')
ax4.grid()
























