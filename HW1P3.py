# -*- coding: utf-8 -*-
"""
HW1P3.py:
    script for Stat OD HW 1 P 3
Created on Mon Jan 24 15:48:22 2022

@author: Samuel Albert
"""

import time
import numpy as np
import sympy as sp
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

from src import Jacobians
from src import astrodynamics as astro
from src import dynamics as dyn

tic = time.time()

# =============================================================================
# Given inputs
# =============================================================================
rvec = np.array([-3483.819243, 8315.124271, 4090.441521])
vvec = np.array([-4.39707389, -3.386925829, 3.140027421])
rvecS = np.array([-2260.575839, 4296.74913, 4136.015932])
vvecS = np.array([-0.313323925, -0.164843809, 0])

# =============================================================================
# Compute Htilde matrix w.r.t. spacecraft
# =============================================================================
Jacsym, evalJac = Jacobians.getHtilde_sc_rho_rhod()
Htilde = evalJac(rvec, vvec, rvecS, vvecS)

# # print symbolic expressions
# for i, row in enumerate(Jacsym):
#     for j, dd in enumerate(row):
#         print('newline')
#         print(sp.latex(dd))

# =============================================================================
# # Compute Htilde matrix w.r.t. observer
# # =============================================================================
# # NOTE: we don't need the partials w.r.t. Vs, but compute them anyway
# JacsymObs, evalJacObs = Jacobians.getHtilde_obs_rho_rhod()
# HtildeObs = evalJacObs(rvec, vvec, rvecS, vvecS)

# # print symbolic expressions
# for i, row in enumerate(JacsymObs):
#     for j, dd in enumerate(row):
#         print('newline')
#         print(sp.latex(dd))








