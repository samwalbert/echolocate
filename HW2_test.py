# -*- coding: utf-8 -*-
"""
HW2_test.py:
    debugger script
Created on Mon Jan 31 20:07:55 2022

@author: Samuel Albert
"""

import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from IPython.display import display
from scipy.integrate import solve_ivp

from src import dynamics as dyn
from src import measurements as mes
from src import astrodynamics as astro
from src import Jacobians
from src import filters

obs = pd.read_pickle('data/HW2_measurements.pkl') # generate via code adapted from HW1
with open('data/HW2_xxTruth.pkl', 'rb') as f:
    xxTruth = np.load(f)

# define ground stations
Nstations = 3 # number of stations
coords = np.empty((Nstations,2))
coords[:] = np.NaN
masks = np.empty(Nstations)
masks[:] = np.NaN
IDs = [None]*Nstations

# Station 1
coords[0,0] = 148.981944
coords[0,1] = -35.398333
masks[0] = 10
IDs[0] = 'Canberra'

# Station 2
coords[1,0] = 355.749444
coords[1,1] = 40.427222
masks[1] = 10
IDs[1] = 'Madrid'

# Station 3
coords[2,0] = 243.205
coords[2,1] = 35.247164
masks[2] = 10
IDs[2] = 'Goldstone'

# constants 
mu = 3.986004415e5      # km^3/s^2
J2 = 0.0010826269
J3 = 0
R = 6378.1363           # km
D = 0.99726968 * 86400  # s
theta0 = 122            # deg, rotation of central body at time t = 0
constParams = {
    'mu'    : mu,
    'J2'    : J2,
    'J3'    : J3,
    'R'     : R,
    'D'     : D,
    'theta0': theta0
    }

# define truth orbit
SMA = 10e3              # km
ECC = 0.001
INC = 40                # deg
AOP = 80                # deg
RAAN = 40               # deg
TA0 = 0                 # deg
Period = 2 * np.pi * np.sqrt(SMA**3/mu)

# convert initial state to inertial cartesian state
[r0vec, v0vec] = astro.kep2cart_TA(SMA, ECC, INC, AOP, RAAN, TA0, mu)
xx0 = np.append(r0vec, v0vec)

t0 = 0
P0 = np.diag([1, 1, 1, 1e-3, 1e-3, 1e-3])**2 # km^2, (km/s)^2
R0 = np.diag([1e-3, 1e-6])**2 # km^2, (km/s)^2

delxx0 = np.array([0.25, -0.25, 0.5, -1e-3, -5e-4, 5e-4])
DELxx0 = delxx0 # initialize estimate of deviation
DELxxM = DELxx0
xx0hat = xx0 + delxx0

ICParams_CKF = {
    't0'    : t0,
    'xx0'   : xx0 + delxx0,
    'P0'    : P0,
    'R0'    : R0
    }

ICParams_EKF = {
    't0'    : t0,
    'xx0'   : xx0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

ICParams_NLBF = {
    't0'    : t0,
    'xx0'   : xx0,
    'xx0hat': xx0hat,
    'DELxxM': DELxxM,
    'P0'    : P0,
    'R0'    : R0
    }

_, evalA = Jacobians.getdfdZ()
_, evalH = Jacobians.getHtilde_sc_rho_rhod()

def plots(thist, xxHat, xxTruth, Phist, rhist, ylim = 1.5e-3):
    xxErr = xxHat - xxTruth
    STDhist = np.empty(xxErr.shape)
    for i in range(xxErr.shape[0]):
        STDhist[i,:] = np.sqrt(np.diag(Phist[i,:,:]))

    fig, ax = plt.subplots(3,1,sharex = True, sharey = True)
    fig.suptitle('Position State Errors')
    for i in range(3):
        ax[i].plot(thist/3600, xxErr[:,i], marker = '.', linestyle = '-', label = 'err')
        ax[i].plot(thist/3600, 3*STDhist[:,i], 'C1--', label = r'$\pm 3\sigma')
        ax[i].plot(thist/3600, -3*STDhist[:,i], 'C1--')
        ax[i].grid()
        if i < 2:
            ax[i].tick_params('x', labelbottom=False)
    ax[0].set_ylabel(r'$\Delta x$, km')
    ax[1].set_ylabel(r'$\Delta y$, km')
    ax[2].set_ylabel(r'$\Delta z$, km')
    ax[2].set_xlabel('time, hours')
    plt.tight_layout()

    # plot zoomed-in version
    fig, ax = plt.subplots(3,1,sharex = True, sharey = True)
    ax[0].set_ylim(-ylim,ylim)
    fig.suptitle('Position State Errors (zoomed-in)')
    for i in range(3):
        ax[i].plot(thist/3600, xxErr[:,i], marker = '.', linestyle = '-', label = 'err')
        ax[i].plot(thist/3600, 3*STDhist[:,i], 'C1--', label = r'$\pm 3\sigma')
        ax[i].plot(thist/3600, -3*STDhist[:,i], 'C1--')
        ax[i].grid()
        if i < 2:
            ax[i].tick_params('x', labelbottom=False)
    ax[0].set_ylabel(r'$\Delta x$, km')
    ax[1].set_ylabel(r'$\Delta y$, km')
    ax[2].set_ylabel(r'$\Delta z$, km')
    ax[2].set_xlabel('time, hours')
    plt.tight_layout()
    
    fig, ax = plt.subplots(3,1,sharex = True, sharey = True)
    fig.suptitle('Velocity State Errors')
    for i in range(3):
        ax[i].plot(thist/3600, xxErr[:,i+3], marker = '.', linestyle = '-', label = 'err')
        ax[i].plot(thist/3600, 3*STDhist[:,i+3], 'C1--', label = r'$\pm 3\sigma')
        ax[i].plot(thist/3600, -3*STDhist[:,i+3], 'C1--')
        ax[i].grid()
        if i < 2:
            ax[i].tick_params('x', labelbottom=False)
    ax[0].set_ylabel(r'$\Delta v_x$, km/s')
    ax[1].set_ylabel(r'$\Delta v_y$, km/s')
    ax[2].set_ylabel(r'$\Delta v_z$, km/s')
    ax[2].set_xlabel('time, hours')
    plt.tight_layout()

    # plot zoomed-in version
    fig, ax = plt.subplots(3,1,sharex = True, sharey = True)
    ax[0].set_ylim(-ylim,ylim)
    fig.suptitle('Velocity State Errors (zoomed-in)')
    for i in range(3):
        ax[i].plot(thist/3600, xxErr[:,i+3], marker = '.', linestyle = '-', label = 'err')
        ax[i].plot(thist/3600, 3*STDhist[:,i+3], 'C1--', label = r'$\pm 3\sigma')
        ax[i].plot(thist/3600, -3*STDhist[:,i+3], 'C1--')
        ax[i].grid()
        if i < 2:
            ax[i].tick_params('x', labelbottom=False)
    ax[0].set_ylabel(r'$\Delta v_x$, km/s')
    ax[1].set_ylabel(r'$\Delta v_y$, km/s')
    ax[2].set_ylabel(r'$\Delta v_z$, km/s')
    ax[2].set_xlabel('time, hours')
    plt.tight_layout()
    
    fig, ax = plt.subplots(2,1, sharex = True)
    fig.suptitle('Post-Update Measurement Residuals')
    for i in range(2):
        ax[i].plot(thist/3600, rhist[:,i], marker = '.', linestyle = '-')
        ax[i].grid()

    ax[0].set_ylabel('range residuals, km')
    ax[0].tick_params('x', labelbottom=False)
    ax[1].set_ylabel('range rate residuals, km/s')
    ax[1].set_xlabel('time, hr')
    plt.tight_layout()

#%%
# =============================================================================
# Run CKF
# =============================================================================
thist, xxHat, Phist, rhist = filters.CKF_J2J3(obs.copy(), xxTruth, ICParams_CKF,
                                                        constParams,
                                                        evalA = evalA,
                                                        evalH = evalH, maxits = 1)

# # %%
# # =============================================================================
# # Run EKF
# # =============================================================================
# thist, xxHat, Phist, rhist = filters.EKF_J2J3(obs.copy(), xxTruth, ICParams_EKF,
#                                                         constParams, coords,
#                                                         evalA = evalA,
#                                                         evalH = evalH, NCKF = 0)

# #%%
# # =============================================================================
# # Run NLBF
# # =============================================================================
# thist, xxHat, Phist, rhist = filters.NLBF_J2J3(obs.copy(), xxTruth, ICParams_NLBF,
#                                                         constParams, coords,
#                                                         evalA = evalA,
#                                                         evalH = evalH,
#                                                         tol = 1e-7, maxits = 15)

#%%
# =============================================================================
# Plots
# =============================================================================
plt.close('all')
plots(thist, xxHat, xxTruth, Phist, rhist)