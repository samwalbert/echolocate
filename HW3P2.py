# -*- coding: utf-8 -*-
"""
HW3P1.py:
    script for problem 1 of HW 3 for Stat OD
Created on Sun Feb 20 15:57:42 2022

@author: Samuel Albert
"""

import numpy as np
import pandas as pd
from scipy.stats import norm
import matplotlib.pyplot as plt

from src import astrodynamics as astro
from src import Jacobians
from src import filters

plt.close('all')

# random seed
randSeed = 42

# =============================================================================
# Load measurement data and define initial values
# =============================================================================
# load measurements and truth trajectory
obs = pd.read_pickle('data/HW2_J3_measurements.pkl') # generate via code adapted from HW1
with open('data/HW2_J3_xxTruth.pkl', 'rb') as f:
    xxTruth = np.load(f)

# constants 
mu = 3.986004415e5      # km^3/s^2
J2 = 0.0010826269
J3 = 0
R = 6378.1363           # km
constParams = {
    'mu'    : mu,
    'J2'    : J2,
    'J3'    : J3,
    'R'     : R
    }

# define truth orbit
SMA = 10e3              # km
ECC = 0.001
INC = 40                # deg
AOP = 80                # deg
RAAN = 40               # deg
TA0 = 0                 # deg
Period = 2 * np.pi * np.sqrt(SMA**3/mu)

# convert initial state to inertial cartesian state
[r0vec, v0vec] = astro.kep2cart_TA(SMA, ECC, INC, AOP, RAAN, TA0, mu)
xx0 = np.append(r0vec, v0vec)

# initial conditions for filters
t0 = 0
sig0list = np.array([1, 1, 1, 1e-3, 1e-3, 1e-3])
sig0list = np.append(sig0list, np.array([1e-9, 1e-9, 1e-9]))
P0 = np.diag(sig0list)**2 # km^2, (km/s)^2
R0 = np.diag([1e-3, 1e-6])**2 # km^2, (km/s)^2

# add a perturbation to the true xx0 to make the filter work for it
delxx0 = np.zeros(6)
for i in range(6):
    sig0 = sig0list[i]
    delxx0[i] = norm.rvs(loc = 0, scale = sig0, size = 1,
                         random_state = randSeed + i)[0]
xx0hat = xx0 + delxx0
xx0hat = np.append(xx0hat, np.zeros(3))

ICParams_CKF = {
    't0'    : t0,
    'xx0'   : xx0hat,
    'P0'    : P0,
    'R0'    : R0,
    'tau'   : Period/30
    }

ICParams_EKF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0,
    'tau'   : Period/30
    }

# generate Jacobians and Q matrix eval functions
_, evalA = Jacobians.getdfdZ()
_, evalH = Jacobians.getHtilde_sc_rho_rhod()

# =============================================================================
# define plotting function
# =============================================================================
def plots(thist, xxHat, xxTruth, Phist, rhist):
    xxErr = xxHat - xxTruth
    STDhist = np.empty(xxErr.shape)
    for i in range(xxErr.shape[0]):
        STDhist[i,:] = np.sqrt(np.diag(Phist[i,:,:]))

    fig, ax = plt.subplots(3,1,sharex = True, sharey = True)
    fig.suptitle('Position State Errors')
    for i in range(3):
        ax[i].plot(thist/3600, xxErr[:,i], marker = '.', linestyle = '-',
                   label = 'err')
        ax[i].plot(thist/3600, 3*STDhist[:,i], 'C1--', label = r'$\pm 3\sigma')
        ax[i].plot(thist/3600, -3*STDhist[:,i], 'C1--')
        ax[i].grid()
        if i < 2:
            ax[i].tick_params('x', labelbottom=False)
    ax[0].set_ylabel(r'$\Delta x$, km')
    ax[1].set_ylabel(r'$\Delta y$, km')
    ax[2].set_ylabel(r'$\Delta z$, km')
    ax[2].set_xlabel('time, hours')
    ax[0].set_ylim((-0.1, 0.1))
    plt.tight_layout()
    
    fig, ax = plt.subplots(3,1,sharex = True, sharey = True)
    fig.suptitle('Velocity State Errors')
    for i in range(3):
        ax[i].plot(thist/3600, xxErr[:,i+3], marker = '.', linestyle = '-',
                   label = 'err')
        ax[i].plot(thist/3600, 3*STDhist[:,i+3], 'C1--',
                   label = r'$\pm 3\sigma')
        ax[i].plot(thist/3600, -3*STDhist[:,i+3], 'C1--')
        ax[i].grid()
        if i < 2:
            ax[i].tick_params('x', labelbottom=False)
    ax[0].set_ylabel(r'$\Delta v_x$, km/s')
    ax[1].set_ylabel(r'$\Delta v_y$, km/s')
    ax[2].set_ylabel(r'$\Delta v_z$, km/s')
    ax[2].set_xlabel('time, hours')
    ax[0].set_ylim((-0.0002, 0.0002))
    plt.tight_layout()
    
    fig, ax = plt.subplots(2,1, sharex = True)
    fig.suptitle('Post-Update Measurement Residuals')
    for i in range(2):
        ax[i].plot(thist/3600, rhist[:,i], '+')
        ax[i].grid()

    ax[0].set_ylabel('range residuals, km')
    ax[0].tick_params('x', labelbottom=False)
    ax[1].set_ylabel('range rate residuals, km/s')
    ax[1].set_xlabel('time, hr')
    plt.tight_layout()
print('setup complete')

#%%
# =============================================================================
# Generate CKF RMS values for varying sigma values
# =============================================================================
# note: setting sig = 0 turns off SNC
sigList = [0, 5e-17, 5e-15, 5e-13, 5e-12,
           5e-11, 5e-10, 5e-9, 5e-8, 5e-7, 5e-6] # km/s^2
RMS_pos = []
RMS_vel = []
RMS_range = []
RMS_rate = []

for sig in sigList:
    print('\nsig = {0:.3e} km/s^2:'.format(sig))
    # define Qct
    Qct = sig**2 * np.eye(3)
    
    # run filter
    thist, xxHat, Phist, rhist, RMSList = filters.CKF_HW3_DMC(obs.copy(),
                                                              xxTruth,
                                                              ICParams_CKF,
                                                              constParams,
                                                              Qct,
                                                              evalA = evalA,
                                                              evalH = evalH,
                                                              maxits = 1)
    
    # append results
    RMS_pos.append(RMSList[0])
    RMS_vel.append(RMSList[1])
    RMS_range.append(RMSList[2])
    RMS_rate.append(RMSList[3])

#%%
# =============================================================================
# Plots vs. sigma
# =============================================================================
fig, ax = plt.subplots(2,1, sharex = True)
fig.suptitle(r'State Errors RMS vs. $\sigma$')
ax[0].semilogx(sigList, RMS_pos, 'o-')
ax[0].grid()
ax[0].set_ylabel('pos. RMS, km')
ax[0].tick_params('x', labelbottom=False)
ax[1].semilogx(sigList, RMS_vel, 'o-')
ax[1].grid()
ax[1].set_ylabel('vel. RMS, km/s')
ax[1].set_xlabel(r'$\sigma$, $km/s^2$')
plt.tight_layout()

fig, ax = plt.subplots(2,1, sharex = True)
fig.suptitle(r'Measurement Residuals RMS vs. $\sigma$')
ax[0].semilogx(sigList, RMS_range, 'o-')
ax[0].grid()
ax[0].set_ylabel('range RMS, km')
ax[0].tick_params('x', labelbottom=False)
ax[1].semilogx(sigList, RMS_rate, 'o-')
ax[1].grid()
ax[1].set_ylabel('range rate RMS, km/s')
ax[1].set_xlabel(r'$\sigma$, $km/s^2$')
plt.tight_layout()

#%%
# =============================================================================
# Run once more for "optimal" sigma value
# =============================================================================
sig = 5e-7
Qct = sig**2 * np.eye(3)

# run filter
thist, xxHat, Phist, rhist, RMSList = filters.CKF_HW3_DMC(obs.copy(),
                                                          xxTruth,
                                                          ICParams_CKF,
                                                          constParams,
                                                          Qct,
                                                          evalA = evalA,
                                                          evalH = evalH,
                                                          maxits = 1)
#%%
plots(thist, xxHat[:,:6], xxTruth, Phist[:,:6,:6], rhist)

#%%
# =============================================================================
# Generate EKF RMS values for varying sigma values
# =============================================================================
sigList = [0, 5e-17, 5e-15, 5e-13, 5e-12,
           5e-11, 5e-10, 5e-9] # km/s^2

RMS_pos = []
RMS_vel = []
RMS_range = []
RMS_rate = []

for sig in sigList:
    print('\nsig = {0:.3e} km/s^2:'.format(sig))
    # define Qct
    Qct = sig**2 * np.eye(3)
    
    # run filter
    thist, xxHat, Phist, rhist, RMSList = filters.EKF_HW3_DMC(obs.copy(),
                                                              xxTruth,
                                                              ICParams_EKF,
                                                              constParams,
                                                              Qct,
                                                              evalA = evalA,
                                                              evalH = evalH,
                                                              NCKF = 0)
    
    # append results
    RMS_pos.append(RMSList[0])
    RMS_vel.append(RMSList[1])
    RMS_range.append(RMSList[2])
    RMS_rate.append(RMSList[3])

#%%
# =============================================================================
# Plots vs. sigma
# =============================================================================
fig, ax = plt.subplots(2,1, sharex = True)
fig.suptitle(r'State Errors RMS vs. $\sigma$')
ax[0].semilogx(sigList, RMS_pos, 'o-')
ax[0].grid()
ax[0].set_ylabel('pos. RMS, km')
ax[0].tick_params('x', labelbottom=False)
ax[1].semilogx(sigList, RMS_vel, 'o-')
ax[1].grid()
ax[1].set_ylabel('vel. RMS, km/s')
ax[1].set_xlabel(r'$\sigma$, $km/s^2$')
plt.tight_layout()

fig, ax = plt.subplots(2,1, sharex = True)
fig.suptitle(r'Measurement Residuals RMS vs. $\sigma$')
ax[0].semilogx(sigList, RMS_range, 'o-')
ax[0].grid()
ax[0].set_ylabel('range RMS, km')
ax[0].tick_params('x', labelbottom=False)
ax[1].semilogx(sigList, RMS_rate, 'o-')
ax[1].grid()
ax[1].set_ylabel('range rate RMS, km/s')
ax[1].set_xlabel(r'$\sigma$, $km/s^2$')
plt.tight_layout()

#%%
# =============================================================================
# Run once more for "optimal" sigma value
# =============================================================================
sig = 5e-10
Qct = sig**2 * np.eye(3)

# run filter
thist, xxHat, Phist, rhist, RMSList = filters.EKF_HW3_DMC(obs.copy(),
                                                          xxTruth,
                                                          ICParams_EKF,
                                                          constParams,
                                                          Qct,
                                                          evalA = evalA,
                                                          evalH = evalH,
                                                          NCKF = 0)
#%%
plots(thist, xxHat[:,:6], xxTruth, Phist[:,:6,:6], rhist)

#%%
# =============================================================================
# Run the above EKF case again, 1/10 the value for tau
# =============================================================================
ICParams_EKF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0,
    'tau'   : Period/300
    }

# run filter
thist, xxHat, Phist, rhist, RMSList = filters.EKF_HW3_DMC(obs.copy(),
                                                          xxTruth,
                                                          ICParams_EKF,
                                                          constParams,
                                                          Qct,
                                                          evalA = evalA,
                                                          evalH = evalH,
                                                          NCKF = 0)
plots(thist, xxHat[:,:6], xxTruth, Phist[:,:6,:6], rhist)

#%%
# =============================================================================
# One last time, with 10x the original value for tau
# =============================================================================
ICParams_EKF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0,
    'tau'   : Period/3
    }

# run filter
thist, xxHat, Phist, rhist, RMSList = filters.EKF_HW3_DMC(obs.copy(),
                                                          xxTruth,
                                                          ICParams_EKF,
                                                          constParams,
                                                          Qct,
                                                          evalA = evalA,
                                                          evalH = evalH,
                                                          NCKF = 0)
plots(thist, xxHat[:,:6], xxTruth, Phist[:,:6,:6], rhist)

#%%
from matplotlib.backends.backend_pdf import PdfPages
filename = './../HW/HW3/HW3figs.pdf'
pp = PdfPages(filename)
figs = [plt.figure(n) for n in plt.get_fignums()]
for fig in figs:
    fig.savefig(pp, format='pdf')
pp.close()

