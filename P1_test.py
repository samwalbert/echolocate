# -*- coding: utf-8 -*-
"""
P1_test.py:
    test script for project 1, Stat OD
Created on Sat Feb 12 15:50:24 2022

@author: Samuel Albert
"""

import numpy as np
import pandas as pd
from matplotlib import cm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.integrate import solve_ivp

from src import dynamics as dyn
from src import Jacobians
from src import filters

# =============================================================================
# Constants & given inputs
# =============================================================================
mu = 3.986004415e5          # km^3/s^2
J2 = 1.082626925638815e-3
R = 6378.1363               # km
omP = 7.2921158553e-5       # rad/s
theta0 = 0.0                # 0 initial ECEF/ECI rotation

rho0 = 3.614e-4             # kg/km^3
r0 = 700.0000 + R           # km
H =  88.6670                # km
Area = 3.0e-6               # km^2
m = 970                     # kg
CD = 2.0

Ngs = 3

params = {
    'R':    R,
    'omP':  omP,
    'Area': Area,
    'm':    m,
    'rho0': rho0,
    'H':    H,
    'r0':   r0,
    'Ngs':  Ngs}

# define ground stations
Nstations = 3               # number of stations
RSvecs = np.empty((Nstations,3)) # ECEF position vectors of stations
RSvecs[:] = np.NaN
IDs = [None]*Nstations

# Station 1
RSvecs[0,0] = -5127.5100   # km
RSvecs[0,1] = -3794.1600   # km
RSvecs[0,2] = 0.0          # km
IDs[0] = '101'

# Station 2
RSvecs[1,0] = 3860.9100    # km
RSvecs[1,1] = 3238.4900    # km
RSvecs[1,2] = 3898.0940    # km
IDs[1] = '337'

# Station 3
RSvecs[2,0] = 549.5050     # km
RSvecs[2,1] = -1380.8720   # km
RSvecs[2,2] = 6182.1970    # km
IDs[2] = '394'

# a-priori satellite state
r0vec = np.array([757.7000,
                  5222.6070,
                  4851.5000]) # km
v0vec = np.array([2.21321,
                  4.67834,
                  -5.37130]) # km/s

# initial conditions
t0 = 0
P0 = np.diag([1, 1, 1, 1, 1, 1, 1e2, 1e6, 1e6,
              1e-16, 1e-16, 1e-16, 1, 1, 1, 1, 1, 1]) # km-base
R0 = np.diag([(1e-5)**2, (1e-6)**2])
xx0hat = np.block([r0vec, v0vec, mu, J2, CD, RSvecs.flatten()]).flatten()

def plots(thist, xxHat, Phist, rhist, xxRef):
    # residuals
    fig, ax = plt.subplots(2,1, sharex = True)
    fig.suptitle('Post-Update Measurement Residuals')
    for i in range(2):
        ax[i].plot(thist/3600, rhist[:,i], '+')
        ax[i].grid()

    ax[0].set_ylabel('range residuals, km')
    ax[0].tick_params('x', labelbottom=False)
    ax[1].set_ylabel('range rate residuals, km/s')
    ax[1].set_xlabel('time, hr')
    plt.tight_layout()
    
    # covariance trace
    fig, ax = plt.subplots(2,1, sharex = True)
    fig.suptitle('Covariance Trace')
    ax[0].semilogy(thist/3600, np.trace(Phist[:,:3,:3], axis1=1, axis2=2), '+-')
    ax[0].grid()
    ax[0].set_ylabel('Pos. Cov., km')
    ax[0].tick_params('x', labelbottom=False)
    ax[1].semilogy(thist/3600, np.trace(Phist[:,3:6,3:6], axis1=1, axis2=2), '+-')
    ax[1].grid()
    ax[1].set_ylabel('Vel. Cov., km')
    ax[1].set_xlabel('time, hr')
    plt.tight_layout()
    
    # final state ellipsoids
    tsigs = 3*np.sqrt(np.diag(Phist[-1,:6,:6]))
    print('3 sigma values:')
    print(tsigs)
    # position:
    u = np.linspace(0, 2 * np.pi, 100)
    v = np.linspace(0, np.pi, 100)
    x = tsigs[0] * np.outer(np.cos(u), np.sin(v))
    y = tsigs[1] * np.outer(np.sin(u), np.sin(v))
    z = tsigs[2] * np.outer(np.ones_like(u), np.cos(v))
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.plot_surface(x, y, z, cmap = cm.viridis)
    ax.set_xlabel('x, km')
    ax.set_ylabel('y, km')
    ax.set_zlabel('z, km')
    fig.suptitle(r'$3\sigma$ Ellipsoid for Spacecraft Position at Final Time')
    plt.tight_layout()
    
    # velocity:
    x = tsigs[3] * np.outer(np.cos(u), np.sin(v))
    y = tsigs[4] * np.outer(np.sin(u), np.sin(v))
    z = tsigs[5] * np.outer(np.ones_like(u), np.cos(v))
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.plot_surface(x, y, z, cmap = cm.viridis)
    ax.set_xlabel('x, km/s')
    ax.set_ylabel('y, km/s')
    ax.set_zlabel('z, km/s')
    fig.suptitle(r'$3\sigma$ Ellipsoid for Spacecraft Velocity at Final Time')
    plt.tight_layout()
    
    # relative states
    DX = np.linalg.norm((xxRef[:,:3] - xxHat[:,:3]), axis = 1)
    DV = np.linalg.norm((xxRef[:,3:6] - xxHat[:,3:6]), axis = 1)
    fig, ax = plt.subplots(2, 1, sharex = True)
    fig.suptitle('Relative States')
    ax[0].plot(thist/3600, DX, '+-')
    ax[0].grid()
    ax[0].set_ylabel(r'$\Delta x$, km')
    ax[0].tick_params('x', labelbottom = False)
    ax[1].plot(thist/3600, DV, '+-')
    ax[1].grid()
    ax[1].set_ylabel(r'$\Delta v$, km')
    
    
    


#%%
# Generate Jacobians
Asym, evalA = Jacobians.getdfdZ_P1(Ngs = 3)
Atest = evalA(r0vec, v0vec, mu, J2, CD, RSvecs, params)

#%%
Hsym, evalH = Jacobians.getH_P1()
xxvec = np.block([r0vec, v0vec, mu, J2, CD, RSvecs.flatten()]).flatten()
Htest = evalH(xxvec, params, 0, 3)



#%% plot initial guess trajectory
# plt.close('all')
# fig, ax = plt.subplots(1, 1)
# ax.plot(sol.y[0,:], sol.y[1,:])
# ax.grid()

# fig, ax = plt.subplots(1, 1)
# ax.plot(sol.t, np.linalg.norm(sol.y[:3,:], axis = 0) - R)
# ax.grid()


#%%
# load observations from file, convert from m to km
# with open('data/P1_observations.txt', 'rb') as f:
fname = 'data/P1_observations.txt'
obs = pd.read_csv(fname, delim_whitespace = True, header = None)
obs.columns = ['t', 'ID', 'range', 'rangerate']
obs.range = obs.range/1e3 # m to km
obs.rangerate = obs.rangerate/1e3 # m/s to km/s


#%% NLBF
P0 = np.diag([1, 1, 1, 1, 1, 1, 1e2, 1e6, 1e6,
              1e-20, 1e-20, 1e-20, 1, 1, 1, 1, 1, 1]) # km-base
ICParams_NLBF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF = filters.NLBF_P1(obs.copy(),
                                                              ICParams_NLBF,
                                                              params,
                                                              evalA = evalA,
                                                              evalH = evalH,
                                                              Ngs = 3,
                                                              tol = 1e-7,
                                                              maxits = 3)

# #%%
# plt.close('all')
# plots(thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF, xxRef)

# #%% CKF
# # ## DEBUG
# # R0 = np.diag([1e-4, 1e-6])
# ICParams_CKF = {
#     't0'    : t0,
#     'xx0hat': xx0hat,
#     'P0'    : P0,
#     'R0'    : R0
#     }

# #%%
# thistCKF, xxhatCKF, PhistCKF, rhistCKF = filters.CKF_P1(obs.copy(),
#                                                         ICParams_CKF,
#                                                         params,
#                                                         evalA = evalA,
#                                                         evalH = evalH,
#                                                         Ngs = 3,
#                                                         tol = 1e-7,
#                                                         maxits = 3)

# #%%
# plt.close('all')
# plots(thistCKF, xxhatCKF, PhistCKF, rhistCKF)



# #%% propagate initial guess trajectory
# Nstate = 9 + Ngs*3
# IC = np.block([r0vec, v0vec, mu, J2, CD, RSvecs.flatten()]).flatten()
# IC = np.append(IC, np.eye(Nstate))
# sol = solve_ivp(lambda t, yy: dyn.muJ2CD_wPhi(t, yy, params, evalA),
#                 (0, thistCKF[-1]), IC, t_eval = thistCKF[1:],
#                 rtol = 1e-12, atol = 1e-12)
