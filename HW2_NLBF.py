# -*- coding: utf-8 -*-
"""
HW2_NLBF.py:
    nonlinaer batch filter for Stat OD HW2
Created on Sun Jan 30 17:14:40 2022

@author: Samuel Albert
"""

import time
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

from src import dynamics as dyn
from src import measurements as mes
from src import astrodynamics as astro
from src import Jacobians

tic = time.time()

# =============================================================================
# Options
# =============================================================================
NOISEON = True
INITIALDELTAX = True
APRIORIESTIMATE = True

#%%
# =============================================================================
# Load measurement data
# =============================================================================
obs = pd.read_pickle('data/HW2_measurements.pkl')

#%%
# =============================================================================
# Set up ground station data for measurement predictions
# =============================================================================
# define ground stations
Nstations = 3 # number of stations
coords = np.empty((Nstations,2))
coords[:] = np.NaN
masks = np.empty(Nstations)
masks[:] = np.NaN
IDs = [None]*Nstations

# Station 1
coords[0,0] = 148.981944
coords[0,1] = -35.398333
masks[0] = 10
IDs[0] = 'Canberra'

# Station 2
coords[1,0] = 355.749444
coords[1,1] = 40.427222
masks[1] = 10
IDs[1] = 'Madrid'

# Station 3
coords[2,0] = 243.205
coords[2,1] = 35.247164
masks[2] = 10
IDs[2] = 'Goldstone'

#%%
# =============================================================================
# Initialize j = 0 values for filter
# =============================================================================
# initialize xx0vec to truth
# use true dynamics to propagate xxvec

# use truth orbit to generate initial state
# constants 
mu = 3.986004415e5      # km^3/s^2
J2 = 0.0010826269
J3 = 0
R = 6378.1363           # km
D = 0.99726968 * 86400  # s
theta0 = 122            # deg, rotation of central body at time t = 0

# define truth orbit
SMA = 10e3              # km
ECC = 0.001
INC = 40                # deg
AOP = 80                # deg
RAAN = 40               # deg
TA0 = 0                 # deg
Period = 2 * np.pi * np.sqrt(SMA**3/mu)

# convert initial state to inertial cartesian state
[r0vec, v0vec] = astro.kep2cart_TA(SMA, ECC, INC, AOP, RAAN, TA0, mu)
xx0 = np.append(r0vec, v0vec)
xx0hat = xx0

Nobs = len(obs)
Nstate = len(xx0)
Ny = 2 # range, range rate

j = 0
t0 = 0

P0 = np.diag([1, 1, 1, 1e-3, 1e-3, 1e-3])**2 # km^2, (km/s)^2, given values
if INITIALDELTAX:
    DELxxM = np.array([0.5, 1.5, -1, 1e-3, -1e-3, -1.5e-3]) # within above cov.
else:
    DELxxM = np.zeros(6)
xx0hat += DELxxM
R0 = np.diag([1e-3, 1e-6])**2 # km^2, (km/s)^2

ICParams = {
    't0'    : t0,
    'xx0'   : xx0,
    'xx0hat': xx0hat,
    'DELxxM': DELxxM,
    'P0'    : P0,
    'R0'    : R0
    }


#%%
# =============================================================================
# Generate necessary jacobian function handles
# =============================================================================
_, evalA = Jacobians.getdfdZ()
_, evalH = Jacobians.getHtilde_sc_rho_rhod()


#%%
# =============================================================================
# =============================================================================
# # Major iterations, j
# =============================================================================
# =============================================================================
maxits = 15 # should be ~10
converged = False
tol = 1e-9

while (not converged and j <= maxits):
    # =========================================================================
    # Initialize filter
    # =========================================================================
    # initialize empty variables
    xxhathist = np.empty((Nobs, Nstate))
    xxhathist[:] = np.NaN
    xxhathist[0,:] = xx0hat

    thist = np.empty(Nobs)
    thist[:] = np.NaN
    thist[0] = t0

    yhist = np.empty((Nobs, Ny))
    yhist[:] = np.NaN # note, yhist[0] stays equal to NaN

    Rhist = np.empty((Nobs, Ny, Ny))
    Rhist[:] = np.NaN
    Rhist[0,:,:] = R0

    rhist = np.empty((Nobs, Ny))
    rhist[:] = np.NaN

    rPhist = np.empty((Nobs, Ny))
    rPhist[:] = np.NaN
    
    # initialize values
    Phi_im1_0 = np.eye(Nstate)
    
    if (APRIORIESTIMATE or j > 0):
        INF = np.linalg.inv(P0)
        NNN = np.linalg.inv(P0) @ DELxxM
    else:
        INF = np.zeros(P0.shape)
        NNN = np.zeros(Nstate)
    
    # =========================================================================
    # # =======================================================================
    # # Minor iterations, i
    # # =======================================================================
    # =========================================================================
    # NOTE TO SELF: index "i" corresponds to the update based on measurement with
    # index "i-1", such that index i = 0 corresponds to initialization, i = 1
    # is the result of the first measurement update.
    
    for i in range(1,len(obs)):
        # =====================================================================
        # Read next observation
        # =====================================================================
        thist[i] = obs['t'][i-1]
        if NOISEON:
            yhist[i,0] = obs['noisyrange'][i-1]
            yhist[i,1] = obs['noisyrate'][i-1]
        else:
            yhist[i,0] = obs['range'][i-1]
            yhist[i,1] = obs['rangerate'][i-1]
        Rhist[i,:,:] = R0
        
        rvecS = np.array([obs['x'][i-1], obs['y'][i-1], obs['z'][i-1]])
        vvecS = np.array([obs['vx'][i-1], obs['vy'][i-1], obs['vz'][i-1]])
        
        # =====================================================================
        # Propagate and time update from t[i-1] to t[i] (or equate)
        # =====================================================================
        if (thist[i] == thist[i-1]):
            # don't update xxhat or STM
            print('skipping propagate step at index {0:d}'.format(i))
            xxhathist[i,:] = xxhathist[i-1,:]
            Phi = Phi_im1_0
        else:
            # numerically propagate nonlinear state, STM
            IC = np.append(xxhathist[i-1,:],
                           np.reshape(np.eye(Nstate), (Nstate**2,)))
            
            sol = solve_ivp(lambda t, yy: dyn.keplerJ2J3_wPhi_ODErv(t, yy, mu, 
                                                                    J2, J3, R, 
                                                                    evalA),
                            (thist[i-1], thist[i]), IC,
                            rtol = 1e-12, atol = 1e-12)
            xxhathist[i,:] = sol.y[:6,-1]
            Phi_i_im1 = np.reshape(sol.y[6:,-1], (Nstate,Nstate))
            Phi = Phi_i_im1 @ Phi_im1_0
            
        # =====================================================================
        # Process observations
        # =====================================================================
        # predict range and range rate measurements for curent state
        indObs = obs['ind'][i-1]
        rho, rhodot = mes.compute_range_rangerate_single(xxhathist[i,:],
                                                         coords[indObs],
                                                         0, theta0, thist[i],
                                                         R, D)
        yPred = np.array([rho, rhodot])
        
        # compute measurement sentivity matrix at this time
        Hi = evalH(xxhathist[i,:3], xxhathist[i,3:], rvecS, vvecS)
        
        # compute pre-update residual
        rhist[i,:] = yhist[i,:] - yPred
        
        # accumulate information
        INF = INF + (Hi @ Phi).T @ np.linalg.inv(Rhist[i,:,:]) @ (Hi @ Phi)
        NNN = NNN + (Hi @ Phi).T @ np.linalg.inv(Rhist[i,:,:]) @ rhist[i,:]
        
        # update previous STM value
        Phi_im1_0 = Phi
        
    # =========================================================================
    # Compute state deviation and covariance at initial time
    # =========================================================================
    DELxxP = np.linalg.inv(INF) @ NNN
    P0P = np.linalg.inv(INF)
    err = np.linalg.norm(DELxxP)
    
    if np.linalg.norm(err) < tol:
        converged = True
    else:
        xx0hat = xx0hat + DELxxP
        DELxxM -= DELxxP
    
    print('completed major iteration j = {0:d}, error = {1:.3e}'.format(j, err))
    j += 1
    
    
#%%
# =============================================================================
# Plots
# =============================================================================
plt.close('all')

fig1, ax1 = plt.subplots(2,1)
fig1.suptitle('measurement residuals')
for i in range(2):
    ax1[i].plot(thist, rhist[:,i], marker = '.', linestyle = '-')
    ax1[i].grid()
ax1[0].set_ylabel('range residuals, km')
ax1[1].set_ylabel('range rate residuals, km/s')
ax1[1].set_xlabel('time, s')
plt.tight_layout()






#%%
toc = time.time()
print('script took {0:.3f} seconds to execute'.format(toc-tic))