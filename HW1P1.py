# -*- coding: utf-8 -*-
"""
Created on Sun Jan 23 18:43:53 2022

@author: Samuel Albert
"""

import numpy as np
import sympy as sp
from IPython.display import display
# sp.init_printing()

from src import Jacobians

Jacsym, evalJac = Jacobians.getdfdZ_wJ2J3()

# test against MATLAB grader inputs
rvec = np.array([-3483.819243, 8315.124271, 4090.441521])
vvec = np.array([-4.39707389, -3.386925829, 3.140027421])
mu = 398600.4415
J2 = 0.00108263
J3 = -2.532300E-06
R = 6378

Jac = evalJac(rvec, vvec, mu, J2, J3, R) # numerical result, test via Excel

# # print symbolic result for report
# for i, row in enumerate(Jacsym):
#     for j, dd in enumerate(row):
#         # print(sp.latex(dd))
#         print('newline')
#         # display(dd)
#         print(str(dd))






    