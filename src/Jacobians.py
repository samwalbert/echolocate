# -*- coding: utf-8 -*-
"""
Jacobians.py:
    symbolically-derived partial derivatives for various dynamics
Created on Sun Jan 23 18:45:19 2022

@author: Samuel Albert
"""

import numpy as np
import sympy as sp
import numba as nb


def getdfdZ_wJ2J3():
    ''' 
    returns function handle that evaluates the desired partial,
    and an array of sympy objects that can then be printed.
    Includes mu, J2, and J3 as states.
    '''
    x, y, z, R, mu, J2, J3 = sp.symbols('x, y, z, R, mu, J2, J3')
    dx, dy, dz = sp.symbols('dx, dy, dz')
    zero = sp.Integer(0)
    
    r = sp.sqrt(x**2 + y**2 + z**2)
    
    # define accelerations
    amu = -mu / r**3 * np.array([x, y, z])
    
    aJ2 = -3/2*J2*mu/r**2*(R/r)**2*np.array([(1 - 5*(z/r)**2)*x/r,
                                             (1 - 5*(z/r)**2)*y/r,
                                             (3 - 5*(z/r)**2)*z/r])
    
    aJ3 = 1/2*J3*mu/r**2*(R/r)**3*np.array([5*(7*(z/r)**3 - 3*(z/r))*x/r,
                                            5*(7*(z/r)**3 - 3*(z/r))*y/r,
                                            3*(1 - 10*(z/r)**2 + 35/3*(z/r)**4)])
    
    a = amu + aJ2 + aJ3
    
    # build state and dynamics function arrays
    Z = np.array([x, y, z, dx, dy, dz, mu, J2, J3])
    f = np.array([dx, dy, dz, a[0], a[1], a[2], zero, zero, zero])
    
    # set up Jacobian matrix and take derivatives
    Jac = np.empty((len(f), len(Z)), dtype = np.object)
    Jac[:] = np.NaN
    for i, fi in enumerate(f):
        for j, varj in enumerate(Z):
            Jac[i,j] = sp.simplify(fi.diff(varj))
    Jac[np.where(Jac == 0)] = 0.0
    Jac[np.where(Jac == 1)] = 1.0

    args = [x, y, z, dx, dy, dz, mu, J2, J3, R]
    evalJac_prot = nb.njit(sp.lambdify(args, Jac, cse = sp.cse))
    
    def evalJac(rvec, vvec, mu, J2, J3, R):            
        args = [rvec[0], rvec[1], rvec[2],
                vvec[0], vvec[1], vvec[2],
                mu, J2, J3, R]
        return np.asarray(evalJac_prot(*args))
    
    return Jac, evalJac

def getdfdZ_wJ2():
    ''' 
    returns function handle that evaluates the desired partial,
    and an array of sympy objects that can then be printed.
    Includes J2 as a state (not mu).
    '''
    x, y, z, R, mu, J2 = sp.symbols('x, y, z, R, mu, J2')
    dx, dy, dz = sp.symbols('dx, dy, dz')
    zero = sp.Integer(0)
    
    r = sp.sqrt(x**2 + y**2 + z**2)
    
    # define accelerations
    amu = -mu / r**3 * np.array([x, y, z])
    
    aJ2 = -3/2*J2*mu/r**2*(R/r)**2*np.array([(1 - 5*(z/r)**2)*x/r,
                                             (1 - 5*(z/r)**2)*y/r,
                                             (3 - 5*(z/r)**2)*z/r])
    
    a = amu + aJ2
    
    # build state and dynamics function arrays
    Z = np.array([x, y, z, dx, dy, dz, J2])
    f = np.array([dx, dy, dz, a[0], a[1], a[2], zero])
    
    # set up Jacobian matrix and take derivatives
    Jac = np.empty((len(f), len(Z)), dtype = np.object)
    Jac[:] = np.NaN
    for i, fi in enumerate(f):
        for j, varj in enumerate(Z):
            Jac[i,j] = fi.diff(varj)
    Jac[np.where(Jac == 0)] = 0.0
    Jac[np.where(Jac == 1)] = 1.0

    args = [x, y, z, dx, dy, dz, mu, J2, R]
    evalJac_prot = nb.njit(sp.lambdify(args, Jac, cse = sp.cse))
    
    def evalJac(rvec, vvec, mu, J2, R):
        args = [rvec[0], rvec[1], rvec[2],
                vvec[0], vvec[1], vvec[2],
                mu, J2, R]
        return np.asarray(evalJac_prot(*args))
    
    return Jac, evalJac

def getdfdZ():
    ''' 
    returns function handle that evaluates the desired partial,
    and an array of sympy objects that can then be printed.
    Includes mu, J2, and J3 gravity terms, but treats none as state.
    '''
    x, y, z, R, mu, J2, J3 = sp.symbols('x, y, z, R, mu, J2, J3')
    dx, dy, dz = sp.symbols('dx, dy, dz')
    
    r = sp.sqrt(x**2 + y**2 + z**2)
    
    # define accelerations
    amu = -mu / r**3 * np.array([x, y, z])
    
    aJ2 = -3/2*J2*mu/r**2*(R/r)**2*np.array([(1 - 5*(z/r)**2)*x/r,
                                             (1 - 5*(z/r)**2)*y/r,
                                             (3 - 5*(z/r)**2)*z/r])
    
    aJ3 = 1/2*J3*mu/r**2*(R/r)**3*np.array([5*(7*(z/r)**3 - 3*(z/r))*x/r,
                                            5*(7*(z/r)**3 - 3*(z/r))*y/r,
                                            3*(1 - 10*(z/r)**2 + 35/3*(z/r)**4)])
    
    a = amu + aJ2 + aJ3
    
    # build state and dynamics function arrays
    Z = np.array([x, y, z, dx, dy, dz])
    f = np.array([dx, dy, dz, a[0], a[1], a[2]])
    
    # set up Jacobian matrix and take derivatives
    Jac = np.empty((len(f), len(Z)), dtype = np.object)
    Jac[:] = np.NaN
    for i, fi in enumerate(f):
        for j, varj in enumerate(Z):
            Jac[i,j] = fi.diff(varj)
    Jac[np.where(Jac == 0)] = 0.0
    Jac[np.where(Jac == 1)] = 1.0

    args = np.array([x, y, z, dx, dy, dz, mu, J2, J3, R])
    evalJac_prot = nb.njit(sp.lambdify([args,], Jac, cse = sp.cse))
    
    def evalJac(rvec, vvec, mu, J2, J3, R):
        args = np.array([rvec[0], rvec[1], rvec[2],
                vvec[0], vvec[1], vvec[2],
                mu, J2, J3, R])
        return np.asarray(evalJac_prot(args))
    
    return Jac, evalJac

def evalAz_HW3_DMC(rvec, vvec, mu, J2, J3, R, evalA, tauvec):    
    '''
    augments A matrix for DMC, HW3
    '''
    A = evalA(rvec, vvec, mu, J2, J3, R)
    Bw = np.diag(tauvec)
    UR = np.block([[np.zeros((3,3))],[np.eye(3)]])
    Az = np.block([[A, UR], [np.zeros((3,6)), -Bw]])
    return Az
    

def getdfdZ_P1(Ngs = 3):
    ''' 
    returns function handle that evaluates the desired partial,
    and an array of sympy objects that can then be printed.
    STATE:
        r: spacecraft position (x, y, z)
        v: spacecraft inertial velocity (dx, dy, dz)
        mu: GM of central body
        J2: oblateness of central body
        CD: drag coefficient of spacecraft
        RSi: position vector for each ground station (xS1, xS2, xS3)
    DYNAMICS:
        mu, J2, and drag affect spacecraft.
        no dynamics for constants (mu, J2, CD)
        each ground station is fixed to earth at some location
    INPUTS:
        Ngs: number of ground stations, defaults to 3
    '''
    x, y, z, dx, dy, dz = sp.symbols('x, y, z, dx, dy, dz')
    R, mu, J2, omP = sp.symbols('R, mu, J2, omP')
    CD, A, m, rho0, H, r0 = sp.symbols('CD, A, m, rho0, H, r0')
    zero = sp.Integer(0)
    
    r = sp.sqrt(x**2 + y**2 + z**2)
    rvec = np.array([x, y, z])
    omvec = np.array([0, 0, omP])
    
    # define spacecraft acceleration
    amu = -mu / r**3 * np.array([x, y, z])
    
    aJ2 = -3/2*J2*mu/r**2*(R/r)**2*np.array([(1 - 5*(z/r)**2)*x/r,
                                             (1 - 5*(z/r)**2)*y/r,
                                             (3 - 5*(z/r)**2)*z/r])
    
    uvec = np.array([dx, dy, dz]) - np.cross(omvec, rvec)
    u = sp.sqrt(uvec[0]**2 + uvec[1]**2 + uvec[2]**2)
    adrag = - 1/2 * rho0 * sp.exp(-(r-r0) / H) * u * CD * A / m * uvec
    
    a = amu + aJ2 + adrag
    
    # define ground station dynamics (currently: assumes all fixed to Earth)
    xSi = sp.symbols('x:'+str(Ngs))
    ySi = sp.symbols('y:'+str(Ngs))
    zSi = sp.symbols('z:'+str(Ngs))
    VSi = [None] * Ngs
    
    # build state and dynamics function arrays
    Z = np.array([x, y, z, dx, dy, dz, mu, J2, CD])
    f = np.array([dx, dy, dz, a[0], a[1], a[2], zero, zero, zero])
    for si in range(Ngs):
        VSi[si] = np.cross(omvec, np.array([xSi[si], ySi[si], zSi[si]]))
        Z = np.append(Z, np.array([xSi[si], ySi[si], zSi[si]]))
        f = np.append(f, np.array([VSi[si][0], VSi[si][1], VSi[si][2]]))
    
    
    # set up Jacobian matrix and take derivatives
    Jac = np.empty((len(f), len(Z)), dtype = np.object)
    Jac[:] = np.NaN
    for i, fi in enumerate(f):
        for j, varj in enumerate(Z):
            Jac[i,j] = fi.diff(varj)
    Jac[np.where(Jac == 0)] = 0.0
    Jac[np.where(Jac == 1)] = 1.0

    args = np.array([x, y, z, dx, dy, dz, 
                     mu, J2, CD, R, omP, A, m, rho0, H, r0])
    for si in range(Ngs):
        args = np.append(args, np.array([xSi[si], ySi[si], zSi[si]]))
        
    evalJac_prot = nb.njit(sp.lambdify([args,], Jac, cse = sp.cse))
    
    def evalJac(rvec, vvec, mu, J2, CD, RSvecs, params):
        
        args = np.array([rvec[0], rvec[1], rvec[2],
                vvec[0], vvec[1], vvec[2],
                mu, J2, CD,
                params['R'], params['omP'], params['Area'], params['m'],
                params['rho0'], params['H'], params['r0']])
        for si in range(len(RSvecs)):
            args = np.append(args, np.array([RSvecs[si][0],
                                             RSvecs[si][1],
                                             RSvecs[si][2]]))
        return np.asarray(evalJac_prot(args))
    
    return Jac, evalJac

def getH_P1():
    '''
    returns function handle that evaluates the desired partial,
    and an array of sympy objects that can then be printed.
    STATE:
        r: spacecraft position (x, y, z)
        v: spacecraft inertial velocity (dx, dy, dz)
        mu: GM of central body
        J2: oblateness of central body
        CD: drag coefficient of spacecraft
        RSi: position vector for each ground station (xS1, xS2, xS3)
    DYNAMICS:
        range and range rate, evaluated from a single ground station at a time
    NOTES:
        evalJac() function requires inputs below:
            Sind: index (from 0) of active ground station
            Ngs: total number of ground stations
        H matrix is then padded with zeros accordingly
    '''
    # define states
    x, y, z, dx, dy, dz = sp.symbols('x, y, z, dx, dy, dz')
    R, mu, J2, omP = sp.symbols('R, mu, J2, omP')
    CD, A, m, rho0, H, r0 = sp.symbols('CD, A, m, rho0, H, r0')
    xS, yS, zS = sp.symbols('xS, yS, zS')
    
    VS = np.cross(np.array([0, 0, omP]), np.array([xS, yS, zS]))
    dxS = VS[0]
    dyS = VS[1]
    dzS = VS[2]
    
    # define dynamics
    rho = sp.sqrt((x-xS)**2 + (y-yS)**2 + (z-zS)**2)
    rhod = (((x-xS) * (dx-dxS)) + ((y-yS) * (dy-dyS)) + ((z-zS) * (dz-dzS))) / rho
    
    # build state and dynamics function arrays
    Z = np.array([x, y, z, dx, dy, dz, mu, J2, CD, xS, yS, zS])
    f = np.array([rho, rhod])
    
    # set up Jacobian matrix and take derivatives
    Jac = np.empty((len(f), len(Z)), dtype = np.object)
    Jac[:] = np.NaN
    for i, fi in enumerate(f):
        for j, varj in enumerate(Z):
            Jac[i,j] = fi.diff(varj)
    Jac[np.where(Jac == 0)] = 0.0
    Jac[np.where(Jac == 1)] = 1.0
    
    args = np.array([x, y, z, dx, dy, dz, xS, yS, zS, mu, J2, CD, omP])
    evalJac_prot = nb.njit(sp.lambdify(args, Jac, cse = sp.cse))
    
    def evalJac(xxvec, params, Sind, Ngs):
        # evaluate H for given station
        ii = 9 + Sind * 3
        args = [xxvec[0], xxvec[1], xxvec[2],
                xxvec[3], xxvec[4], xxvec[5],
                xxvec[ii], xxvec[ii+1], xxvec[ii+2],
                xxvec[6], xxvec[7], xxvec[8], params['omP']]
        Hprot = np.asarray(evalJac_prot(*args))
        
        # now pad with zeros for other station position states
        H = np.zeros((2, (9+3*Ngs)))
        H[:,:9] = Hprot[:,:9]
        H[:,(9+3*Sind):(12+3*Sind)] = Hprot[:,9:]
        return H
    
    return Jac, evalJac

def getHtilde_sc_rho_rhod():
    '''
    returns a function that evaluates the desired partial, and an array of
    sympy objects that can be printed.
    '''
    # define states
    x, y, z, dx, dy, dz = sp.symbols('x, y, z, dx, dy, dz')
    xS, yS, zS, dxS, dyS, dzS = sp.symbols('xS, yS, zS, dxS, dyS, dzS')
    
    # define dynamics
    rho = sp.sqrt((x-xS)**2 + (y-yS)**2 + (z-zS)**2)
    rhod = (((x-xS) * (dx-dxS)) + ((y-yS) * (dy-dyS)) + ((z-zS) * (dz-dzS))) / rho
    
    
    # build state and dynamics function arrays
    Z = np.array([x, y, z, dx, dy, dz])
    f = np.array([rho, rhod])
    
    # set up Jacobian matrix and take derivatives
    RRs, rhodNum = sp.symbols('RRs, rhodNum')
    Jac = np.empty((len(f), len(Z)), dtype = np.object)
    Jac[:] = np.NaN
    for i, fi in enumerate(f):
        for j, varj in enumerate(Z):
            Jac[i,j] = fi.diff(varj)
    Jac[np.where(Jac == 0)] = 0.0
    Jac[np.where(Jac == 1)] = 1.0
            
    
    # create function handle to evaluate numerically
    args = [x, y, z, dx, dy, dz, xS, yS, zS, dxS, dyS, dzS]
    evalJac_prot = nb.njit(sp.lambdify(args, Jac, cse = sp.cse))
    
    def evalJac(rvec, vvec, rvecS, vvecS):
        
        args = [rvec[0], rvec[1], rvec[2],
                vvec[0], vvec[1], vvec[2],
                rvecS[0], rvecS[1], rvecS[2],
                vvecS[0], vvecS[1], vvecS[2]]
        return np.asarray(evalJac_prot(*args))
    
    return Jac, evalJac

def getHtilde_obs_rho_rhod():
    '''
    returns a function that evaluates the desired partial, and an array of
    sympy objects that can be printed.
    '''
    # define states
    x, y, z, dx, dy, dz = sp.symbols('x, y, z, dx, dy, dz')
    xS, yS, zS, dxS, dyS, dzS = sp.symbols('xS, yS, zS, dxS, dyS, dzS')
    
    # define dynamics
    rho = sp.sqrt((x-xS)**2 + (y-yS)**2 + (z-zS)**2)
    rhod = (((x-xS) * (dx-dxS)) + ((y-yS) * (dy-dyS)) + ((z-zS) * (dz-dzS))) / rho
    
    
    # build state and dynamics function arrays
    Z = np.array([xS, yS, zS, dxS, dyS, dzS])
    f = np.array([rho, rhod])
    
    # set up Jacobian matrix and take derivatives
    RRs, rhodNum = sp.symbols('RRs, rhodNum')
    Jac = np.empty((len(f), len(Z)), dtype = np.object)
    Jac[:] = np.NaN
    for i, fi in enumerate(f):
        for j, varj in enumerate(Z):
            Jac[i,j] = fi.diff(varj)
    Jac[np.where(Jac == 0)] = 0.0
    Jac[np.where(Jac == 1)] = 1.0
            
    
    # create function handle to evaluate numerically
    args = [x, y, z, dx, dy, dz, xS, yS, zS, dxS, dyS, dzS]
    evalJac_prot = nb.njit(sp.lambdify(args, Jac, cse = sp.cse))
    
    def evalJac(rvec, vvec, rvecS, vvecS):
        
        args = [rvec[0], rvec[1], rvec[2],
                vvec[0], vvec[1], vvec[2],
                rvecS[0], rvecS[1], rvecS[2],
                vvecS[0], vvecS[1], vvecS[2]]
        return np.asarray(evalJac_prot(*args))
    
    return Jac, evalJac
    

    
    
    























