# -*- coding: utf-8 -*-
"""
dynamics.py:
    dynamics function for Stat OD codebase
Created on Mon Jan 24 09:58:32 2022

@author: Samuel Albert
"""

import numpy as np
from src import Jacobians

def keplerJ2_ODE(t, yy, mu, J2, R):
    '''
    dynamics for mu + J2 spacecraft, no control. Treats J2 as a state variable.
    '''
    
    dydt = np.empty(7)
    dydt[:] = np.NaN
    
    # extract state components
    x = yy[0]
    y = yy[1]
    z = yy[2]
    dx = yy[3]
    dy = yy[4]
    dz = yy[5]
    J2 = yy[6]
    
    # evaluate constants
    r = np.sqrt(x**2 + y**2 + z**2)
    mu_r3 = mu / r**3
    
    # evaluate accelerations
    ax = -mu_r3 * x - 3/2 * J2 * mu/r**2 * (R/r)**2 * (1 - 5*(z/r)**2) * x/r
    ay = -mu_r3 * y - 3/2 * J2 * mu/r**2 * (R/r)**2 * (1 - 5*(z/r)**2) * y/r
    az = -mu_r3 * z - 3/2 * J2 * mu/r**2 * (R/r)**2 * (3 - 5*(z/r)**2) * z/r
    
    # ODEs
    dydt[0] = dx
    dydt[1] = dy
    dydt[2] = dz
    dydt[3] = ax
    dydt[4] = ay
    dydt[5] = az
    dydt[6] = 0
    
    return dydt

def keplerJ2_ODErv(t, yy, mu, J2, R):
    '''
    dynamics for mu + J2 spacecraft, no control.
    NOTE: does NOT treat J2 as a state
    '''
    
    dydt = np.empty(6)
    dydt[:] = np.NaN
    
    # extract state components
    x = yy[0]
    y = yy[1]
    z = yy[2]
    dx = yy[3]
    dy = yy[4]
    dz = yy[5]
    
    # evaluate constants
    r = np.sqrt(x**2 + y**2 + z**2)
    mu_r3 = mu / r**3
    
    # evaluate accelerations
    ax = -mu_r3 * x - 3/2 * J2 * mu/r**2 * (R/r)**2 * (1 - 5*(z/r)**2) * x/r
    ay = -mu_r3 * y - 3/2 * J2 * mu/r**2 * (R/r)**2 * (1 - 5*(z/r)**2) * y/r
    az = -mu_r3 * z - 3/2 * J2 * mu/r**2 * (R/r)**2 * (3 - 5*(z/r)**2) * z/r
    
    # ODEs
    dydt[0] = dx
    dydt[1] = dy
    dydt[2] = dz
    dydt[3] = ax
    dydt[4] = ay
    dydt[5] = az
    
    return dydt

def keplerJ2_wPhi_ODE(t, yy, mu, J2, R, evalJac):
    '''
    dynamics for mu + J2 spacecraft, no control. Treats J2 as a state variable.
    '''
    
    dydt = np.empty(56)
    dydt[:] = np.NaN
    
    # =========================================================================
    # deal with vector state
    # =========================================================================
    x = yy[0]
    y = yy[1]
    z = yy[2]
    dx = yy[3]
    dy = yy[4]
    dz = yy[5]
    J2 = yy[6]  
    
    # evaluate constants
    r = np.sqrt(x**2 + y**2 + z**2)
    mu_r3 = mu / r**3
    
    # evaluate accelerations
    ax = -mu_r3 * x - 3/2 * J2 * mu/r**2 * (R/r)**2 * (1 - 5*(z/r)**2) * x/r
    ay = -mu_r3 * y - 3/2 * J2 * mu/r**2 * (R/r)**2 * (1 - 5*(z/r)**2) * y/r
    az = -mu_r3 * z - 3/2 * J2 * mu/r**2 * (R/r)**2 * (3 - 5*(z/r)**2) * z/r
    
    # ODEs
    dydt[0] = dx
    dydt[1] = dy
    dydt[2] = dz
    dydt[3] = ax
    dydt[4] = ay
    dydt[5] = az
    dydt[6] = 0
    
    # =========================================================================
    # deal with STM
    # =========================================================================
    # extract Phi
    Phi = np.reshape(yy[7:], (7,7))  # get 7x7 STM from 49x1 yy vector slice
    
    # evaluate Jacobian matrix at this time
    # _, evalJac = Jacobians.getdfdZ_wJ2()
    dfdZ = evalJac(yy[0:3], yy[3:6], mu, J2, R)
    
    # matrix ODE
    dPhi = dfdZ @ Phi
    
    # flatten and assign dPhi
    dydt[7:] = np.reshape(dPhi, (49,))
    
    return dydt


def keplerJ2J3_wPhi_ODErv(t, yy, mu, J2, J3, R, evalJac):
    '''
    dynamics for mu + J2 + J3 spacecraft, no control.
    Note: does NOT treat J2 or J3 as a state variable.
    '''
    
    dydt = np.empty(42)
    dydt[:] = np.NaN
    
    # =========================================================================
    # deal with vector state
    # =========================================================================
    x = yy[0]
    y = yy[1]
    z = yy[2]
    dx = yy[3]
    dy = yy[4]
    dz = yy[5] 
    
    # evaluate constants
    r = np.sqrt(x**2 + y**2 + z**2)
    mu_r3 = mu / r**3
    mu_r2 = mu / r**2
    Rr2 = (R/r)**2
    Rr3 = (R/r)**3
    
    # evaluate accelerations
    ax = -mu_r3 * x - 3/2 * J2 * mu_r2 * Rr2 * (1 - 5*(z/r)**2) * x/r
    ax = ax + 1/2 * J3 * mu_r2 * Rr3 * 5 * (7 * (z/r)**3 - 3 * (z/r)) * x/r
    ay = -mu_r3 * y - 3/2 * J2 * mu_r2 * Rr2 * (1 - 5*(z/r)**2) * y/r
    ay = ay + 1/2 * J3 * mu_r2 * Rr3 * 5 * (7 * (z/r)**3 - 3 * (z/r)) * y/r
    az = -mu_r3 * z - 3/2 * J2 * mu_r2 * Rr2 * (3 - 5*(z/r)**2) * z/r
    az = az + 1/2 * J3 * mu_r2 * Rr3 * 3 * (1 - 10 * (z/r)**2 + 35/3 * (z/r)**4)
    
    # ODEs
    dydt[0] = dx
    dydt[1] = dy
    dydt[2] = dz
    dydt[3] = ax
    dydt[4] = ay
    dydt[5] = az
    
    # =========================================================================
    # deal with STM
    # =========================================================================
    # extract Phi
    Phi = np.reshape(yy[6:], (6,6))  # get 6x6 STM from 36x1 yy vector slice
    
    # evaluate Jacobian matrix at this time
    # _, evalJac = Jacobians.getdfdZ_wJ2()
    dfdZ = evalJac(yy[:3], yy[3:], mu, J2, J3, R)
    
    # matrix ODE
    dPhi = dfdZ @ Phi
    
    # flatten and assign dPhi
    dydt[6:] = np.reshape(dPhi, (36,))
    
    return dydt

def ODE_HW3_DMC(t, yy, mu, J2, J3, R, evalJac, tau):
    '''
    dynamics for mu + J2 + J3 spacecraft, no control.
    Note: does NOT treat J2 or J3 as a state variable.
    '''
    
    dydt = np.empty(90)
    dydt[:] = np.NaN
    
    # =========================================================================
    # deal with vector state
    # =========================================================================
    x = yy[0]
    y = yy[1]
    z = yy[2]
    dx = yy[3]
    dy = yy[4]
    dz = yy[5] 
    
    wvec = np.array([yy[6], yy[7], yy[8]])
    # wx = yy[6]
    # wy = yy[7]
    # wz = yy[8]
    
    # evaluate constants
    r = np.sqrt(x**2 + y**2 + z**2)
    mu_r3 = mu / r**3
    mu_r2 = mu / r**2
    Rr2 = (R/r)**2
    Rr3 = (R/r)**3
    
    # evaluate accelerations
    ax = -mu_r3 * x - 3/2 * J2 * mu_r2 * Rr2 * (1 - 5*(z/r)**2) * x/r
    ax = ax + 1/2 * J3 * mu_r2 * Rr3 * 5 * (7 * (z/r)**3 - 3 * (z/r)) * x/r
    ay = -mu_r3 * y - 3/2 * J2 * mu_r2 * Rr2 * (1 - 5*(z/r)**2) * y/r
    ay = ay + 1/2 * J3 * mu_r2 * Rr3 * 5 * (7 * (z/r)**3 - 3 * (z/r)) * y/r
    az = -mu_r3 * z - 3/2 * J2 * mu_r2 * Rr2 * (3 - 5*(z/r)**2) * z/r
    az = az + 1/2 * J3 * mu_r2 * Rr3 * 3 * (1 - 10 * (z/r)**2 + 35/3 * (z/r)**4)
    
    # perturbing accelerations
    Bw = 1/tau * np.eye(3)
    dw = -Bw @ wvec    
    
    # ODEs
    dydt[0] = dx
    dydt[1] = dy
    dydt[2] = dz
    dydt[3] = ax
    dydt[4] = ay
    dydt[5] = az
    dydt[6] = dw[0]
    dydt[7] = dw[1]
    dydt[8] = dw[2]
    
    # =========================================================================
    # deal with STM
    # =========================================================================
    # extract Phi
    Phi = np.reshape(yy[9:], (9,9))  # get 6x6 STM from 36x1 yy vector slice
    
    # evaluate Jacobian matrix at this time
    # _, evalJac = Jacobians.getdfdZ_wJ2()
    dfdZ = evalJac(yy[:3], yy[3:6], mu, J2, J3, R, tau)
    
    # matrix ODE
    dPhi = dfdZ @ Phi
    
    # flatten and assign dPhi
    dydt[9:] = np.reshape(dPhi, (81,))
    
    return dydt

def muJ2CD_wPhi(t, yy, params, evalJac):
    '''
    dynamics for mu + J2 + drag spacecraft, no control.
    STATE:
        r: spacecraft position (x, y, z)
        v: spacecraft inertial velocity (dx, dy, dz)
        mu: GM of central body
        J2: oblateness of central body
        CD: drag coefficient of spacecraft
        RSi: position vector for each ground station (xS1, xS2, xS3)
    '''
    
    Nstate = 9 + 3*params['Ngs']
    dydt = np.empty(len(yy))
    dydt[:] = np.NaN
    
    A = params['Area']
    m = params['m']
    rho0 = params['rho0']
    r0 = params['r0']
    H = params['H']
    R = params['R']
    omP = params['omP']
    Ngs = params['Ngs']
    
    # =========================================================================
    # deal with spacecraft vector states
    # =========================================================================
    x = yy[0]
    y = yy[1]
    z = yy[2]
    dx = yy[3]
    dy = yy[4]
    dz = yy[5]
    
    mu = yy[6]
    J2 = yy[7]
    CD = yy[8]
    
    # evaluate constants
    omvec = np.array([0,0,omP])
    rvec = np.array([x, y, z])
    r = np.linalg.norm(rvec)
    BC = m / (CD * A)
    
    uvec = np.array([dx, dy, dz]) - np.cross(omvec, rvec)
    u = np.linalg.norm(uvec)
    
    # evaluate accelerations
    amu = -mu / r**3 * np.array([x, y, z])
    aJ2 = -3/2 * J2 * mu/r**2 * (R/r)**2 * np.array([(1 - 5*(z/r)**2)*x/r,
                                                     (1 - 5*(z/r)**2)*y/r,
                                                     (3 - 5*(z/r)**2)*z/r])
    adrag = -1/2 * (rho0 * np.exp(-(r - r0) / H)) * u / BC * uvec
    a = amu + aJ2 + adrag
    
    # ODEs
    dydt[0] = dx
    dydt[1] = dy
    dydt[2] = dz
    dydt[3] = a[0]
    dydt[4] = a[1]
    dydt[5] = a[2]
    
    # =========================================================================
    # Constants (mu, J2, CD)
    # =========================================================================
    dydt[6] = 0
    dydt[7] = 0
    dydt[8] = 0
    
    # =========================================================================
    # station positions
    # =========================================================================
    sind = 9
    for si in range(Ngs):
        rSvec_N = yy[sind:sind+3]
        dydt[sind:sind+3] = np.cross(omvec, rSvec_N)
        sind += 3
    
    # =========================================================================
    # deal with STM
    # =========================================================================
    # extract Phi
    Phi = np.reshape(yy[Nstate:], (Nstate, Nstate))  # get STM from yy
    
    # evaluate Jacobian matrix at this time
    RSvecs = np.reshape(yy[9:(9+Ngs*3)], (Ngs, 3))
    dfdZ = evalJac(yy[:3], yy[3:6], mu, J2, CD, RSvecs, params)
    
    # matrix ODE
    dPhi = dfdZ @ Phi
    
    # flatten and assign dPhi
    dydt[Nstate:] = np.reshape(dPhi, (Nstate**2,))
    
    return dydt

















