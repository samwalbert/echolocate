# -*- coding: utf-8 -*-
"""
measurements.py:
    measurement model functions for Stat OD codebase
Created on Mon Jan 24 21:09:16 2022

@author: Samuel Albert
"""

import numpy as np

from src import astrodynamics as astro

def compute_range_rangerate(xxvec, coords, masks, theta0, t, R, D, df,
                            IDs = None):
    '''
    returns ideal range and range-rate observations of spacecraft with state
    xxvec from n ground stations at coords with elevation masks.
    INPUTS:
        xxvec: inertial position, velocity of spacecraft
        coords: nx2 array of longitude, latitude (degs) coordinates of stations
        masks: elevation mask for each station (deg)
        IDs: n-vector of strings with ID for each station, optional
        theta0: initial earth rotation angle 
        t: current time, s
        R: spherical radius for this planet, km
        D: length of rotation period for this planet, s
        df: list of observations to which new observations are appended
        IDs: optional, list of names for each station
    OUTPUTS:
        df: list of observations, each with range, range rate, EL, timestamp,
        ID, and index
    '''
    
    n = len(masks)
    if IDs == None:
        IDs = [''] * n
    
    # compute angular rotation rate from day length
    OmP = 2 * np.pi / D
    # OmP = 7.2921158553E-5 # hardcode rotation period to match MATLAB grader
    
    rvec_N = xxvec[:3]
    vvec_N = xxvec[3:]
    
    # loop through each station
    Nmeasurements = 0
    for i in range(n):
        # obs[i,3] = i
        
        lon = np.radians(coords[i,0])
        lat = np.radians(coords[i,1])
        
        # compute ECI position vector for station i
        theta = np.radians(theta0) + t * OmP
        rvecS_N = astro.LLR2rvec_N(R, lon, lat, theta)
        
        # compute elevation angle
        uhat = rvecS_N / np.linalg.norm(rvecS_N)
        rhovec = rvec_N - rvecS_N
        rhohat = rhovec / np.linalg.norm(rhovec)
        EL = np.degrees(np.arcsin(np.dot(uhat, rhohat))) # radians)
        
        # compute and return observations if EL > mask
        if (EL > masks[i]):
            Nmeasurements += 1
            
            # compute inertial velocity of station
            vvecS_N = np.cross(np.array([0, 0, OmP]), rvecS_N)
            
            # compute and report ideal range and range-rate
            rho = np.linalg.norm(rvec_N - rvecS_N)
            rhodot = np.dot((rvec_N - rvecS_N), (vvec_N - vvecS_N)) /rho
            
            # build data element
            dfi = {
                    'range'     : rho,
                    'rangerate' : rhodot,
                    'EL'        : EL,
                    't'         : t,
                    'ID'        : IDs[i],
                    'ind'       : i,
                    'x'         : rvecS_N[0],
                    'y'         : rvecS_N[1],
                    'z'         : rvecS_N[2],
                    'vx'        : vvecS_N[0],
                    'vy'        : vvecS_N[1],
                    'vz'        : vvecS_N[2]
            }
            df.append(dfi)
    
    return Nmeasurements

def compute_range_rangerate_single(xxvec, rvecS_N, vvecS_N):
    '''
    returns ideal range and range-rate observations of spacecraft with state
    xxvec from a single ground station at coords with elevation masks.
    INPUTS:
        xxvec: inertial position, velocity of spacecraft
        coords: nx2 array of longitude, latitude (degs) coordinates of stations
        masks: elevation mask for each station (deg)
        IDs: n-vector of strings with ID for each station, optional
        theta0: initial earth rotation angle 
        t: current time, s
        R: spherical radius for this planet, km
        D: length of rotation period for this planet, s
    OUTPUTS:
        rho: range, km
        rhodot: range rate, km/s
    '''
    
    rvec_N = xxvec[:3]
    vvec_N = xxvec[3:]
    
    # compute and report ideal range and range-rate
    rho = np.linalg.norm(rvec_N - rvecS_N)
    rhodot = np.dot((rvec_N - rvecS_N), (vvec_N - vvecS_N)) /rho
    
    return rho, rhodot

def compute_obs_P1(rvec_N, vvec_N, rvecS_N, params, t):
    '''
    returns ideal range and range-rate observations of spacecraft with state
    xxvec from a single ground station at coords with elevation masks.
    INPUTS:
        xxvec: inertial position, velocity of spacecraft
        xSvec: nx3 array of ECEF positions of stations, km
        masks: elevation mask for each station (deg)
        IDs: n-vector of strings with ID for each station, optional
        theta0: initial earth rotation angle 
        omP: planetary rotation rate, rad/s
        R: spherical radius for this planet, km
        t: current time, s
    OUTPUTS:
        rho: range
        rhodot: range rate
    '''
    
    # compute inertial velocity of station
    vvecS_N = np.cross(np.array([0, 0, params['omP']]), rvecS_N)
    
    # compute and report ideal range and range-rate
    rho = np.linalg.norm(rvec_N - rvecS_N)
    rhodot = np.dot((rvec_N - rvecS_N), (vvec_N - vvecS_N)) /rho
    
    return rho, rhodot






