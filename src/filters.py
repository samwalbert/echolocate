# -*- coding: utf-8 -*-
"""
filters.py:
    estimation filters functions for Stat OD codebase
Created on Mon Jan 31 10:59:42 2022

@author: Samuel Albert
"""

import sys
import time
import copy
import numpy as np
import sympy as sp
import numba as nb
from scipy.integrate import solve_ivp

from src import dynamics as dyn
from src import measurements as mes
from src import astrodynamics as astro
from src import Jacobians


def CKF_J2J3(obs, xxTruth, ICParams, constParams,
             NOISEON = True, evalA = None, evalH = None, verbose = True,
             tol = 1e-8, maxits = 10):
    '''
    classical Kalman filter for mu + J2 + J3 dynamics, range and range rate
    measurements. Can ignore J2/J3 by setting to zero. ICparams and constParams
    are unpacked below.
    
    NOISEON - if False, uses pure range and range rate measurements
    
    function computes evalA and evalH by default, but can pass in as args for
    faster repeated runtime.
    
    returns: time, residuals, state history, cov. history, total error history
    '''
    ICParams = copy.deepcopy(ICParams) # prevent xx0hat changing outside func.
    
    tic = time.time()
    
    mu = constParams['mu']
    J2 = constParams['J2']
    J3 = constParams['J3']
    R = constParams['R']
    
    t0 = ICParams['t0']
    xx0 = ICParams['xx0']
    P0 = ICParams['P0']
    R0 = ICParams['R0']
    
    # =========================================================================
    # Initialize variables
    # =========================================================================
    Nobs = len(obs)
    Nstate = len(xx0)
    Ny = 2 # range, range rate
    
    j = 0
    
    # =========================================================================
    # Generate necessary jacobian function handles
    # =========================================================================
    if evalA == None:
        _, evalA = Jacobians.getdfdZ()
    if evalH == None:
        _, evalH = Jacobians.getHtilde_sc_rho_rhod()
    
    # =========================================================================
    # =========================================================================
    # # Major iterations, j
    # =========================================================================
    # =========================================================================
    converged = False
    
    while (not converged and j < maxits):
        thist = np.empty(Nobs+1)
        thist[:] = np.NaN
        thist[0] = t0
    
        xxhist = np.empty((Nobs+1, Nstate))
        xxhist[:] = np.NaN
        xxhist[0,:] = xx0
    
        DELxxhistP = np.empty(xxhist.shape)
        DELxxhistP[:] = np.NaN
        DELxxhistP[0,:] = np.zeros(Nstate)
    
        DELxxhistM = np.empty(xxhist.shape)
        DELxxhistM[:] = np.NaN
    
        PhistP = np.empty((Nobs+1, Nstate, Nstate))
        PhistP[:] = np.NaN
        PhistP[0,:,:] = P0
    
        PhistM = np.empty((Nobs+1, Nstate, Nstate))
        PhistM[:] = np.NaN
    
        yhist = np.empty((Nobs+1, Ny))
        yhist[:] = np.NaN # note, yhist[0] stays equal to NaN
    
        Rhist = np.empty((Nobs+1, Ny, Ny))
        Rhist[:] = np.NaN
        Rhist[0,:,:] = R0
    
        rMhist = np.empty((Nobs+1, Ny))
        rMhist[:] = np.NaN
    
        rPhist = np.empty((Nobs+1, Ny))
        rPhist[:] = np.NaN
    
        rbase = np.empty((Nobs+1, Ny))
        rbase[:] = np.NaN
    
        Khist = np.empty((Nobs+1, Nstate, Ny))
        Khist[:] = np.NaN
        
        Phi_i_0 = np.eye(Nstate)
        
        # =====================================================================
        # # ===================================================================
        # # Minor iterations, i
        # # ===================================================================
        # =====================================================================
        # NOTE TO SELF: index "i" corresponds to the update based on measurement 
        # with index "i-1", such that index i = 0 corresponds to initialization,
        # i = 1 is the result of the first measurement update.
        for i in range(1,Nobs+1):
            # =================================================================
            # Read next observation
            # =================================================================
            thist[i] = obs['t'][i-1]
            if NOISEON:
                yhist[i,0] = obs['noisyrange'][i-1]
                yhist[i,1] = obs['noisyrate'][i-1]
            else:
                yhist[i,0] = obs['range'][i-1]
                yhist[i,1] = obs['rangerate'][i-1]
            Rhist[i,:,:] = R0
            
            rvecS = np.array([obs['x'][i-1], obs['y'][i-1], obs['z'][i-1]])
            vvecS = np.array([obs['vx'][i-1], obs['vy'][i-1], obs['vz'][i-1]])
            
            # =================================================================
            # Propagate and time update from t[i-1] to t[i] (or equate)
            # =================================================================
            if (thist[i] == thist[i-1]):
                # don't update xx, DELxxM, or PM
                xxhist[i,:] = xxhist[i-1,:]
                DELxxhistM[i,:] = DELxxhistP[i-1,:]
                PhistM[i,:,:] = PhistP[i-1,:,:]
            else:
                # numerically propagate nonlinear state
                IC = np.append(xxhist[i-1,:],
                               np.reshape(np.eye(Nstate), (Nstate**2,)))
                
                sol = solve_ivp(lambda t, yy: \
                                dyn.keplerJ2J3_wPhi_ODErv(t, yy, mu,
                                                          J2, J3, R,
                                                          evalA),
                                (thist[i-1], thist[i]), IC,
                                rtol = 1e-13, atol = 1e-13, method = 'RK45')
                xxhist[i,:] = sol.y[:6,-1]
                Phi = np.reshape(sol.y[6:,-1], (Nstate,Nstate))
                
                Phi_i_0 = Phi @ Phi_i_0
                
                # linearly update state deviation and covariance
                DELxxhistM[i,:] = Phi @ DELxxhistP[i-1,:]
                PhistM[i,:,:] = Phi @ PhistP[i-1,:,:] @ Phi.T # + process noise
                
            # =================================================================
            # Process observations
            # =================================================================
            # predict range and range rate measurements for curent state
            rho, rhodot = mes.compute_range_rangerate_single(xxhist[i,:],
                                                             rvecS, vvecS)
            yPred = np.array([rho, rhodot])
            
            # compute measurement sentivity matrix at this time
            Hi = evalH(xxhist[i,:3], xxhist[i,3:], rvecS, vvecS)
            
            # compute pre-update residual
            rbase[i,:] = yhist[i,:] - yPred
            rMhist[i,:] = rbase[i,:] - Hi @ DELxxhistM[i,:]
            
            # compute Kalman gain matrix
            Khist[i,:,:] = PhistM[i,:,:] @ Hi.T @ np.linalg.inv((Hi @ PhistM[i,:,:] @ Hi.T + Rhist[i,:,:]))
            # Khist[i,:,:] = np.linalg.solve(Hi @ PhistM[i,:,:] @ Hi.T + Rhist[i,:,:],
            #                                (PhistM[i,:,:] @ Hi.T).T).T
            
            # =================================================================
            # Measurement update
            # =================================================================
            # update estimate of state deviation
            DELxxhistP[i,:] = DELxxhistM[i,:] + Khist[i,:,:] @ rMhist[i,:]
            
            # compute post-update residual
            rPhist[i,:] = rbase[i,:] - Hi @ DELxxhistP[i,:]
            
            # update covariance using Jordan-Busy form
            PhistP[i,:,:] = (np.eye(Nstate) - Khist[i,:,:] @ Hi)\
                @ PhistM[i,:,:] @ (np.eye(Nstate) - Khist[i,:,:] @ Hi).T\
                    + Khist[i,:,:] @ Rhist[i,:,:] @ Khist[i,:,:].T
        
        # =====================================================================
        # Generate and print RMS values
        # =====================================================================
        xxHat = xxhist + DELxxhistP
        RMS_scalar = np.empty(Nstate)
        for i in range(Nstate):
            RMS_scalar[i] = np.sqrt(((xxHat[:,i] - xxTruth[:,i])**2).sum()\
                / xxHat.shape[0])
        RMS_r = np.sqrt(((xxHat[:,:3] - xxTruth[:,:3])**2).sum() / xxHat.shape[0])
        RMS_v = np.sqrt(((xxHat[:,3:] - xxTruth[:,3:])**2).sum() / xxHat.shape[0])
        RMS_rrho = np.sqrt((rPhist[1:,0]**2).sum() / xxHat.shape[0])
        RMS_rrate = np.sqrt((rPhist[1:,1]**2).sum() / xxHat.shape[0])
        
        # =====================================================================
        # Compute error, check for convergence
        # =====================================================================
        err = np.linalg.norm(DELxxhistP[-1,:])
        if np.linalg.norm(err) < tol:
            converged = True
        else:
            # update NL trajectory for next major iteration
            xx0 += np.linalg.inv(Phi_i_0) @ DELxxhistP[-1,:]
        
        toc = time.time()
        
        if verbose:
            print('-'*100)
            print('CKF completed major iteration j = {0:d} after {1:.0f} seconds'\
                  ', error = {2:.3e}.'.format(j, toc-tic, err))
            print('RMS Values:')
            print('    pos: {0:.5e} km'.format(RMS_r))
            print('        x: {0:.5e} km, y: {1:.5e} km, z: {2:.5e} km'\
                  .format(RMS_scalar[0], RMS_scalar[1], RMS_scalar[1]))
            print('    vel: {0:.5e} km/s'.format(RMS_v))
            print('        vx: {0:.5e} km/s, vy: {1:.5e} km/s, vz: {2:.5e} km/s'\
                  .format(RMS_scalar[3], RMS_scalar[4], RMS_scalar[5]))
            print('    range: {0:.5e} km'.format(RMS_rrho))
            print('    range rate: {0:.5e} km/s'.format(RMS_rrate))
            print('-'*100)
        
        # increment major iterations
        j += 1
    
    
    return thist, xxHat, PhistP, rPhist

def CKF_P1(obs, ICParams, constParams, evalA = None, evalH = None, Ngs = 3,
           verbose = True, tol = 1e-8, maxits = 10):
    '''
    classical batch filter for project 1. ICparams and constParams
    are unpacked below.
    STATE:
        r: spacecraft position (x, y, z)
        v: spacecraft inertial velocity (dx, dy, dz)
        mu: GM of central body
        J2: oblateness of central body
        CD: drag coefficient of spacecraft
        RSi: position vector for each ground station (xS1, xS2, xS3)
    DYNAMICS:
        mu, J2, and drag affect spacecraft.
        no dynamics for constants (mu, J2, CD)
        each ground station is fixed to earth at some location
    
    function computes evalA and evalH by default, but can pass in as args for
    faster repeated runtime.
    
    returns: time, residuals, state history, cov. history, total error history
    '''
    ICParams = copy.deepcopy(ICParams) # prevent xx0hat changing outside func.
    
    tic = time.time()
    
    t0 = ICParams['t0']
    xx0 = ICParams['xx0hat']
    P0 = ICParams['P0']
    R0 = ICParams['R0']
    
    # =========================================================================
    # Initialize variables
    # =========================================================================
    Nobs = len(obs)
    Nstate = len(xx0)
    Ny = 2 # range, range rate
    
    j = 0
    
    xx0hat = xx0
    
    # =========================================================================
    # Generate necessary jacobian function handles
    # =========================================================================
    if evalA == None:
        _, evalA = Jacobians.getdfdZ_P1(Ngs = Ngs)
    if evalH == None:
        _, evalH = Jacobians.getH_P1()
    
    # =========================================================================
    # =========================================================================
    # # Major iterations, j
    # =========================================================================
    # =========================================================================
    converged = False
    
    while (not converged and j < maxits):
        thist = np.empty(Nobs+1)
        thist[:] = np.NaN
        thist[0] = t0
    
        xxhist = np.empty((Nobs+1, Nstate))
        xxhist[:] = np.NaN
        xxhist[0,:] = xx0hat
        # print('initializing xx0hat: ')
        # print(xx0hat)
        print()
    
        DELxxhistP = np.empty(xxhist.shape)
        DELxxhistP[:] = np.NaN
        DELxxhistP[0,:] = np.zeros(Nstate)
    
        DELxxhistM = np.empty(xxhist.shape)
        DELxxhistM[:] = np.NaN
    
        PhistP = np.empty((Nobs+1, Nstate, Nstate))
        PhistP[:] = np.NaN
        PhistP[0,:,:] = P0
    
        PhistM = np.empty((Nobs+1, Nstate, Nstate))
        PhistM[:] = np.NaN
    
        yhist = np.empty((Nobs+1, Ny))
        yhist[:] = np.NaN # note, yhist[0] stays equal to NaN
    
        Rhist = np.empty((Nobs+1, Ny, Ny))
        Rhist[:] = np.NaN
        Rhist[0,:,:] = R0
    
        rMhist = np.empty((Nobs+1, Ny))
        rMhist[:] = np.NaN
    
        rPhist = np.empty((Nobs+1, Ny))
        rPhist[:] = np.NaN
    
        rbase = np.empty((Nobs+1, Ny))
        rbase[:] = np.NaN
    
        Khist = np.empty((Nobs+1, Nstate, Ny))
        Khist[:] = np.NaN
        
        Phi_i_0 = np.eye(Nstate)
        
        Phihist = np.empty((Nobs+1, Nstate, Nstate))
        Phihist[:] = np.NaN
        Phihist[0,:,:] = np.eye(Nstate)
        
        # =====================================================================
        # # ===================================================================
        # # Minor iterations, i
        # # ===================================================================
        # =====================================================================
        # NOTE TO SELF: index "i" corresponds to the update based on measurement 
        # with index "i-1", such that index i = 0 corresponds to initialization,
        # i = 1 is the result of the first measurement update.
        for i in range(1,Nobs+1):
            # =================================================================
            # Read next observation
            # =================================================================
            thist[i] = obs['t'][i-1]
            yhist[i,0] = obs['range'][i-1]
            yhist[i,1] = obs['rangerate'][i-1]
            Rhist[i,:,:] = R0
            
            # set index of station based on ID number
            if obs['ID'][i-1] == 101:
                obsInd = 0
            elif obs['ID'][i-1] == 337:
                obsInd = 1
            elif obs['ID'][i-1] == 394:
                obsInd = 2
            else:
                sys.exit('Error: station ID not recognized')
            
            # =================================================================
            # Propagate and time update from t[i-1] to t[i] (or equate)
            # =================================================================
            if (thist[i] == thist[i-1]):
                # don't update xx, DELxxM, or PM
                xxhist[i,:] = xxhist[i-1,:]
                DELxxhistM[i,:] = DELxxhistP[i-1,:]
                PhistM[i,:,:] = PhistP[i-1,:,:]
                Phihist[i,:,:] = np.eye(Nstate)
            else:
                # numerically propagate nonlinear state
                IC = np.append(xxhist[i-1,:],
                               np.reshape(np.eye(Nstate), (Nstate**2,)))
                
                sol = solve_ivp(lambda t, yy: \
                                dyn.muJ2CD_wPhi(t, yy, constParams, evalA),
                                (thist[i-1], thist[i]), IC,
                                rtol = 1e-13, atol = 1e-13, method = 'RK45')
                xxhist[i,:] = sol.y[:Nstate,-1]
                Phi = np.reshape(sol.y[Nstate:,-1], (Nstate,Nstate))
                Phihist[i,:,:] = Phi
                
                Phi_i_0 = Phi @ Phi_i_0
                
                # linearly update state deviation and covariance
                DELxxhistM[i,:] = Phi @ DELxxhistP[i-1,:]
                PhistM[i,:,:] = Phi @ PhistP[i-1,:,:] @ Phi.T # + process noise
                
            # =================================================================
            # Process observations
            # =================================================================
            # predict range and range rate measurements for curent state
            sii = 9 + obsInd * 3
            rho, rhodot = mes.compute_obs_P1(xxhist[i,:3], xxhist[i,3:6],
                                             xxhist[i,sii:(sii+3)],
                                             constParams, thist[i])
            yPred = np.array([rho, rhodot])
            
            # compute measurement sentivity matrix at this time
            Hi = evalH(xxhist[i,:], constParams, obsInd, Ngs)
            
            # compute pre-update residual
            rbase[i,:] = yhist[i,:] - yPred
            rMhist[i,:] = rbase[i,:] - Hi @ DELxxhistM[i,:]
            
            # compute Kalman gain matrix
            Khist[i,:,:] = PhistM[i,:,:] @ Hi.T\
                @ np.linalg.pinv((Hi @ PhistM[i,:,:] @ Hi.T + Rhist[i,:,:]))
            # Khist[i,:,:] = np.linalg.solve(Hi @ PhistM[i,:,:] @ Hi.T + Rhist[i,:,:],
            #                                 (PhistM[i,:,:] @ Hi.T).T).T
            
            # =================================================================
            # Measurement update
            # =================================================================
            # update estimate of state deviation
            DELxxhistP[i,:] = DELxxhistM[i,:] + Khist[i,:,:] @ rMhist[i,:]
            
            # compute post-update residual
            rPhist[i,:] = rbase[i,:] - Hi @ DELxxhistP[i,:]
            
            # update covariance using Jordan-Busy form
            PhistP[i,:,:] = (np.eye(Nstate) - Khist[i,:,:] @ Hi)\
                @ PhistM[i,:,:] @ (np.eye(Nstate) - Khist[i,:,:] @ Hi).T\
                    + Khist[i,:,:] @ Rhist[i,:,:] @ Khist[i,:,:].T
        
        # =====================================================================
        # Compute error, check for convergence
        # =====================================================================
        xxHat = xxhist + DELxxhistP
        err = np.linalg.norm(DELxxhistP[-1,:])
        if np.linalg.norm(err) < tol:
            converged = True
        else:
            # update NL trajectory for next major iteration
            updateX = np.linalg.solve(Phi_i_0, DELxxhistP[-1,:])
            xx0hat += updateX
        
        RMS_rrho = np.sqrt((rPhist[1:,0]**2).sum() / xxHat.shape[0])
        RMS_rrate = np.sqrt((rPhist[1:,1]**2).sum() / xxHat.shape[0])
        
        toc = time.time()
        
        if verbose:
            print('-'*100)
            print('CKF completed major iteration j = {0:d} after {1:.0f} seconds'\
                  ', error = {2:.3e}.'.format(j, toc-tic, err))
            print('RMS Values:')
            print('    range: {0:.5e} km'.format(RMS_rrho))
            print('    range rate: {0:.5e} km/s'.format(RMS_rrate))
            print('-'*100)
        
        # increment major iterations
        j += 1
    
    if (not converged) and verbose:
        print('*'*100)
        print('WARNING: CKF did not converge to within tol of {0:.3e}'\
              .format(tol))
        print('*'*100)
    
    return thist, xxHat, PhistP, rPhist

def CKF_HW3_SNC(obs, xxTruth, ICParams, constParams, Qct, minDt = 10,
             evalA = None, evalH = None, evalQ = None, verbose = True,
             tol = 1e-8, maxits = 10):
    '''
    classical Kalman filter for HW3, using SNC. ICparams and constParams
    are unpacked below.
    STATE:
        r: spacecraft position (x, y, z)
        v: spacecraft inertial velocity (dx, dy, dz)
    DYNAMICS:
        mu, J2, and J3 affect spacecraft. Just set J3 = 0 in filter to ignore.
        no dynamics for constants (mu, J2, CD).
        each ground station is fixed to earth at some location.
    
    function computes evalA and evalH by default, but can pass in as args for
    faster repeated runtime.
    
    Compute evalQ for inertial Qct by default, or accepts as input
    
    returns: time, residuals, state history, cov. history, total error history
    '''
    ICParams = copy.deepcopy(ICParams) # prevent xx0hat changing outside func.
    
    tic = time.time()
    
    mu = constParams['mu']
    J2 = constParams['J2']
    J3 = constParams['J3']
    R = constParams['R']
    
    t0 = ICParams['t0']
    xx0 = ICParams['xx0']
    P0 = ICParams['P0']
    R0 = ICParams['R0']
    
    # =========================================================================
    # Initialize variables
    # =========================================================================
    Nobs = len(obs)
    Nstate = len(xx0)
    Ny = 2 # range, range rate
    
    j = 0
    
    # =========================================================================
    # Generate necessary jacobian function handles
    # =========================================================================
    if evalA == None:
        _, evalA = Jacobians.getdfdZ()
    if evalH == None:
        _, evalH = Jacobians.getHtilde_sc_rho_rhod()
    if evalQ == None:
        evalQ = getQ_HW3_inertial()
    
    # =========================================================================
    # =========================================================================
    # # Major iterations, j
    # =========================================================================
    # =========================================================================
    converged = False
    
    while (not converged and j < maxits):
        thist = np.empty(Nobs+1)
        thist[:] = np.NaN
        thist[0] = t0
    
        xxhist = np.empty((Nobs+1, Nstate))
        xxhist[:] = np.NaN
        xxhist[0,:] = xx0
    
        DELxxhistP = np.empty(xxhist.shape)
        DELxxhistP[:] = np.NaN
        DELxxhistP[0,:] = np.zeros(Nstate)
    
        DELxxhistM = np.empty(xxhist.shape)
        DELxxhistM[:] = np.NaN
    
        PhistP = np.empty((Nobs+1, Nstate, Nstate))
        PhistP[:] = np.NaN
        PhistP[0,:,:] = P0
    
        PhistM = np.empty((Nobs+1, Nstate, Nstate))
        PhistM[:] = np.NaN
    
        yhist = np.empty((Nobs+1, Ny))
        yhist[:] = np.NaN # note, yhist[0] stays equal to NaN
    
        Rhist = np.empty((Nobs+1, Ny, Ny))
        Rhist[:] = np.NaN
        Rhist[0,:,:] = R0
    
        rMhist = np.empty((Nobs+1, Ny))
        rMhist[:] = np.NaN
    
        rPhist = np.empty((Nobs+1, Ny))
        rPhist[:] = np.NaN
    
        rbase = np.empty((Nobs+1, Ny))
        rbase[:] = np.NaN
    
        Khist = np.empty((Nobs+1, Nstate, Ny))
        Khist[:] = np.NaN
        
        Phi_i_0 = np.eye(Nstate)
        
        # =====================================================================
        # # ===================================================================
        # # Minor iterations, i
        # # ===================================================================
        # =====================================================================
        # NOTE TO SELF: index "i" corresponds to the update based on
        # measurement with index "i-1", such that index i = 0 corresponds to
        # initialization, i = 1 is the result of the first measurement update.
        for i in range(1,Nobs+1):
            # =================================================================
            # Read next observation
            # =================================================================
            thist[i] = obs['t'][i-1]
            yhist[i,0] = obs['noisyrange'][i-1]
            yhist[i,1] = obs['noisyrate'][i-1]
            Rhist[i,:,:] = R0
            
            rvecS = np.array([obs['x'][i-1], obs['y'][i-1], obs['z'][i-1]])
            vvecS = np.array([obs['vx'][i-1], obs['vy'][i-1], obs['vz'][i-1]])
            
            # =================================================================
            # Propagate and time update from t[i-1] to t[i] (or equate)
            # =================================================================
            if (thist[i] == thist[i-1]):
                # don't update xx, DELxxM, or PM. No process noise.
                xxhist[i,:] = xxhist[i-1,:]
                DELxxhistM[i,:] = DELxxhistP[i-1,:]
                PhistM[i,:,:] = PhistP[i-1,:,:]
            else:
                tprev = thist[i-1]
                xxiprev = xxhist[i-1,:]
                DELxxiPrev = DELxxhistP[i-1, :]
                PPrev = PhistP[i-1, :, :]
                tspan = thist[i] - thist[i-1]
                t_minor_arc = np.linspace(tprev, thist[i],
                                          int(np.ceil(tspan/minDt))+1)[1:]
                for ti in t_minor_arc:
                    # numerically propagate nonlinear state
                    IC = np.append(xxiprev,
                                   np.reshape(np.eye(Nstate), (Nstate**2,)))
                    
                    sol = solve_ivp(lambda t, yy: \
                                    dyn.keplerJ2J3_wPhi_ODErv(t, yy, mu,
                                                              J2, J3, R,
                                                              evalA),
                                    (tprev, ti), IC,
                                    rtol = 1e-13, atol = 1e-13)
                    xxi = sol.y[:6,-1]
                    Phii = np.reshape(sol.y[6:,-1], (Nstate,Nstate))
                    
                    Phi_i_0 = Phii @ Phi_i_0
                    
                    # evaluate process noise (SNC)
                    ON = astro.getON(xxi[:3], xxi[3:6])
                    Qdt = evalQ(ti - tprev, Qct, ON)
                    
                    # linearly update state deviation and covariance
                    DELxxiM = Phii @ DELxxiPrev
                    PiM = Phii @ PPrev @ Phii.T + Qdt
                    
                    # update previous values
                    tprev = ti
                    xxiprev = xxi
                    DELxxiPrev = DELxxiM
                    PPrev = PiM
                
                # assign updated values to actual arrays
                xxhist[i,:] = xxi
                DELxxhistM[i,:] = DELxxiM
                PhistM[i,:,:] = PiM
                
            # =================================================================
            # Process observations
            # =================================================================
            # predict range and range rate measurements for curent state
            rho, rhodot = mes.compute_range_rangerate_single(xxhist[i,:],
                                                             rvecS, vvecS)
            yPred = np.array([rho, rhodot])
            
            # compute measurement sentivity matrix at this time
            Hi = evalH(xxhist[i,:3], xxhist[i,3:], rvecS, vvecS)
            
            # compute pre-update residual
            rbase[i,:] = yhist[i,:] - yPred
            rMhist[i,:] = rbase[i,:] - Hi @ DELxxhistM[i,:]
            
            # compute Kalman gain matrix
            Khist[i,:,:] = PhistM[i,:,:] @ Hi.T\
                @ np.linalg.inv((Hi @ PhistM[i,:,:] @ Hi.T + Rhist[i,:,:]))
            
            # =================================================================
            # Measurement update
            # =================================================================
            # update estimate of state deviation
            DELxxhistP[i,:] = DELxxhistM[i,:] + Khist[i,:,:] @ rMhist[i,:]
            
            # compute post-update residual
            rPhist[i,:] = rbase[i,:] - Hi @ DELxxhistP[i,:]
            
            # update covariance using Jordan-Busy form
            PhistP[i,:,:] = (np.eye(Nstate) - Khist[i,:,:] @ Hi)\
                @ PhistM[i,:,:] @ (np.eye(Nstate) - Khist[i,:,:] @ Hi).T\
                    + Khist[i,:,:] @ Rhist[i,:,:] @ Khist[i,:,:].T
        
        # =====================================================================
        # Generate and print RMS values
        # =====================================================================
        xxHat = xxhist + DELxxhistP
        RMS_scalar = np.empty(Nstate)
        for i in range(Nstate):
            RMS_scalar[i] = np.sqrt(((xxHat[:,i] - xxTruth[:,i])**2).sum()\
                / xxHat.shape[0])
        RMS_r = np.sqrt(((xxHat[:,:3] - xxTruth[:,:3])**2).sum()\
                        / xxHat.shape[0])
        RMS_v = np.sqrt(((xxHat[:,3:] - xxTruth[:,3:])**2).sum()\
                        / xxHat.shape[0])
        RMS_rrho = np.sqrt((rPhist[1:,0]**2).sum() / xxHat.shape[0])
        RMS_rrate = np.sqrt((rPhist[1:,1]**2).sum() / xxHat.shape[0])
        RMSList = [RMS_r, RMS_v, RMS_rrho, RMS_rrate]
        
        # =====================================================================
        # Compute error, check for convergence
        # =====================================================================
        err = np.linalg.norm(DELxxhistP[-1,:])
        if np.linalg.norm(err) < tol:
            converged = True
        else:
            # update NL trajectory for next major iteration
            xx0 += np.linalg.inv(Phi_i_0) @ DELxxhistP[-1,:]
        
        toc = time.time()
        
        if verbose:
            print('-'*100)
            print('CKF completed major iteration j = {0:d} after {1:.0f}'\
                  ' seconds, error = {2:.3e}.'.format(j, toc-tic, err))
            print('RMS Values:')
            print('    pos: {0:.5e} km'.format(RMS_r))
            print('        x: {0:.5e} km, y: {1:.5e} km, z: {2:.5e} km'\
                  .format(RMS_scalar[0], RMS_scalar[1], RMS_scalar[1]))
            print('    vel: {0:.5e} km/s'.format(RMS_v))
            print('        vx: {0:.5e} km/s, vy: {1:.5e} km/s, vz:'\
                  ' {2:.5e} km/s'.format(RMS_scalar[3], RMS_scalar[4],
                                         RMS_scalar[5]))
            print('    range: {0:.5e} km'.format(RMS_rrho))
            print('    range rate: {0:.5e} km/s'.format(RMS_rrate))
            print('-'*100)
        
        # increment major iterations
        j += 1
    
    
    return thist, xxHat, PhistP, rPhist, RMSList

def CKF_HW3_DMC(obs, xxTruth, ICParams, constParams, Qct, minDt = 10,
             evalA = None, evalH = None, verbose = True,
             tol = 1e-8, maxits = 10):
    '''
    classical Kalman filter for HW3, using DMC. ICparams and constParams
    are unpacked below.
    STATE:
        r: spacecraft position (x, y, z)
        v: spacecraft inertial velocity (dx, dy, dz)
        w: deterministic component of perturbing (unmodeled) acceleration (DMC)
    DYNAMICS:
        mu, J2, and J3 affect spacecraft. Just set J3 = 0 in filter to ignore.
        no dynamics for constants (mu, J2, CD).
        each ground station is fixed to earth at some location.
    
    function computes evalA and evalH by default, but can pass in as args for
    faster repeated runtime.
    
    Compute evalQ for inertial Qct by default, or accepts as input
    
    returns: time, residuals, state history, cov. history, total error history
    '''
    ICParams = copy.deepcopy(ICParams) # prevent xx0hat changing outside func.
    
    tic = time.time()
    
    mu = constParams['mu']
    J2 = constParams['J2']
    J3 = constParams['J3']
    R = constParams['R']
    
    t0 = ICParams['t0']
    xx0 = ICParams['xx0']
    P0 = ICParams['P0']
    R0 = ICParams['R0']
    tau = ICParams['tau']
    
    # =========================================================================
    # Initialize variables
    # =========================================================================
    Nobs = len(obs)
    Nstate = len(xx0)
    Ny = 2 # range, range rate
    
    j = 0
    
    # =========================================================================
    # Generate necessary jacobian function handles
    # =========================================================================
    if evalA == None:
        _, evalA = Jacobians.getdfdZ()
    if evalH == None:
        _, evalH = Jacobians.getHtilde_sc_rho_rhod()
    
    # augment matrices to account for DMC
    def evalAz(rvec, vvec, mu, J2, J3, R, tau):
        A = evalA(rvec, vvec, mu, J2, J3, R)
        Bw = 1/tau * np.eye(3)
        UR = np.block([[np.zeros((3,3))],[np.eye(3)]])
        return np.block([[A, UR], [np.zeros((3,6)), -Bw]])
    
    def evalHz(rvec, vvec, rvecS, vvecS):
        H = evalH(rvec, vvec, rvecS, vvecS)
        return np.block([H, np.zeros((2,3))])
    
    # =========================================================================
    # =========================================================================
    # # Major iterations, j
    # =========================================================================
    # =========================================================================
    converged = False
    
    while (not converged and j < maxits):
        thist = np.empty(Nobs+1)
        thist[:] = np.NaN
        thist[0] = t0
    
        xxhist = np.empty((Nobs+1, Nstate))
        xxhist[:] = np.NaN
        xxhist[0,:] = xx0
    
        DELxxhistP = np.empty(xxhist.shape)
        DELxxhistP[:] = np.NaN
        DELxxhistP[0,:] = np.zeros(Nstate)
    
        DELxxhistM = np.empty(xxhist.shape)
        DELxxhistM[:] = np.NaN
    
        PhistP = np.empty((Nobs+1, Nstate, Nstate))
        PhistP[:] = np.NaN
        PhistP[0,:,:] = P0
    
        PhistM = np.empty((Nobs+1, Nstate, Nstate))
        PhistM[:] = np.NaN
    
        yhist = np.empty((Nobs+1, Ny))
        yhist[:] = np.NaN # note, yhist[0] stays equal to NaN
    
        Rhist = np.empty((Nobs+1, Ny, Ny))
        Rhist[:] = np.NaN
        Rhist[0,:,:] = R0
    
        rMhist = np.empty((Nobs+1, Ny))
        rMhist[:] = np.NaN
    
        rPhist = np.empty((Nobs+1, Ny))
        rPhist[:] = np.NaN
    
        rbase = np.empty((Nobs+1, Ny))
        rbase[:] = np.NaN
    
        Khist = np.empty((Nobs+1, Nstate, Ny))
        Khist[:] = np.NaN
        
        Phi_i_0 = np.eye(Nstate)
        
        # =====================================================================
        # # ===================================================================
        # # Minor iterations, i
        # # ===================================================================
        # =====================================================================
        # NOTE TO SELF: index "i" corresponds to the update based on
        # measurement with index "i-1", such that index i = 0 corresponds to
        # initialization, i = 1 is the result of the first measurement update.
        for i in range(1,Nobs+1):
            # =================================================================
            # Read next observation
            # =================================================================
            thist[i] = obs['t'][i-1]
            yhist[i,0] = obs['noisyrange'][i-1]
            yhist[i,1] = obs['noisyrate'][i-1]
            Rhist[i,:,:] = R0
            
            rvecS = np.array([obs['x'][i-1], obs['y'][i-1], obs['z'][i-1]])
            vvecS = np.array([obs['vx'][i-1], obs['vy'][i-1], obs['vz'][i-1]])
            
            # =================================================================
            # Propagate and time update from t[i-1] to t[i] (or equate)
            # =================================================================
            if (thist[i] == thist[i-1]):
                # don't update xx, DELxxM, or PM. No process noise.
                xxhist[i,:] = xxhist[i-1,:]
                DELxxhistM[i,:] = DELxxhistP[i-1,:]
                PhistM[i,:,:] = PhistP[i-1,:,:]
            else:
                tprev = thist[i-1]
                xxiprev = xxhist[i-1,:]
                DELxxiPrev = DELxxhistP[i-1, :]
                PPrev = PhistP[i-1, :, :]
                tspan = thist[i] - thist[i-1]
                t_minor_arc = np.linspace(tprev, thist[i],
                                          int(np.ceil(tspan/minDt))+1)[1:]
                for ti in t_minor_arc:
                    # numerically propagate nonlinear state
                    IC = np.append(xxiprev,
                                   np.reshape(np.eye(Nstate), (Nstate**2,)))
                    
                    sol = solve_ivp(lambda t, yy: \
                                    dyn.ODE_HW3_DMC(t, yy, mu, J2, J3, R,
                                                    evalAz, tau),
                                    (tprev, ti), IC,
                                    rtol = 1e-12, atol = 1e-12)
                    xxi = sol.y[:Nstate,-1]
                    Phii = np.reshape(sol.y[Nstate:,-1], (Nstate,Nstate))
                    
                    Phi_i_0 = Phii @ Phi_i_0
                    
                    # evaluate process noise (DMC)
                    Qdt = evalQ_HW3_DMC(Qct, tau, minDt)
                    
                    # linearly update state deviation and covariance
                    DELxxiM = Phii @ DELxxiPrev
                    PiM = Phii @ PPrev @ Phii.T + Qdt
                    
                    # update previous values
                    tprev = ti
                    xxiprev = xxi
                    DELxxiPrev = DELxxiM
                    PPrev = PiM
                
                # assign updated values to actual arrays
                xxhist[i,:] = xxi
                DELxxhistM[i,:] = DELxxiM
                PhistM[i,:,:] = PiM
                
            # =================================================================
            # Process observations
            # =================================================================
            # predict range and range rate measurements for curent state
            rho, rhodot = mes.compute_range_rangerate_single(xxhist[i,:6],
                                                             rvecS, vvecS)
            yPred = np.array([rho, rhodot])
            
            # compute measurement sentivity matrix at this time
            Hi = evalHz(xxhist[i,:3], xxhist[i,3:6], rvecS, vvecS)
            
            # compute pre-update residual
            rbase[i,:] = yhist[i,:] - yPred
            rMhist[i,:] = rbase[i,:] - Hi @ DELxxhistM[i,:]
            
            # compute Kalman gain matrix
            Khist[i,:,:] = PhistM[i,:,:] @ Hi.T\
                @ np.linalg.inv((Hi @ PhistM[i,:,:] @ Hi.T + Rhist[i,:,:]))
            
            # =================================================================
            # Measurement update
            # =================================================================
            # update estimate of state deviation
            DELxxhistP[i,:] = DELxxhistM[i,:] + Khist[i,:,:] @ rMhist[i,:]
            
            # compute post-update residual
            rPhist[i,:] = rbase[i,:] - Hi @ DELxxhistP[i,:]
            
            # update covariance using Jordan-Busy form
            PhistP[i,:,:] = (np.eye(Nstate) - Khist[i,:,:] @ Hi)\
                @ PhistM[i,:,:] @ (np.eye(Nstate) - Khist[i,:,:] @ Hi).T\
                    + Khist[i,:,:] @ Rhist[i,:,:] @ Khist[i,:,:].T
        
        # =====================================================================
        # Generate and print RMS values
        # =====================================================================
        xxHat = xxhist + DELxxhistP
        RMS_scalar = np.empty(6)
        for i in range(6):
            RMS_scalar[i] = np.sqrt(((xxHat[:,i] - xxTruth[:,i])**2).sum()\
                / xxHat.shape[0])
        RMS_r = np.sqrt(((xxHat[:,:3] - xxTruth[:,:3])**2).sum()\
                        / xxHat.shape[0])
        RMS_v = np.sqrt(((xxHat[:,3:6] - xxTruth[:,3:])**2).sum()\
                        / xxHat.shape[0])
        RMS_rrho = np.sqrt((rPhist[1:,0]**2).sum() / xxHat.shape[0])
        RMS_rrate = np.sqrt((rPhist[1:,1]**2).sum() / xxHat.shape[0])
        RMSList = [RMS_r, RMS_v, RMS_rrho, RMS_rrate]
        
        # =====================================================================
        # Compute error, check for convergence
        # =====================================================================
        err = np.linalg.norm(DELxxhistP[-1,:])
        if np.linalg.norm(err) < tol:
            converged = True
        else:
            # update NL trajectory for next major iteration
            xx0 += np.linalg.inv(Phi_i_0) @ DELxxhistP[-1,:]
        
        toc = time.time()
        
        if verbose:
            print('-'*100)
            print('CKF completed major iteration j = {0:d} after {1:.0f}'\
                  ' seconds, error = {2:.3e}.'.format(j, toc-tic, err))
            print('RMS Values:')
            print('    pos: {0:.5e} km'.format(RMS_r))
            print('        x: {0:.5e} km, y: {1:.5e} km, z: {2:.5e} km'\
                  .format(RMS_scalar[0], RMS_scalar[1], RMS_scalar[1]))
            print('    vel: {0:.5e} km/s'.format(RMS_v))
            print('        vx: {0:.5e} km/s, vy: {1:.5e} km/s, vz:'\
                  ' {2:.5e} km/s'.format(RMS_scalar[3], RMS_scalar[4],
                                         RMS_scalar[5]))
            print('    range: {0:.5e} km'.format(RMS_rrho))
            print('    range rate: {0:.5e} km/s'.format(RMS_rrate))
            print('-'*100)
        
        # increment major iterations
        j += 1
    
    
    return thist, xxHat, PhistP, rPhist, RMSList

def EKF_J2J3(obs, xxTruth, ICParams, constParams, coords,
             NOISEON = True, evalA = None, evalH = None, NCKF = 0):
    '''
    extended Kalman filter for mu + J2 + J3 dynamics, range and range rate
    measurements. Can ignore J2/J3 by setting to zero. ICparams and constParams
    are unpacked below.
    
    NOISEON - if False, uses pure range and range rate measurements
    
    function computes evalA and evalH by default, but can pass in as args for
    faster repeated runtime.
    
    returns: time, residuals, state history, cov. history, total error history
    '''
    tic = time.time()
    
    mu = constParams['mu']
    J2 = constParams['J2']
    J3 = constParams['J3']
    R = constParams['R']
    D = constParams['D']
    theta0 = constParams['theta0']
    
    t0 = ICParams['t0']
    xx0 = ICParams['xx0']
    xx0hat = ICParams['xx0hat']
    P0 = ICParams['P0']
    R0 = ICParams['R0']
    
    # =========================================================================
    # # =======================================================================
    # # Warm start using CKF
    # # =======================================================================
    # =========================================================================
    DELxx0 = xx0hat - xx0
    ICParams_CKF = {
        't0'    : t0,
        'xx0'   : xx0,
        'P0'    : P0,
        'DELxx0': DELxx0,
        'R0'    : R0
        }
    thistCKF, xxHatCKF, PhistPCKF, rPhistCKF\
        = CKF_J2J3(obs.iloc[:NCKF], xxTruth[:NCKF+1], ICParams_CKF, constParams, coords,
                   verbose = False)
    
    t0 = thistCKF[-1]
    xx0hat = xxHatCKF[-1,:]
    P0 = PhistPCKF[-1,:,:]
    obs = obs.iloc[NCKF:].copy()
    obs.reset_index(drop=True, inplace=True)
    
    # =========================================================================
    # =========================================================================
    # # EKF
    # =========================================================================
    # =========================================================================
    
    # =========================================================================
    # Initialize
    # =========================================================================
    Nobs = len(obs)
    Nstate = len(xx0)
    Ny = 2 # range, range rate
    
    thist = np.empty(Nobs+1)
    thist[:] = np.NaN
    thist[0] = t0

    xxhathistP = np.empty((Nobs+1, Nstate))
    xxhathistP[:] = np.NaN
    xxhathistP[0,:] = xx0hat

    xxhathistM = np.empty((Nobs+1, Nstate))
    xxhathistM[:] = np.NaN

    PhistP = np.empty((Nobs+1, Nstate, Nstate))
    PhistP[:] = np.NaN
    PhistP[0,:,:] = P0

    PhistM = np.empty((Nobs+1, Nstate, Nstate))
    PhistM[:] = np.NaN

    yhist = np.empty((Nobs+1, Ny))
    yhist[:] = np.NaN # note, yhist[0] stays equal to NaN

    Rhist = np.empty((Nobs+1, Ny, Ny))
    Rhist[:] = np.NaN
    Rhist[0,:,:] = R0

    rMhist = np.empty((Nobs+1, Ny))
    rMhist[:] = rPhistCKF[-1,:]

    rPhist = np.empty((Nobs+1, Ny))
    rPhist[:] = rPhistCKF[-1,:]

    Khist = np.empty((Nobs+1, Nstate, Ny))
    Khist[:] = np.NaN
    
    # =========================================================================
    # Generate necessary jacobian function handles
    # =========================================================================
    if evalA == None:
        _, evalA = Jacobians.getdfdZ()
    if evalH == None:
        _, evalH = Jacobians.getHtilde_sc_rho_rhod()
    
    # =========================================================================
    # # =======================================================================
    # # Minor iterations, i
    # # =======================================================================
    # =========================================================================
    # NOTE TO SELF: index "i" corresponds to the update based on measurement 
    # with index "i-1", such that index i = 0 corresponds to initialization,
    # i = 1 is the result of the first measurement update.
    for i in range(1,Nobs+1):
        # =========================================================================
        # Read next observation
        # =========================================================================
        thist[i] = obs['t'][i-1]
        if NOISEON:
            yhist[i,0] = obs['noisyrange'][i-1]
            yhist[i,1] = obs['noisyrate'][i-1]
        else:
            yhist[i,0] = obs['range'][i-1]
            yhist[i,1] = obs['rangerate'][i-1]
        Rhist[i,:,:] = R0
        
        rvecS = np.array([obs['x'][i-1], obs['y'][i-1], obs['z'][i-1]])
        vvecS = np.array([obs['vx'][i-1], obs['vy'][i-1], obs['vz'][i-1]])
        
        # =========================================================================
        # Propagate and time update from t[i-1] to t[i] (or equate)
        # =========================================================================
        if (thist[i] == thist[i-1]):
            # don't update xxhat or PM
            xxhathistM[i,:] = xxhathistP[i-1,:]
            PhistM[i,:,:] = PhistP[i-1,:,:]
        else:
            # numerically propagate nonlinear state, STM
            IC = np.append(xxhathistP[i-1,:],
                           np.reshape(np.eye(Nstate), (Nstate**2,)))
            
            sol = solve_ivp(lambda t, yy: \
                            dyn.keplerJ2J3_wPhi_ODErv(t, yy, mu,
                                                      J2, J3, R,
                                                      evalA),
                            (thist[i-1], thist[i]), IC,
                            rtol = 1e-13, atol = 1e-13, method = 'RK45')
            xxhathistM[i,:] = sol.y[:6,-1]
            Phi = np.reshape(sol.y[6:,-1], (Nstate,Nstate))
            
            # linearly update covariance
            PhistM[i,:,:] = Phi @ PhistP[i-1,:,:] @ Phi.T # + process noise
            
        # =========================================================================
        # Process observations
        # =========================================================================
        # predict range and range rate measurements for curent state, pre-update
        indObs = obs['ind'][i-1]
        rho, rhodot = mes.compute_range_rangerate_single(xxhathistM[i,:],
                                                         coords[indObs],
                                                         theta0, thist[i],
                                                         R, D)
        yPredM = np.array([rho, rhodot])
        
        # compute measurement sentivity matrix at this time
        Hi = evalH(xxhathistM[i,:3], xxhathistM[i,3:], rvecS, vvecS)
        
        # compute pre-update residual
        rMhist[i,:] = yhist[i,:] - yPredM
        
        # compute Kalman gain matrix
        Khist[i,:,:] = PhistM[i,:,:] @ Hi.T @ np.linalg.inv((Hi @ PhistM[i,:,:] @ Hi.T + Rhist[i,:,:]))
        # Khist[i,:,:] = np.linalg.solve(Hi @ PhistM[i,:,:] @ Hi.T + Rhist[i,:,:],
        #                                (PhistM[i,:,:] @ Hi.T).T).T
        
        # =========================================================================
        # Measurement update
        # =========================================================================
        # update estimate of state
        xxhathistP[i,:] = xxhathistM[i,:] + Khist[i,:,:] @ rMhist[i,:]
        
        # update covariance using Jordan-Busy form
        PhistP[i,:,:] = (np.eye(Nstate) - Khist[i,:,:] @ Hi)\
            @ PhistM[i,:,:] @ (np.eye(Nstate) - Khist[i,:,:] @ Hi).T\
                + Khist[i,:,:] @ Rhist[i,:,:] @ Khist[i,:,:].T
        
        # predict range and range rate measurements for curent state, post-update
        rho, rhodot = mes.compute_range_rangerate_single(xxhathistP[i,:],
                                                         coords[indObs],
                                                         theta0, thist[i],
                                                         R, D)
        yPredP = np.array([rho, rhodot])
        
        # compute post-update residual
        rPhist[i,:] = yhist[i,:] - yPredP
    
    # =========================================================================
    # Combine CKF and EKF results
    # =========================================================================
    thist = np.append(thistCKF, thist[1:])
    xxhathistP = np.append(xxHatCKF, xxhathistP[1:,:], axis = 0)
    PhistP = np.append(PhistPCKF, PhistP[1:,:,:], axis = 0)
    rPhist = np.append(rPhistCKF, rPhist[1:,:], axis = 0)
        
    # =========================================================================
    # Generate and print RMS values
    # =========================================================================
    RMS_scalar = np.empty(Nstate)
    for i in range(Nstate):
        RMS_scalar[i] = np.sqrt(((xxhathistP[:,i] - xxTruth[:,i])**2).sum()\
            / xxhathistP.shape[0])
    RMS_r = np.sqrt(((xxhathistP[:,:3] - xxTruth[:,:3])**2).sum()\
        / xxhathistP.shape[0])
    RMS_v = np.sqrt(((xxhathistP[:,3:] - xxTruth[:,3:])**2).sum()\
        / xxhathistP.shape[0])
    RMS_rrho = np.sqrt((rPhist[1:,0]**2).sum() / xxhathistP.shape[0])
    RMS_rrate = np.sqrt((rPhist[1:,1]**2).sum() / xxhathistP.shape[0])
    
    toc = time.time()
    
    print('------------------------------------------------------------------')
    print('EKF completed after {0:.0f} seconds. RMS values:'.format(toc-tic))
    print('    pos: {0:.5e} km'.format(RMS_r))
    print('        x: {0:.5e} km, y: {1:.5e} km, z: {2:.5e} km'\
          .format(RMS_scalar[0], RMS_scalar[1], RMS_scalar[1]))
    print('    vel: {0:.5e} km/s'.format(RMS_v))
    print('        vx: {0:.5e} km/s, vy: {1:.5e} km/s, vz: {2:.5e} km/s'\
          .format(RMS_scalar[3], RMS_scalar[4], RMS_scalar[5]))
    print('    range: {0:.5e} km'.format(RMS_rrho))
    print('    range rate: {0:.5e} km/s'.format(RMS_rrate))
    print('------------------------------------------------------------------')
    
    return thist, xxhathistP, PhistP, rPhist

def EKF_HW3_SNC(obs, xxTruth, ICParams, constParams, Qct, minDt = 10,
             evalA = None, evalH = None, evalQ = None, verbose = True,
             NCKF = 0):
    '''
    extended Kalman filter for HW3. ICparams and constParams
    are unpacked below.
    STATE:
        r: spacecraft position (x, y, z)
        v: spacecraft inertial velocity (dx, dy, dz)
    DYNAMICS:
        mu, J2, and J3 affect spacecraft. Just set J3 = 0 in filter to ignore.
        no dynamics for constants (mu, J2, CD).
        each ground station is fixed to earth at some location.
    
    function computes evalA and evalH by default, but can pass in as args for
    faster repeated runtime.
    
    Compute evalQ for inertial Qct by default, or accepts as input
    
    returns: time, residuals, state history, cov. history, total error history
    '''
    ICParams = copy.deepcopy(ICParams) # prevent xx0hat changing outside func.
    
    tic = time.time()
    
    mu = constParams['mu']
    J2 = constParams['J2']
    J3 = constParams['J3']
    R = constParams['R']
    
    t0 = ICParams['t0']
    xx0hat = ICParams['xx0hat']
    P0 = ICParams['P0']
    R0 = ICParams['R0']
    
    # =========================================================================
    # # =======================================================================
    # # Warm start using CKF
    # # =======================================================================
    # =========================================================================
    ICParams_CKF = {
        't0'    : t0,
        'xx0'   : xx0hat,
        'P0'    : P0,
        'R0'    : R0
        }
    thistCKF, xxHatCKF, PhistPCKF, rPhistCKF, RMSList\
        = CKF_HW3_SNC(obs.iloc[:NCKF], xxTruth[:NCKF+1], ICParams_CKF,
                      constParams, Qct, minDt, verbose = False, maxits = 1)
    
    t0 = thistCKF[-1]
    xx0hat = xxHatCKF[-1,:]
    P0 = PhistPCKF[-1,:,:]
    obs = obs.iloc[NCKF:].copy()
    obs.reset_index(drop=True, inplace=True)
    
    # =========================================================================
    # =========================================================================
    # # EKF
    # =========================================================================
    # =========================================================================
    
    # =========================================================================
    # Initialize
    # =========================================================================
    Nobs = len(obs)
    Nstate = len(xx0hat)
    Ny = 2 # range, range rate
    
    thist = np.empty(Nobs+1)
    thist[:] = np.NaN
    thist[0] = t0

    xxhathistP = np.empty((Nobs+1, Nstate))
    xxhathistP[:] = np.NaN
    xxhathistP[0,:] = xx0hat

    xxhathistM = np.empty((Nobs+1, Nstate))
    xxhathistM[:] = np.NaN

    PhistP = np.empty((Nobs+1, Nstate, Nstate))
    PhistP[:] = np.NaN
    PhistP[0,:,:] = P0

    PhistM = np.empty((Nobs+1, Nstate, Nstate))
    PhistM[:] = np.NaN

    yhist = np.empty((Nobs+1, Ny))
    yhist[:] = np.NaN # note, yhist[0] stays equal to NaN

    Rhist = np.empty((Nobs+1, Ny, Ny))
    Rhist[:] = np.NaN
    Rhist[0,:,:] = R0

    rMhist = np.empty((Nobs+1, Ny))
    rMhist[:] = rPhistCKF[-1,:]

    rPhist = np.empty((Nobs+1, Ny))
    rPhist[:] = rPhistCKF[-1,:]

    Khist = np.empty((Nobs+1, Nstate, Ny))
    Khist[:] = np.NaN
    
    # =========================================================================
    # Generate necessary jacobian function handles
    # =========================================================================
    if evalA == None:
        _, evalA = Jacobians.getdfdZ()
    if evalH == None:
        _, evalH = Jacobians.getHtilde_sc_rho_rhod()
    if evalQ == None:
        evalQ = getQ_HW3_inertial()
    
    # =========================================================================
    # # =======================================================================
    # # Minor iterations, i
    # # =======================================================================
    # =========================================================================
    # NOTE TO SELF: index "i" corresponds to the update based on measurement 
    # with index "i-1", such that index i = 0 corresponds to initialization,
    # i = 1 is the result of the first measurement update.
    for i in range(1,Nobs+1):
        # =========================================================================
        # Read next observation
        # =========================================================================
        thist[i] = obs['t'][i-1]
        yhist[i,0] = obs['noisyrange'][i-1]
        yhist[i,1] = obs['noisyrate'][i-1]
        Rhist[i,:,:] = R0
        
        rvecS = np.array([obs['x'][i-1], obs['y'][i-1], obs['z'][i-1]])
        vvecS = np.array([obs['vx'][i-1], obs['vy'][i-1], obs['vz'][i-1]])
        
        # =========================================================================
        # Propagate and time update from t[i-1] to t[i] (or equate)
        # =========================================================================
        if (thist[i] == thist[i-1]):
            # don't update xxhat or PM
            xxhathistM[i,:] = xxhathistP[i-1,:]
            PhistM[i,:,:] = PhistP[i-1,:,:]
        else:
            tprev = thist[i-1]
            xxiprev = xxhathistP[i-1,:]
            PPrev = PhistP[i-1, :, :]
            tspan = thist[i] - thist[i-1]
            t_minor_arc = np.linspace(tprev, thist[i],
                                      int(np.ceil(tspan/minDt))+1)[1:]
            for ti in t_minor_arc:
                # numerically propagate nonlinear state, STM
                IC = np.append(xxiprev,
                               np.reshape(np.eye(Nstate), (Nstate**2,)))
                
                sol = solve_ivp(lambda t, yy: \
                                dyn.keplerJ2J3_wPhi_ODErv(t, yy, mu,
                                                          J2, J3, R,
                                                          evalA),
                                (tprev, ti), IC,
                                rtol = 1e-13, atol = 1e-13)
                xxi = sol.y[:6,-1]
                Phii = np.reshape(sol.y[6:,-1], (Nstate,Nstate))
                
                # evaluate process noise (SNC)
                ON = astro.getON(xxi[:3], xxi[3:6])
                Qdt = evalQ(ti - tprev, Qct, ON)
                
                # linearly update covariance
                PiM = Phii @ PPrev @ Phii.T + Qdt
                
                # update previous values
                tprev = ti
                xxiprev = xxi
                PPrev = PiM
                
            # assign updated values to actual arrays
            xxhathistM[i,:] = xxi
            PhistM[i, :, :] = PiM
            
        # =========================================================================
        # Process observations
        # =========================================================================
        # predict range and range rate measurements for curent state, pre-update
        rho, rhodot = mes.compute_range_rangerate_single(xxhathistM[i,:],
                                                         rvecS, vvecS)
        yPredM = np.array([rho, rhodot])
        
        # compute measurement sentivity matrix at this time
        Hi = evalH(xxhathistM[i,:3], xxhathistM[i,3:], rvecS, vvecS)
        
        # compute pre-update residual
        rMhist[i,:] = yhist[i,:] - yPredM
        
        # compute Kalman gain matrix
        Khist[i,:,:] = PhistM[i,:,:] @ Hi.T @\
            np.linalg.inv((Hi @ PhistM[i,:,:] @ Hi.T + Rhist[i,:,:]))
        
        # =========================================================================
        # Measurement update
        # =========================================================================
        # update estimate of state
        xxhathistP[i,:] = xxhathistM[i,:] + Khist[i,:,:] @ rMhist[i,:]
        
        # update covariance using Jordan-Busy form
        PhistP[i,:,:] = (np.eye(Nstate) - Khist[i,:,:] @ Hi)\
            @ PhistM[i,:,:] @ (np.eye(Nstate) - Khist[i,:,:] @ Hi).T\
                + Khist[i,:,:] @ Rhist[i,:,:] @ Khist[i,:,:].T
        
        # predict range and range rate measurements for curent state, post-update
        rho, rhodot = mes.compute_range_rangerate_single(xxhathistP[i,:],
                                                         rvecS, vvecS)
        yPredP = np.array([rho, rhodot])
        
        # compute post-update residual
        rPhist[i,:] = yhist[i,:] - yPredP
    
    # =========================================================================
    # Combine CKF and EKF results
    # =========================================================================
    thist = np.append(thistCKF, thist[1:])
    xxhathistP = np.append(xxHatCKF, xxhathistP[1:,:], axis = 0)
    PhistP = np.append(PhistPCKF, PhistP[1:,:,:], axis = 0)
    rPhist = np.append(rPhistCKF, rPhist[1:,:], axis = 0)
        
    # =========================================================================
    # Generate and print RMS values
    # =========================================================================
    RMS_scalar = np.empty(Nstate)
    for i in range(Nstate):
        RMS_scalar[i] = np.sqrt(((xxhathistP[:,i] - xxTruth[:,i])**2).sum()\
            / xxhathistP.shape[0])
    RMS_r = np.sqrt(((xxhathistP[:,:3] - xxTruth[:,:3])**2).sum()\
        / xxhathistP.shape[0])
    RMS_v = np.sqrt(((xxhathistP[:,3:] - xxTruth[:,3:])**2).sum()\
        / xxhathistP.shape[0])
    RMS_rrho = np.sqrt((rPhist[1:,0]**2).sum() / xxhathistP.shape[0])
    RMS_rrate = np.sqrt((rPhist[1:,1]**2).sum() / xxhathistP.shape[0])
    RMSList = [RMS_r, RMS_v, RMS_rrho, RMS_rrate]
    
    toc = time.time()
    
    print('------------------------------------------------------------------')
    print('EKF completed after {0:.0f} seconds. RMS values:'.format(toc-tic))
    print('    pos: {0:.5e} km'.format(RMS_r))
    print('        x: {0:.5e} km, y: {1:.5e} km, z: {2:.5e} km'\
          .format(RMS_scalar[0], RMS_scalar[1], RMS_scalar[1]))
    print('    vel: {0:.5e} km/s'.format(RMS_v))
    print('        vx: {0:.5e} km/s, vy: {1:.5e} km/s, vz: {2:.5e} km/s'\
          .format(RMS_scalar[3], RMS_scalar[4], RMS_scalar[5]))
    print('    range: {0:.5e} km'.format(RMS_rrho))
    print('    range rate: {0:.5e} km/s'.format(RMS_rrate))
    print('------------------------------------------------------------------')
    
    return thist, xxhathistP, PhistP, rPhist, RMSList

def EKF_HW3_DMC(obs, xxTruth, ICParams, constParams, Qct, minDt = 10,
             evalA = None, evalH = None, verbose = True,
             NCKF = 0):
    '''
    extended Kalman filter for HW3, using DMC. ICparams and constParams
    are unpacked below.
    STATE:
        r: spacecraft position (x, y, z)
        v: spacecraft inertial velocity (dx, dy, dz)
    DYNAMICS:
        mu, J2, and J3 affect spacecraft. Just set J3 = 0 in filter to ignore.
        no dynamics for constants (mu, J2, CD).
        each ground station is fixed to earth at some location.
    
    function computes evalA and evalH by default, but can pass in as args for
    faster repeated runtime.
    
    Compute evalQ for inertial Qct by default, or accepts as input
    
    returns: time, residuals, state history, cov. history, total error history
    '''
    ICParams = copy.deepcopy(ICParams) # prevent xx0hat changing outside func.
    
    tic = time.time()
    
    mu = constParams['mu']
    J2 = constParams['J2']
    J3 = constParams['J3']
    R = constParams['R']
    
    t0 = ICParams['t0']
    xx0hat = ICParams['xx0hat']
    P0 = ICParams['P0']
    R0 = ICParams['R0']
    tau = ICParams['tau']
    
    if NCKF > 0:
        # =========================================================================
        # # =======================================================================
        # # Warm start using CKF
        # # =======================================================================
        # =========================================================================
        ICParams_CKF = {
            't0'    : t0,
            'xx0'   : xx0hat,
            'P0'    : P0,
            'R0'    : R0,
            'tau'   : tau
            }
        thistCKF, xxHatCKF, PhistPCKF, rPhistCKF, RMSList\
            = CKF_HW3_DMC(obs.iloc[:NCKF], xxTruth[:NCKF+1], ICParams_CKF,
                          constParams, Qct, minDt, verbose = False, maxits = 1)
        
        t0 = thistCKF[-1]
        xx0hat = xxHatCKF[-1,:]
        P0 = PhistPCKF[-1,:,:]
        obs = obs.iloc[NCKF:].copy()
        obs.reset_index(drop=True, inplace=True)
    
    # =========================================================================
    # =========================================================================
    # # EKF
    # =========================================================================
    # =========================================================================
    
    # =========================================================================
    # Initialize
    # =========================================================================
    Nobs = len(obs)
    Nstate = len(xx0hat)
    Ny = 2 # range, range rate
    
    thist = np.empty(Nobs+1)
    thist[:] = np.NaN
    thist[0] = t0

    xxhathistP = np.empty((Nobs+1, Nstate))
    xxhathistP[:] = np.NaN
    xxhathistP[0,:] = xx0hat

    xxhathistM = np.empty((Nobs+1, Nstate))
    xxhathistM[:] = np.NaN

    PhistP = np.empty((Nobs+1, Nstate, Nstate))
    PhistP[:] = np.NaN
    PhistP[0,:,:] = P0

    PhistM = np.empty((Nobs+1, Nstate, Nstate))
    PhistM[:] = np.NaN

    yhist = np.empty((Nobs+1, Ny))
    yhist[:] = np.NaN # note, yhist[0] stays equal to NaN

    Rhist = np.empty((Nobs+1, Ny, Ny))
    Rhist[:] = np.NaN
    Rhist[0,:,:] = R0

    rMhist = np.empty((Nobs+1, Ny))
    if NCKF > 0:
        rMhist[:] = rPhistCKF[-1,:]
    else:
        rMhist[:] = np.NaN

    rPhist = np.empty((Nobs+1, Ny))
    if NCKF > 0:
        rPhist[:] = rPhistCKF[-1,:]
    else:
        rPhist[:] = np.NaN

    Khist = np.empty((Nobs+1, Nstate, Ny))
    Khist[:] = np.NaN
    
    # =========================================================================
    # Generate necessary jacobian function handles
    # =========================================================================
    if evalA == None:
        _, evalA = Jacobians.getdfdZ()
    if evalH == None:
        _, evalH = Jacobians.getHtilde_sc_rho_rhod()
    
    # augment matrices to account for DMC
    def evalAz(rvec, vvec, mu, J2, J3, R, tau):
        A = evalA(rvec, vvec, mu, J2, J3, R)
        Bw = 1/tau * np.eye(3)
        UR = np.block([[np.zeros((3,3))],[np.eye(3)]])
        return np.block([[A, UR], [np.zeros((3,6)), -Bw]])
    
    def evalHz(rvec, vvec, rvecS, vvecS):
        H = evalH(rvec, vvec, rvecS, vvecS)
        return np.block([H, np.zeros((2,3))])
    
    # =========================================================================
    # # =======================================================================
    # # Minor iterations, i
    # # =======================================================================
    # =========================================================================
    # NOTE TO SELF: index "i" corresponds to the update based on measurement 
    # with index "i-1", such that index i = 0 corresponds to initialization,
    # i = 1 is the result of the first measurement update.
    for i in range(1,Nobs+1):
        # =========================================================================
        # Read next observation
        # =========================================================================
        thist[i] = obs['t'][i-1]
        yhist[i,0] = obs['noisyrange'][i-1]
        yhist[i,1] = obs['noisyrate'][i-1]
        Rhist[i,:,:] = R0
        
        rvecS = np.array([obs['x'][i-1], obs['y'][i-1], obs['z'][i-1]])
        vvecS = np.array([obs['vx'][i-1], obs['vy'][i-1], obs['vz'][i-1]])
        
        # =========================================================================
        # Propagate and time update from t[i-1] to t[i] (or equate)
        # =========================================================================
        if (thist[i] == thist[i-1]):
            # don't update xxhat or PM
            xxhathistM[i,:] = xxhathistP[i-1,:]
            PhistM[i,:,:] = PhistP[i-1,:,:]
        else:
            tprev = thist[i-1]
            xxiprev = xxhathistP[i-1,:]
            PPrev = PhistP[i-1, :, :]
            tspan = thist[i] - thist[i-1]
            t_minor_arc = np.linspace(tprev, thist[i],
                                      int(np.ceil(tspan/minDt))+1)[1:]
            for ti in t_minor_arc:
                # numerically propagate nonlinear state, STM
                IC = np.append(xxiprev,
                               np.reshape(np.eye(Nstate), (Nstate**2,)))
                
                sol = solve_ivp(lambda t, yy: \
                                dyn.ODE_HW3_DMC(t, yy, mu, J2, J3, R,
                                                evalAz, tau),
                                (tprev, ti), IC,
                                rtol = 1e-12, atol = 1e-12)
                xxi = sol.y[:Nstate,-1]
                Phii = np.reshape(sol.y[Nstate:,-1], (Nstate,Nstate))
                
                # evaluate process noise (DMC)
                Qdt = evalQ_HW3_DMC(Qct, tau, minDt)
                if i == 2:
                    print(np.diag(Qdt))
                
                # linearly update covariance
                PiM = Phii @ PPrev @ Phii.T + Qdt
                
                # update previous values
                tprev = ti
                xxiprev = xxi
                PPrev = PiM
                
            # assign updated values to actual arrays
            xxhathistM[i,:] = xxi
            PhistM[i, :, :] = PiM
            
        # =========================================================================
        # Process observations
        # =========================================================================
        # predict range and range rate measurements for curent state, pre-update
        rho, rhodot = mes.compute_range_rangerate_single(xxhathistM[i,:6],
                                                         rvecS, vvecS)
        yPredM = np.array([rho, rhodot])
        
        # compute measurement sentivity matrix at this time
        Hi = evalHz(xxhathistM[i,:3], xxhathistM[i,3:6], rvecS, vvecS)
        
        # compute pre-update residual
        rMhist[i,:] = yhist[i,:] - yPredM
        
        # compute Kalman gain matrix
        Khist[i,:,:] = PhistM[i,:,:] @ Hi.T @\
            np.linalg.inv((Hi @ PhistM[i,:,:] @ Hi.T + Rhist[i,:,:]))
        
        # =========================================================================
        # Measurement update
        # =========================================================================
        # update estimate of state
        xxhathistP[i,:] = xxhathistM[i,:] + Khist[i,:,:] @ rMhist[i,:]
        
        # update covariance using Jordan-Busy form
        PhistP[i,:,:] = (np.eye(Nstate) - Khist[i,:,:] @ Hi)\
            @ PhistM[i,:,:] @ (np.eye(Nstate) - Khist[i,:,:] @ Hi).T\
                + Khist[i,:,:] @ Rhist[i,:,:] @ Khist[i,:,:].T
        
        # predict range and range rate measurements for curent state, post-update
        rho, rhodot = mes.compute_range_rangerate_single(xxhathistP[i,:6],
                                                         rvecS, vvecS)
        yPredP = np.array([rho, rhodot])
        
        # compute post-update residual
        rPhist[i,:] = yhist[i,:] - yPredP
    
    if NCKF > 0:
        # =========================================================================
        # Combine CKF and EKF results
        # =========================================================================
        thist = np.append(thistCKF, thist[1:])
        xxhathistP = np.append(xxHatCKF, xxhathistP[1:,:], axis = 0)
        PhistP = np.append(PhistPCKF, PhistP[1:,:,:], axis = 0)
        rPhist = np.append(rPhistCKF, rPhist[1:,:], axis = 0)
        
    # =========================================================================
    # Generate and print RMS values
    # =========================================================================
    RMS_scalar = np.empty(6)
    for i in range(6):
        RMS_scalar[i] = np.sqrt(((xxhathistP[:,i] - xxTruth[:,i])**2).sum()\
            / xxhathistP.shape[0])
    RMS_r = np.sqrt(((xxhathistP[:,:3] - xxTruth[:,:3])**2).sum()\
        / xxhathistP.shape[0])
    RMS_v = np.sqrt(((xxhathistP[:,3:6] - xxTruth[:,3:])**2).sum()\
        / xxhathistP.shape[0])
    RMS_rrho = np.sqrt((rPhist[1:,0]**2).sum() / xxhathistP.shape[0])
    RMS_rrate = np.sqrt((rPhist[1:,1]**2).sum() / xxhathistP.shape[0])
    RMSList = [RMS_r, RMS_v, RMS_rrho, RMS_rrate]
    
    toc = time.time()
    
    print('------------------------------------------------------------------')
    print('EKF completed after {0:.0f} seconds. RMS values:'.format(toc-tic))
    print('    pos: {0:.5e} km'.format(RMS_r))
    print('        x: {0:.5e} km, y: {1:.5e} km, z: {2:.5e} km'\
          .format(RMS_scalar[0], RMS_scalar[1], RMS_scalar[1]))
    print('    vel: {0:.5e} km/s'.format(RMS_v))
    print('        vx: {0:.5e} km/s, vy: {1:.5e} km/s, vz: {2:.5e} km/s'\
          .format(RMS_scalar[3], RMS_scalar[4], RMS_scalar[5]))
    print('    range: {0:.5e} km'.format(RMS_rrho))
    print('    range rate: {0:.5e} km/s'.format(RMS_rrate))
    print('------------------------------------------------------------------')
    
    return thist, xxhathistP, PhistP, rPhist, RMSList

def NLBF_J2J3(obs, xxTruth, ICParams, constParams, coords,
             NOISEON = True, evalA = None, evalH = None,
             tol = 1e-8, maxits = 10):
    '''
    nonlinear batch filter for mu + J2 + J3 dynamics, range and range rate
    measurements. Can ignore J2/J3 by setting to zero. ICparams and constParams
    are unpacked below.
    
    NOISEON - if False, uses pure range and range rate measurements
    
    function computes evalA and evalH by default, but can pass in as args for
    faster repeated runtime.
    
    returns: time, residuals, state history, cov. history, total error history
    '''
    tic = time.time()
    
    mu = constParams['mu']
    J2 = constParams['J2']
    J3 = constParams['J3']
    R = constParams['R']
    D = constParams['D']
    theta0 = constParams['theta0']
    
    t0 = ICParams['t0']
    xx0hat = ICParams['xx0hat']
    P0 = ICParams['P0']
    R0 = ICParams['R0']
    
    # =========================================================================
    # Initialize
    # =========================================================================
    Nobs = len(obs)
    Nstate = len(xx0hat)
    Ny = 2 # range, range rate

    j = 0
    
    DELxxM = np.zeros(Nstate)
    P0P = P0
    
    # =========================================================================
    # Generate necessary jacobian function handles
    # =========================================================================
    if evalA == None:
        _, evalA = Jacobians.getdfdZ()
    if evalH == None:
        _, evalH = Jacobians.getHtilde_sc_rho_rhod()
    
    # =========================================================================
    # =========================================================================
    # # Major iterations, j
    # =========================================================================
    # =========================================================================
    converged = False

    while (not converged and j <= maxits):
        # =====================================================================
        # Initialize filter
        # =====================================================================
        # initialize empty variables
        xxhathist = np.empty((Nobs+1, Nstate))
        xxhathist[:] = np.NaN
        xxhathist[0,:] = xx0hat
        
        Phist = np.empty((Nobs+1, Nstate, Nstate))
        Phist[:] = np.NaN
        Phist[0,:,:] = P0P

        thist = np.empty(Nobs+1)
        thist[:] = np.NaN
        thist[0] = t0

        yhist = np.empty((Nobs+1, Ny))
        yhist[:] = np.NaN # note, yhist[0] stays equal to NaN

        Rhist = np.empty((Nobs+1, Ny, Ny))
        Rhist[:] = np.NaN
        Rhist[0,:,:] = R0

        rhist = np.empty((Nobs+1, Ny))
        rhist[:] = np.NaN
        
        # initialize values
        Phi_im1_0 = np.eye(Nstate)
        
        INF = np.linalg.inv(P0)
        NNN = np.linalg.inv(P0) @ DELxxM
        
        # =====================================================================
        # # ===================================================================
        # # Minor iterations, i
        # # ===================================================================
        # =====================================================================
        # NOTE TO SELF: index "i" corresponds to the update based on
        # measurement with index "i-1", such that index i = 0 corresponds to
        # initialization, i = 1  is the result of the first measurement update.
        
        for i in range(1,Nobs+1):
            # =================================================================
            # Read next observation
            # =================================================================
            thist[i] = obs['t'][i-1]
            if NOISEON:
                yhist[i,0] = obs['noisyrange'][i-1]
                yhist[i,1] = obs['noisyrate'][i-1]
            else:
                yhist[i,0] = obs['range'][i-1]
                yhist[i,1] = obs['rangerate'][i-1]
            Rhist[i,:,:] = R0
            
            rvecS = np.array([obs['x'][i-1], obs['y'][i-1], obs['z'][i-1]])
            vvecS = np.array([obs['vx'][i-1], obs['vy'][i-1], obs['vz'][i-1]])
            
            # =================================================================
            # Propagate and time update from t[i-1] to t[i] (or equate)
            # =================================================================
            if (thist[i] == thist[i-1]):
                # don't update xxhat or STM
                xxhathist[i,:] = xxhathist[i-1,:]
                Phi = Phi_im1_0
                Phist[i,:,:] = Phist[i-1,:,:]
            else:
                # numerically propagate nonlinear state, STM
                IC = np.append(xxhathist[i-1,:],
                               np.reshape(np.eye(Nstate), (Nstate**2,)))
                
                sol = solve_ivp(lambda t, yy: \
                                dyn.keplerJ2J3_wPhi_ODErv(t, yy, mu,
                                                          J2, J3, R,
                                                          evalA),
                                (thist[i-1], thist[i]), IC,
                                rtol = 1e-13, atol = 1e-13, method = 'RK45')
                xxhathist[i,:] = sol.y[:6,-1]
                Phi_i_im1 = np.reshape(sol.y[6:,-1], (Nstate,Nstate))
                Phi = Phi_i_im1 @ Phi_im1_0
                
                # linearly update cov. (only used for reporting)
                Phist[i,:,:] = Phi_i_im1 @ Phist[i-1,:,:] @ Phi_i_im1.T
                
            # =================================================================
            # Process observations
            # =================================================================
            # predict range and range rate measurements for curent state
            indObs = obs['ind'][i-1]
            rho, rhodot = mes.compute_range_rangerate_single(xxhathist[i,:],
                                                             coords[indObs],
                                                             theta0, thist[i],
                                                             R, D)
            yPred = np.array([rho, rhodot])
            
            # compute measurement sentivity matrix at this time
            Hi = evalH(xxhathist[i,:3], xxhathist[i,3:], rvecS, vvecS)
            
            # compute pre-update residual
            rhist[i,:] = yhist[i,:] - yPred
            
            # accumulate information
            INF = INF + (Hi @ Phi).T @ np.linalg.inv(Rhist[i,:,:]) @ (Hi @ Phi)
            NNN = NNN + (Hi @ Phi).T @ np.linalg.inv(Rhist[i,:,:]) @ rhist[i,:]
            
            # update previous STM value
            Phi_im1_0 = Phi
            
        # =====================================================================
        # Compute state deviation and covariance at initial time
        # =====================================================================
        DELxxP = np.linalg.inv(INF) @ NNN
        P0P = np.linalg.inv(INF)
        err = np.linalg.norm(DELxxP)
        
        if np.linalg.norm(err) < tol:
            converged = True
        else:
            xx0hat = xx0hat + DELxxP
            DELxxM -= DELxxP
        
        toc = time.time()
        print('-'*100)
        print('NLBF completed major iteration j = {0:d} after {1:.0f} seconds'\
              ', error = {2:.3e}.'.format(j, toc-tic, err))
            
        # =====================================================================
        # Generate and print RMS values
        # =====================================================================
        RMS_scalar = np.empty(Nstate)
        for i in range(Nstate):
            RMS_scalar[i] = np.sqrt(((xxhathist[:,i] - xxTruth[:,i])**2).sum()\
                / xxhathist.shape[0])
        RMS_r = np.sqrt(((xxhathist[:,:3] - xxTruth[:,:3])**2).sum()\
            / xxhathist.shape[0])
        RMS_v = np.sqrt(((xxhathist[:,3:] - xxTruth[:,3:])**2).sum()\
            / xxhathist.shape[0])
        RMS_rrho = np.sqrt((rhist[1:,0]**2).sum() / xxhathist.shape[0])
        RMS_rrate = np.sqrt((rhist[1:,1]**2).sum() / xxhathist.shape[0])
        
        print('RMS values:')
        print('    pos: {0:.5e} km'.format(RMS_r))
        print('        x: {0:.5e} km, y: {1:.5e} km, z: {2:.5e} km'\
              .format(RMS_scalar[0], RMS_scalar[1], RMS_scalar[1]))
        print('    vel: {0:.5e} km/s'.format(RMS_v))
        print('        vx: {0:.5e} km/s, vy: {1:.5e} km/s, vz: {2:.5e} km/s'\
              .format(RMS_scalar[3], RMS_scalar[4], RMS_scalar[5]))
        print('    range: {0:.5e} km'.format(RMS_rrho))
        print('    range rate: {0:.5e} km/s'.format(RMS_rrate))
        print('-'*100)
        j += 1
    
    if not converged:
        sys.exit('NLBF did not converge')
    
    return thist, xxhathist, Phist, rhist


def NLBF_P1(obs, ICParams, constParams, evalA = None, evalH = None, Ngs = 3,
            tol = 1e-8, maxits = 10):
    '''
    nonlinear batch filter for project 1. ICparams and constParams
    are unpacked below.
    STATE:
        r: spacecraft position (x, y, z)
        v: spacecraft inertial velocity (dx, dy, dz)
        mu: GM of central body
        J2: oblateness of central body
        CD: drag coefficient of spacecraft
        RSi: position vector for each ground station (xS1, xS2, xS3)
    DYNAMICS:
        mu, J2, and drag affect spacecraft.
        no dynamics for constants (mu, J2, CD)
        each ground station is fixed to earth at some location
    
    function computes evalA and evalH by default, but can pass in as args for
    faster repeated runtime.
    
    returns: time, residuals, state history, cov. history, total error history
    '''
    tic = time.time()
    
    t0 = ICParams['t0']
    xx0hat = ICParams['xx0hat']
    P0 = ICParams['P0']
    R0 = ICParams['R0']
    
    
    # =========================================================================
    # Initialize
    # =========================================================================
    Nobs = len(obs)
    Nstate = len(xx0hat)
    Ny = 2 # range, range rate

    j = 0
    
    DELxxM = np.zeros(Nstate)
    P0P = P0
    
    # =========================================================================
    # Generate necessary jacobian function handles
    # =========================================================================
    if evalA == None:
        _, evalA = Jacobians.getdfdZ_P1(Ngs = Ngs)
    if evalH == None:
        _, evalH = Jacobians.getH_P1()
    
    # =========================================================================
    # =========================================================================
    # # Major iterations, j
    # =========================================================================
    # =========================================================================
    converged = False

    while (not converged and j < maxits):
        # =====================================================================
        # Initialize filter
        # =====================================================================
        # initialize empty variables
        xxhathist = np.empty((Nobs+1, Nstate))
        xxhathist[:] = np.NaN
        xxhathist[0,:] = xx0hat
        
        Phist = np.empty((Nobs+1, Nstate, Nstate))
        Phist[:] = np.NaN
        Phist[0,:,:] = P0P

        thist = np.empty(Nobs+1)
        thist[:] = np.NaN
        thist[0] = t0

        yhist = np.empty((Nobs+1, Ny))
        yhist[:] = np.NaN # note, yhist[0] stays equal to NaN

        Rhist = np.empty((Nobs+1, Ny, Ny))
        Rhist[:] = np.NaN
        Rhist[0,:,:] = R0

        rhist = np.empty((Nobs+1, Ny))
        rhist[:] = np.NaN
        
        # initialize values
        Phi_im1_0 = np.eye(Nstate)
        
        if P0 is None:
            INF = np.zeros((Nstate, Nstate))
            NNN = np.zeros(Nstate)
        else:
            INF = np.linalg.inv(P0)
            NNN = np.linalg.inv(P0) @ DELxxM
        
        # =====================================================================
        # # ===================================================================
        # # Minor iterations, i
        # # ===================================================================
        # =====================================================================
        # NOTE TO SELF: index "i" corresponds to the update based on
        # measurement with index "i-1", such that index i = 0 corresponds to
        # initialization, i = 1  is the result of the first measurement update.
        
        for i in range(1,Nobs+1):
            # =================================================================
            # Read next observation
            # =================================================================
            # read time, range, and range rate. set R to constant value R0.
            thist[i] = obs['t'][i-1]
            yhist[i,0] = obs['range'][i-1]
            yhist[i,1] = obs['rangerate'][i-1]
            Rhist[i,:,:] = R0
            
            # set index of station based on ID number
            if obs['ID'][i-1] == 101:
                obsInd = 0
            elif obs['ID'][i-1] == 337:
                obsInd = 1
            elif obs['ID'][i-1] == 394:
                obsInd = 2
            else:
                sys.exit('Error: station ID not recognized')
            
            # =================================================================
            # Propagate and time update from t[i-1] to t[i] (or equate)
            # =================================================================
            if (thist[i] == thist[i-1]):
                # don't update xxhat or STM
                xxhathist[i,:] = xxhathist[i-1,:]
                Phi = Phi_im1_0
                Phist[i,:,:] = Phist[i-1,:,:]
            else:
                # numerically propagate nonlinear state, STM
                IC = np.append(xxhathist[i-1,:],
                               np.reshape(np.eye(Nstate), (Nstate**2,)))
                
                sol = solve_ivp(lambda t, yy: \
                                dyn.muJ2CD_wPhi(t, yy, constParams, evalA),
                                (thist[i-1], thist[i]), IC,
                                rtol = 1e-13, atol = 1e-13, method = 'RK45')
                xxhathist[i,:] = sol.y[:Nstate,-1]
                Phi_i_im1 = np.reshape(sol.y[Nstate:,-1], (Nstate,Nstate))
                Phi = Phi_i_im1 @ Phi_im1_0
                
                # linearly update cov. (only used for reporting)
                Phist[i,:,:] = Phi_i_im1 @ Phist[i-1,:,:] @ Phi_i_im1.T
                
            # =================================================================
            # Process observations
            # =================================================================
            # predict range and range rate measurements for curent state
            sii = 9 + obsInd * 3
            rho, rhodot = mes.compute_obs_P1(xxhathist[i,:3], xxhathist[i,3:6],
                                             xxhathist[i,sii:(sii+3)],
                                             constParams, thist[i])
            yPred = np.array([rho, rhodot])
            
            # compute measurement sentivity matrix at this time
            Hi = evalH(xxhathist[i,:], constParams, obsInd, Ngs)
            
            # compute pre-update residual
            rhist[i,:] = yhist[i,:] - yPred
            
            # accumulate information
            INF = INF + (Hi @ Phi).T @ np.linalg.inv(Rhist[i,:,:]) @ (Hi @ Phi)
            NNN = NNN + (Hi @ Phi).T @ np.linalg.inv(Rhist[i,:,:]) @ rhist[i,:]
            
            # update previous STM value
            Phi_im1_0 = Phi
            
        # =====================================================================
        # Compute state deviation and covariance at initial time
        # =====================================================================
        DELxxP = np.linalg.inv(INF) @ NNN
        P0P = np.linalg.inv(INF)
        err = np.linalg.norm(DELxxP)
        
        if np.linalg.norm(err) < tol:
            converged = True
        else:
            xx0hat = xx0hat + DELxxP
            DELxxM -= DELxxP
        
        RMS_rrho = np.sqrt((rhist[1:,0]**2).sum() / xxhathist.shape[0])
        RMS_rrate = np.sqrt((rhist[1:,1]**2).sum() / xxhathist.shape[0])
        
        toc = time.time()
        print('-'*100)
        print('NLBF completed major iteration j = {0:d} after {1:.0f} seconds'\
              ', error = {2:.3e}.'.format(j, toc-tic, err))
        print('RMS values:')
        print('    range: {0:.5e} km'.format(RMS_rrho))
        print('    range rate: {0:.5e} km/s'.format(RMS_rrate))
        print('-'*100)
        j += 1
    
    if not converged:
        print('*'*100)
        print('WARNING: NLBF did not converge to within tol of {0:.3e}'\
              .format(tol))
        print('*'*100)
    
    return thist, xxhathist, Phist, rhist


# =============================================================================
# Process noise models
# =============================================================================
def getQ_HW3_inertial():
    '''
    Dt: time step, seconds
    Qct: continuous time process noise covariance matrix, qxq
    '''
    
    # form symbolic variables
    Dt = sp.symbols('Dt')
    Qct = sp.MatrixSymbol('Qct', 3, 3)
    
    # STM and B for perturbed double integrator
    Phi = np.block([[np.eye(3), np.eye(3) * Dt], [np.zeros((3,3)), np.eye(3)]])
    B = np.block([[np.zeros((3,3))],[np.eye(3)]])
    
    # form integrand to get discrete-time Q
    integrand = Phi @ B @ Qct @ B.T @ Phi.T
    int_flat = integrand.flatten()
    
    # integrate
    QQ_flat = np.empty(int_flat.shape, dtype = object)
    for i in range(len(int_flat)):
        QQ_flat[i] = sp.integrate(int_flat[i], Dt)
    QQ = np.reshape(QQ_flat, (6,6))
    
    # lambdify, convert to numpy 2D array
    eval_QQ_prot = nb.njit(sp.lambdify([Dt, Qct], QQ, cse = sp.cse))
    return lambda Dt, Qct, ON: np.asarray(eval_QQ_prot(Dt, Qct))

def getQ_HW3_DMC_symb():
    
    # form symbolic variables
    Dt = sp.symbols('Dt')
    sigx, sigy, sigz = sp.symbols('sigx, sigy, sigz')
    # Qct = sp.MatrixSymbol('Qct', 3, 3)
    taux, tauy, tauz = sp.symbols('taux, tauy, tauz')
    # tau = sp.symbols('tau')
    
    # Qct = sigx**2 * sp.eye(3)
    Qct = sp.diag(sigx**2, sigy**2, sigz**2)
    
    # Bw = sp.diag(1/taux, 1/tauy, 1/tauz)
    # Bw = 1/tau * sp.eye(3)
    Bw = sp.diag(1/taux, 1/tauy, 1/tauz)
    Z = sp.zeros(3)
    I = sp.eye(3)
    A = sp.Matrix(sp.BlockMatrix([[Z, I, Z],
                                  [Z, Z, I],
                                  [Z, Z, -Bw]]))
    Phi = sp.exp(A * Dt)
    
    B = sp.Matrix(sp.BlockMatrix([[sp.zeros(3)],
                                  [sp.zeros(3)],
                                  [sp.eye(3)]]))
    
    # form integrand
    integrand = Phi @ B @ Qct @ B.T @ Phi.T # 9x9
    
    # integrate
    QQ = np.empty((9,9), dtype = object)
    for i in range(9):
        for j in range(9):
            QQ[i, j] = sp.integrate(integrand[i,j], Dt)
    
    QQ[np.where(QQ == 0)] = 0.0
    QQ[np.where(QQ == 1)] = 1.0
    # lambdify, convert to numpy 2D array
    # eval_QQ_prot = nb.njit(sp.lambdify([Dt, sig, taux, tauy, tauz],
    #                                     QQ, cse = sp.cse))
    eval_QQ_prot = sp.lambdify([Dt, sigx, sigy, sigz, taux, tauy, tauz],
                                        QQ)
    return QQ, lambda Dt, sigvec, tauvec: np.asarray(eval_QQ_prot(Dt,
                                                                  sigvec[0],
                                                                  sigvec[1],
                                                                  sigvec[2],
                                                                  tauvec[0],
                                                                  tauvec[1],
                                                                  tauvec[2]))

def evalQ_HW3_DMC(Qct, tau, dt):
    bw = 1/tau
    
    Qrr = 1/(3*bw**2)*dt**3 - 1/bw**3*dt**2 + 1/bw**4*dt\
        - 2/bw**4*np.exp(-bw*dt)*dt + 1/(2*bw**5)*(1-np.exp(-2*bw*dt))
    Qrv = 1/(2*bw**2)*dt**2 - 1/bw**3*dt + 1/bw**3*np.exp(-bw*dt)*dt\
        + 1/bw**4*(1 - np.exp(-bw*dt)) - 1/(2*bw**4)*(1 - np.exp(-2*bw*dt))
    Qrw = 1/(2*bw**3)*(1-np.exp(-2*bw*dt)) - 1/bw**2*np.exp(-bw*dt)*dt
    Qvv = 1/bw**2*dt - 2/bw**3*(1 - np.exp(-bw*dt))\
        + 1/(2*bw**3)*(1-np.exp(-2*bw*dt))
    Qvw = 1/(2*bw**2)*(1+np.exp(-2*bw*dt)) - 1/bw**2*np.exp(-bw*dt)
    Qww = 1/(2*bw)*(1-np.exp(-2*bw*dt))
    
    return np.block([[Qrr*Qct, Qrv*Qct, Qrw*Qct],
                     [Qrv*Qct, Qvv*Qct, Qvw*Qct],
                     [Qrw*Qct, Qvw*Qct, Qww*Qct]])
    
    

def getQ_HW3_RIC():
    '''
    Dt: time step, seconds
    Qct: continuous time process noise covariance matrix, qxq
    ON: DCM to go from inertial (N) to Hill/RIC (O)
    '''
    
    # form symbolic variables
    Dt = sp.symbols('Dt')
    Qct = sp.MatrixSymbol('Qct', 3, 3)
    ON = sp.MatrixSymbol('ON', 3, 3)
    
    # STM and B for perturbed double integrator
    Phi = np.block([[np.eye(3), np.eye(3) * Dt], [np.zeros((3,3)), np.eye(3)]])
    B = np.block([[np.zeros((3,3))],[np.eye(3)]])
    
    # form integrand to get discrete-time Q
    integrand = Phi @ B @ ON.T @ Qct @ ON @ B.T @ Phi.T
    int_flat = integrand.flatten()
    
    # integrate
    QQ_flat = np.empty(int_flat.shape, dtype = object)
    for i in range(len(int_flat)):
        QQ_flat[i] = sp.integrate(int_flat[i], Dt)
    QQ = np.reshape(QQ_flat, (6,6))
    
    # lambdify, convert to numpy 2D array
    eval_QQ_prot = nb.njit(sp.lambdify([Dt, Qct, ON], QQ, cse = sp.cse))
    return lambda Dt, Qct, ON: np.asarray(eval_QQ_prot(Dt, Qct, ON))
    
    
    
    
    