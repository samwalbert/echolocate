# -*- coding: utf-8 -*-
"""
P1_main.py:
    master script for project 1, Stat OD
Created on Sat Feb 12 15:50:24 2022

https://bitbucket.org/samwalbert/echolocate/src/master/

@author: Samuel Albert
"""

import numpy as np
import pandas as pd
from matplotlib import cm
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.integrate import solve_ivp

from src import dynamics as dyn
from src import Jacobians
from src import filters

# =============================================================================
# Constants & given inputs
# =============================================================================
mu = 3.986004415e5          # km^3/s^2
J2 = 1.082626925638815e-3
R = 6378.1363               # km
omP = 7.2921158553e-5       # rad/s
theta0 = 0.0                # 0 initial ECEF/ECI rotation

rho0 = 3.614e-4             # kg/km^3
r0 = 700.0000 + R           # km
H =  88.6670                # km
Area = 3.0e-6               # km^2
m = 970                     # kg
CD = 2.0

Ngs = 3

params = {
    'R':    R,
    'omP':  omP,
    'Area': Area,
    'm':    m,
    'rho0': rho0,
    'H':    H,
    'r0':   r0,
    'Ngs':  Ngs
    }

# define ground stations
Nstations = 3               # number of stations
RSvecs = np.empty((Nstations,3)) # ECEF position vectors of stations
RSvecs[:] = np.NaN
IDs = [None]*Nstations

# Station 1
RSvecs[0,0] = -5127.5100   # km
RSvecs[0,1] = -3794.1600   # km
RSvecs[0,2] = 0.0          # km
IDs[0] = '101'

# Station 2
RSvecs[1,0] = 3860.9100    # km
RSvecs[1,1] = 3238.4900    # km
RSvecs[1,2] = 3898.0940    # km
IDs[1] = '337'

# Station 3
RSvecs[2,0] = 549.5050     # km
RSvecs[2,1] = -1380.8720   # km
RSvecs[2,2] = 6182.1970    # km
IDs[2] = '394'

# a-priori satellite state
r0vec = np.array([757.7000,
                  5222.6070,
                  4851.5000]) # km
v0vec = np.array([2.21321,
                  4.67834,
                  -5.37130]) # km/s

# initial conditions
t0 = 0
P0 = np.diag([1, 1, 1, 1, 1, 1, 1e2, 1e6, 1e6,
              1e-16, 1e-16, 1e-16, 1, 1, 1, 1, 1, 1]) # km-base
R0 = np.diag([(1e-5)**2, (1e-6)**2])
xx0hat = np.block([r0vec, v0vec, mu, J2, CD, RSvecs.flatten()]).flatten()

def set_axes_equal(ax):
    '''
    https://stackoverflow.com/questions/13685386/matplotlib-equal-unit-length
    -with-equal-aspect-ratio-z-axis-is-not-equal-to
    '''
    x_limits = ax.get_xlim3d()
    y_limits = ax.get_ylim3d()
    z_limits = ax.get_zlim3d()

    x_range = abs(x_limits[1] - x_limits[0])
    x_middle = np.mean(x_limits)
    y_range = abs(y_limits[1] - y_limits[0])
    y_middle = np.mean(y_limits)
    z_range = abs(z_limits[1] - z_limits[0])
    z_middle = np.mean(z_limits)

    plot_radius = 0.5*max([x_range, y_range, z_range])

    ax.set_xlim3d([x_middle - plot_radius, x_middle + plot_radius])
    ax.set_ylim3d([y_middle - plot_radius, y_middle + plot_radius])
    ax.set_zlim3d([z_middle - plot_radius, z_middle + plot_radius])

def plots(thist, xxHat, Phist, rhist, xxRef):
    # residuals
    fig, ax = plt.subplots(2,1, sharex = True)
    fig.suptitle('Post-Update Measurement Residuals')
    for i in range(2):
        ax[i].plot(thist/3600, rhist[:,i], '+')
        ax[i].grid()

    ax[0].set_ylabel('range residuals, km')
    ax[0].tick_params('x', labelbottom=False)
    ax[1].set_ylabel('range rate residuals, km/s')
    ax[1].set_xlabel('time, hr')
    plt.tight_layout()
    
    # covariance trace
    fig, ax = plt.subplots(2,1, sharex = True)
    fig.suptitle('Covariance Trace')
    ax[0].semilogy(thist/3600, np.trace(Phist[:,:3,:3], axis1=1, axis2=2), '+-')
    ax[0].grid()
    ax[0].set_ylabel(r'$Tr(P_r),\ km^2$')
    ax[0].tick_params('x', labelbottom=False)
    ax[1].semilogy(thist/3600, np.trace(Phist[:,3:6,3:6], axis1=1, axis2=2), '+-')
    ax[1].grid()
    ax[1].set_ylabel(r'$Tr(P_v),\ km^2/s^2$')
    ax[1].set_xlabel('time, hr')
    plt.tight_layout()
    
    # final state ellipsoids
    tsigs = 3*np.sqrt(np.diag(Phist[-1,:6,:6]))
    print('3 sigma values:')
    print(tsigs)
    # position:
    u = np.linspace(0, 2 * np.pi, 100)
    v = np.linspace(0, np.pi, 100)
    x = tsigs[0] * np.outer(np.cos(u), np.sin(v))
    y = tsigs[1] * np.outer(np.sin(u), np.sin(v))
    z = tsigs[2] * np.outer(np.ones_like(u), np.cos(v))
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.plot_surface(x, y, z, cmap = cm.viridis)
    ax.set_xlabel('x, km')
    ax.set_ylabel('y, km')
    ax.set_zlabel('z, km')
    fig.suptitle(r'$3\sigma$ Ellipsoid for Spacecraft Position at Final Time')
    set_axes_equal(ax)
    plt.tight_layout()
    
    # velocity:
    x = tsigs[3] * np.outer(np.cos(u), np.sin(v))
    y = tsigs[4] * np.outer(np.sin(u), np.sin(v))
    z = tsigs[5] * np.outer(np.ones_like(u), np.cos(v))
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.plot_surface(x, y, z, cmap = cm.viridis)
    ax.set_xlabel('x, km/s')
    ax.set_ylabel('y, km/s')
    ax.set_zlabel('z, km/s')
    fig.suptitle(r'$3\sigma$ Ellipsoid for Spacecraft Velocity at Final Time')
    set_axes_equal(ax)
    plt.tight_layout()
    
    # relative states
    DX = np.linalg.norm((xxRef[:,:3] - xxHat[:,:3]), axis = 1)
    DV = np.linalg.norm((xxRef[:,3:6] - xxHat[:,3:6]), axis = 1)
    fig, ax = plt.subplots(2, 1, sharex = True)
    fig.suptitle('Relative States')
    ax[0].plot(thist/3600, DX, '+-')
    ax[0].grid()
    ax[0].set_ylabel(r'$\Delta r$, km')
    ax[0].set_ylim((-1, 7))
    ax[0].tick_params('x', labelbottom = False)
    ax[1].plot(thist/3600, DV, '+-')
    ax[1].grid()
    ax[1].set_ylabel(r'$\Delta v$, km/s')
    ax[1].set_ylim((-0.001, 0.01))
    ax[1].set_xlabel('time, hr')
    plt.tight_layout()
    
    
    


#%%
# Generate Jacobians
Asym, evalA = Jacobians.getdfdZ_P1(Ngs = 3)
Hsym, evalH = Jacobians.getH_P1()


#%%
# load observations from file, convert from m to km
fname = 'data/P1_observations.txt'
obs = pd.read_csv(fname, delim_whitespace = True, header = None)
obs.columns = ['t', 'ID', 'range', 'rangerate']
obs.range = obs.range/1e3 # m to km
obs.rangerate = obs.rangerate/1e3 # m/s to km/s

#%%
# =============================================================================
# Baseline runs of NLBF, CKF, and CKFJ (CKF with major iterations)
# always limited to max iterations of 3
# =============================================================================
#%% NLBF
ICParams_NLBF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF = filters.NLBF_P1(obs.copy(),
                                                              ICParams_NLBF,
                                                              params,
                                                              evalA = evalA,
                                                              evalH = evalH,
                                                              Ngs = 3,
                                                              tol = 1e-7,
                                                              maxits = 3)

#%% CKF
ICParams_CKF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

thistCKF, xxhatCKF, PhistCKF, rhistCKF = filters.CKF_P1(obs.copy(),
                                                        ICParams_CKF,
                                                        params,
                                                        evalA = evalA,
                                                        evalH = evalH,
                                                        Ngs = 3,
                                                        tol = 1e-7,
                                                        maxits = 1)

#%% CKFJ
thistCKFJ, xxhatCKFJ, PhistCKFJ, rhistCKFJ = filters.CKF_P1(obs.copy(),
                                                            ICParams_CKF,
                                                            params,
                                                            evalA = evalA,
                                                            evalH = evalH,
                                                            Ngs = 3,
                                                            tol = 1e-7,
                                                            maxits = 3)

#%% propagate initial guess trajectory
Nstate = 9 + Ngs*3
IC = np.block([r0vec, v0vec, mu, J2, CD, RSvecs.flatten()]).flatten()
IC = np.append(IC, np.eye(Nstate))
sol = solve_ivp(lambda t, yy: dyn.muJ2CD_wPhi(t, yy, params, evalA),
                (0, thistCKF[-1]), IC, t_eval = thistCKF[1:],
                rtol = 1e-12, atol = 1e-12)
xxRef = sol.y[:6,:].T
xxRef = np.append(IC[None,:6], xxRef, axis = 0) # hacky way to account for double value at t0

#%% PLOTS
# plt.close('all')
plots(thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF, xxRef)

plots(thistCKF, xxhatCKF, PhistCKF, rhistCKF, xxRef)

plots(thistCKFJ, xxhatCKFJ, PhistCKFJ, rhistCKFJ, xxRef)

#%%
# plot state estimates relative to each other
r_NLBF_CKF = np.linalg.norm(xxhatNLBF[:,:3] - xxhatCKF[:,:3], axis = 1)
v_NLBF_CKF = np.linalg.norm(xxhatNLBF[:,3:6] - xxhatCKF[:,3:6], axis = 1)
r_NLBF_CKFJ = np.linalg.norm(xxhatNLBF[:,:3] - xxhatCKFJ[:,:3], axis = 1)
v_NLBF_CKFJ = np.linalg.norm(xxhatNLBF[:,3:6] - xxhatCKFJ[:,3:6], axis = 1)
r_CKF_CKFJ = np.linalg.norm(xxhatCKF[:,:3] - xxhatCKFJ[:,:3], axis = 1)
v_CKF_CKFJ = np.linalg.norm(xxhatCKF[:,3:6] - xxhatCKFJ[:,3:6], axis = 1)

fig, ax = plt.subplots(2, 1, sharex = True)
fig.suptitle('Estimated states comparison')
ax[0].semilogy(thistNLBF/3600, r_NLBF_CKF, '+-', label = 'NLBF vs. CKF')
ax[0].semilogy(thistNLBF/3600, r_NLBF_CKFJ, '+-', label = 'NLBF vs. CKFJ')
ax[0].semilogy(thistNLBF/3600, r_CKF_CKFJ, '+-', label = 'CKF vs. CKFJ')
ax[0].grid()
ax[0].set_ylabel(r'$\Delta r$, km')
ax[0].tick_params('x', labelbottom = False)
ax[0].legend()
ax[1].semilogy(thistNLBF/3600, v_NLBF_CKF, '+-', label = 'NLBF vs. CKF')
ax[1].semilogy(thistNLBF/3600, v_NLBF_CKFJ, '+-', label = 'NLBF vs. CKFJ')
ax[1].semilogy(thistNLBF/3600, v_CKF_CKFJ, '+-', label = 'CKF vs. CKFJ')
ax[1].grid()
ax[1].set_ylabel(r'$\Delta v$, km/s')
ax[1].set_xlabel('time, hr')
plt.tight_layout()

#%%
# =============================================================================
# Relative data strength
# =============================================================================
#%% range only
R0 = np.diag([(1e-5)**2, np.inf])
ICParams_NLBF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF = filters.NLBF_P1(obs.copy(),
                                                              ICParams_NLBF,
                                                              params,
                                                              evalA = evalA,
                                                              evalH = evalH,
                                                              Ngs = 3,
                                                              tol = 1e-7,
                                                              maxits = 3)
plots(thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF, xxRef)

#%% range rate only
R0 = np.diag([np.inf, (1e-6)**2])
ICParams_NLBF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF = filters.NLBF_P1(obs.copy(),
                                                              ICParams_NLBF,
                                                              params,
                                                              evalA = evalA,
                                                              evalH = evalH,
                                                              Ngs = 3,
                                                              tol = 1e-7,
                                                              maxits = 3)
plots(thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF, xxRef)

#%%
# =============================================================================
# Fixing and unfixing ground stations
# =============================================================================
# plt.close('all')
R0 = np.diag([(1e-5)**2, (1e-6)**2])
P0 = np.diag([1, 1, 1, 1, 1, 1, 1e2, 1e6, 1e6,
              1e-16, 1e-16, 1e-16, 1, 1, 1, 1, 1, 1]) # km-base

#%% no fixed stations
P0 = np.diag([1, 1, 1, 1, 1, 1, 1e2, 1e6, 1e6,
              1, 1, 1, 1, 1, 1, 1, 1, 1]) # km-base
ICParams_NLBF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF = filters.NLBF_P1(obs.copy(),
                                                              ICParams_NLBF,
                                                              params,
                                                              evalA = evalA,
                                                              evalH = evalH,
                                                              Ngs = 3,
                                                              tol = 1e-7,
                                                              maxits = 3)
plots(thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF, xxRef)

#%% fix station 337
P0 = np.diag([1, 1, 1, 1, 1, 1, 1e2, 1e6, 1e6,
              1, 1, 1, 1e-16, 1e-16, 1e-16, 1, 1, 1]) # km-base
ICParams_NLBF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF = filters.NLBF_P1(obs.copy(),
                                                              ICParams_NLBF,
                                                              params,
                                                              evalA = evalA,
                                                              evalH = evalH,
                                                              Ngs = 3,
                                                              tol = 1e-7,
                                                              maxits = 3)
plots(thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF, xxRef)

#%% fix station 394
P0 = np.diag([1, 1, 1, 1, 1, 1, 1e2, 1e6, 1e6,
              1, 1, 1, 1, 1, 1, 1e-16, 1e-16, 1e-16]) # km-base
ICParams_NLBF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF = filters.NLBF_P1(obs.copy(),
                                                              ICParams_NLBF,
                                                              params,
                                                              evalA = evalA,
                                                              evalH = evalH,
                                                              Ngs = 3,
                                                              tol = 1e-7,
                                                              maxits = 3)
plots(thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF, xxRef)

#%%
# =============================================================================
# Playing with P0 and R0 values (extension)
# =============================================================================
plt.close('all')
R0_0 = np.diag([(1e-5)**2, (1e-6)**2])
P0_0 = np.diag([1, 1, 1, 1, 1, 1, 1e2, 1e6, 1e6,
              1e-16, 1e-16, 1e-16, 1, 1, 1, 1, 1, 1]) # km-base

#%% test 0: baseline NLBF
P0 = P0_0
R0 = R0_0
ICParams_NLBF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

thistNLBF, xxhatbase, PhistNLBF, rhistNLBF = filters.NLBF_P1(obs.copy(),
                                                              ICParams_NLBF,
                                                              params,
                                                              evalA = evalA,
                                                              evalH = evalH,
                                                              Ngs = 3,
                                                              tol = 1e-7,
                                                              maxits = 3)
plots(thistNLBF, xxhatbase, PhistNLBF, rhistNLBF, xxRef)

#%% test 1: increase R0
P0 = P0_0
R0 = R0_0 * 1e6

ICParams_CKF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

thistCKF, xxhatCKF, PhistCKF, rhistCKF = filters.CKF_P1(obs.copy(),
                                                        ICParams_CKF,
                                                        params,
                                                        evalA = evalA,
                                                        evalH = evalH,
                                                        Ngs = 3,
                                                        tol = 1e-7,
                                                        maxits = 1)
plots(thistCKF, xxhatCKF, PhistCKF, rhistCKF, xxRef)
r_test1 = np.linalg.norm(xxhatCKF[:,:3] - xxhatbase[:,:3], axis = 1)
v_test1 = np.linalg.norm(xxhatCKF[:,3:6] - xxhatbase[:,3:6], axis = 1)

#%% test 2: decrease P0
R0 = R0_0
P0 = np.diag([1e-8, 1e-8, 1e-8, 1e-14, 1e-14, 1e-14, 1, 1, 1,
              1e-16, 1e-16, 1e-16, 1, 1, 1, 1, 1, 1]) # km-base

ICParams_CKF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

thistCKF, xxhatCKF, PhistCKF, rhistCKF = filters.CKF_P1(obs.copy(),
                                                        ICParams_CKF,
                                                        params,
                                                        evalA = evalA,
                                                        evalH = evalH,
                                                        Ngs = 3,
                                                        tol = 1e-7,
                                                        maxits = 1)
plots(thistCKF, xxhatCKF, PhistCKF, rhistCKF, xxRef)
r_test2 = np.linalg.norm(xxhatCKF[:,:3] - xxhatbase[:,:3], axis = 1)
v_test2 = np.linalg.norm(xxhatCKF[:,3:6] - xxhatbase[:,3:6], axis = 1)

#%% test 3: increase P0
R0 = R0_0
P0 = P0_0 * 1e6

ICParams_CKF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

thistCKF, xxhatCKF, PhistCKF, rhistCKF = filters.CKF_P1(obs.copy(),
                                                        ICParams_CKF,
                                                        params,
                                                        evalA = evalA,
                                                        evalH = evalH,
                                                        Ngs = 3,
                                                        tol = 1e-7,
                                                        maxits = 1)
plots(thistCKF, xxhatCKF, PhistCKF, rhistCKF, xxRef)
r_test3 = np.linalg.norm(xxhatCKF[:,:3] - xxhatbase[:,:3], axis = 1)
v_test3 = np.linalg.norm(xxhatCKF[:,3:6] - xxhatbase[:,3:6], axis = 1)

#%% test 4: NLBF P0 = 0
R0 = R0_0
P0 = None

ICParams_NLBF = {
    't0'    : t0,
    'xx0hat': xx0hat,
    'P0'    : P0,
    'R0'    : R0
    }

thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF = filters.NLBF_P1(obs.copy(),
                                                              ICParams_NLBF,
                                                              params,
                                                              evalA = evalA,
                                                              evalH = evalH,
                                                              Ngs = 3,
                                                              tol = 1e-7,
                                                              maxits = 6)
plots(thistNLBF, xxhatNLBF, PhistNLBF, rhistNLBF, xxRef)
r_test4 = np.linalg.norm(xxhatNLBF[:,:3] - xxhatbase[:,:3], axis = 1)
v_test4 = np.linalg.norm(xxhatNLBF[:,3:6] - xxhatbase[:,3:6], axis = 1)


#%%
fig, ax = plt.subplots(2, 1, sharex = True)
fig.suptitle('Estimated states comparison')
ax[0].semilogy(thistNLBF/3600, r_test1, '+-', label = 'test 1')
ax[0].semilogy(thistNLBF/3600, r_test2, '+--', label = 'test 2')
ax[0].semilogy(thistNLBF/3600, r_test3, '+:', label = 'test 3')
ax[0].semilogy(thistNLBF/3600, r_test4, '+-.', label = 'test 4')
ax[0].grid()
ax[0].set_ylabel(r'$\Delta r$, km')
ax[0].tick_params('x', labelbottom = False)
ax[0].legend()
ax[1].semilogy(thistNLBF/3600, v_test1, '+-', label = 'test 1')
ax[1].semilogy(thistNLBF/3600, v_test2, '+-', label = 'test 2')
ax[1].semilogy(thistNLBF/3600, v_test3, '+-', label = 'test 3')
ax[1].semilogy(thistNLBF/3600, v_test4, '+-', label = 'test 4')
ax[1].grid()
ax[1].set_ylabel(r'$\Delta v$, km/s')
ax[1].set_xlabel('time, hr')
plt.tight_layout()
