# -*- coding: utf-8 -*-
"""
HW1P2.py:
    script for Stat OD HW 1 P 2
Created on Mon Jan 24 09:48:04 2022

@author: Samuel Albert
"""

import time
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

from src import Jacobians
from src import astrodynamics as astro
from src import dynamics as dyn

tic = time.time()
mpl.rcParams.update({'font.size': 15})
plt.close('all')

# =============================================================================
# Constants
# =============================================================================
mu = 3.986004415e5
J2 = 1.08263e-3
J3 = 0 # only J2 for this problem
R = 6378

# =============================================================================
# Propagate reference trajectory, along with STM
# =============================================================================
# given orbit elements
SMA = 10e3 # km
# ECC = 0.01 # used for MATLAB grader
ECC = 0.001 # for given ref. traj.
INC = 40 # deg
AOP = 80 # deg
RAAN = 40 # deg
TA0 = 0 # deg

# convert to inertial cartesian state
[r0vec, v0vec] = astro.kep2cart_TA(SMA, ECC, INC, AOP, RAAN, TA0, mu)

# get function handle to evaluate full Jacobian
_, evalJac = Jacobians.getdfdZ_wJ2()

# integrate reference trajectory
t0 = 0
P = 2 * np.pi * np.sqrt(SMA**3/mu)
tf = 15 * P + 60
# t_eval = np.linspace(t0, tf, 3000)
t_eval = np.arange(0, (15*P)+60, 60)
xx0vec = np.append(r0vec, v0vec) # r and v vectors
xx0vec = np.append(xx0vec, J2) # add J2 as state
xx0vec = np.append(xx0vec, np.eye(7).flatten()) # add Phi IC


sol = solve_ivp(lambda t, yy: dyn.keplerJ2_wPhi_ODE(t, yy, mu, J2, R, evalJac),
                (t0, tf), xx0vec, t_eval = t_eval, rtol = 1e-12, atol = 1e-12)
print(sol.message)

#%%
# =============================================================================
# Check reference against given trajectory data
# =============================================================================
filename = '../HW/HW1/HW1_p2a_traj.txt'
refData = np.loadtxt(filename, delimiter = ',')
tRef = refData[:,0]
rvecRef = refData[:,1:4] / 1e3 # convert to km
vvecRef = refData[:,4:7] / 1e3 # convert to km/s
PhiRef_flat = refData[:,7:]

# for each time, compare values
rErrList = np.empty(len(tRef))
rErrList[:] = np.NaN
vErrList = np.empty(len(tRef))
vErrList[:] = np.NaN
PhiErrList = np.empty(len(tRef))
PhiErrList[:] = np.NaN

for i, (tRefi, rvecRefi, vvecRefi) in enumerate(zip(tRef, rvecRef, vvecRef)):
    rErrList[i] = np.linalg.norm(sol.y[0:3, i] - rvecRefi)
    vErrList[i] = np.linalg.norm(sol.y[3:6, i] - vvecRefi)
    
    PhiRefi_flat = PhiRef_flat[i,:]
    
    Phii = np.reshape(sol.y[7:,i], (7,7))
    Phii_noJ2 = Phii[:6, :6]
    Phii_noJ2_flat = np.reshape(Phii_noJ2.T, (36,))
    
    PhiErrList[i] = np.linalg.norm(Phii_noJ2_flat - PhiRefi_flat)

# plot comparison results
fig00 = plt.figure()
ax01 = plt.subplot(311)
ax01.plot(tRef/P, rErrList)
ax01.set_ylabel('pos. err. norms, km')
plt.tick_params('x', labelbottom=False)
ax01.grid()

ax02 = plt.subplot(312)
ax02.plot(tRef/P, vErrList)
ax02.set_ylabel('vel. err. norms, km/s')
plt.tick_params('x', labelbottom=False)
ax02.grid()

ax03 = plt.subplot(313)
ax03.plot(tRef/P, PhiErrList)
ax03.set_ylabel('Phi err. norms, ~')
ax03.set_xlabel('# orbits')
ax03.grid()
fig00.tight_layout()



#%%
# =============================================================================
# Test single ODE call against MATLAB grader values
# =============================================================================
# make sure ECC = 0.01 when running this check!
zd = dyn.keplerJ2_wPhi_ODE(0, xx0vec, mu, J2, R, evalJac)
# manipulate zd to match MATLAB convention
Phid = np.reshape(zd[7:],(7,7))
Phid_flat_MATLAB = np.reshape(Phid.T, (49,))
zd_MATLAB = np.append(zd[:7], Phid_flat_MATLAB)
    

#%%
# =============================================================================
# Propagate nonlinear perturbed trajectory
# =============================================================================
del0vec = np.array([1, 0, 0, 0, 10/1e3, 0, 0])
xx0vec_pert = xx0vec[:7] + del0vec

sol_pert = solve_ivp(lambda t, yy: dyn.keplerJ2_ODE(t, yy, mu, J2, R),
                (t0, tf), xx0vec_pert, t_eval = t_eval, rtol = 1e-12, atol = 1e-12)
print(sol_pert.message)


#%%
# =============================================================================
# Propagate state using STM
# =============================================================================
deltaxx_STM = np.empty((6,len(sol.t)))
deltaxx_STM[:] = np.NaN
deltaxx_STM[:,0] = del0vec[:6]

for i, yy in enumerate(sol.y[:,:-1].T):
    Phi = np.reshape(yy[7:], (7,7))
    xxvec = Phi @ del0vec
    deltaxx_STM[:,i+1] = xxvec[:6]

#%%
# =============================================================================
# Compute delta x for both methods
# =============================================================================
deltaxx_NL = np.empty((6, len(sol.t)))
deltaxx_NL[:] = np.NaN
ddelta = np.empty(deltaxx_STM.shape)
ddelta[:] = np.NaN

for i, yy in enumerate(sol_pert.y.T):
    deltaxx_NL[:,i] = yy[:6] - sol.y[:6, i]
    ddelta[:,i] = deltaxx_STM[:,i] - deltaxx_NL[:,i]




#%%
# =============================================================================
# Plots
# =============================================================================
# plt.close('all')

fig = plt.figure()
ax = plt.subplot(111)
ax.plot(sol.t/P, np.linalg.norm(deltaxx_NL[:3,:], axis = 0))
ax.grid()
ax.set_xlabel('# orbits')
ax.set_ylabel(r'$\delta x_r$, km')
fig.tight_layout()

fig1 = plt.figure()

ax11 = plt.subplot(311)
ax11.grid()
ax11.plot(sol.t/P, deltaxx_STM[0,:], 'C0', label = 'STM')
ax11.plot(sol.t/P, deltaxx_NL[0,:], 'C1--', label = 'nonlinear')
plt.tick_params('x', labelbottom=False)
ax11.set_ylabel('x, km')

ax12 = plt.subplot(312, sharex = ax11)
ax12.grid()
ax12.plot(sol.t/P, deltaxx_STM[1,:], 'C0')
ax12.plot(sol.t/P, deltaxx_NL[1,:], 'C1--')
plt.tick_params('x', labelbottom=False)
ax12.set_ylabel('y, km')

ax13 = plt.subplot(313, sharex = ax11)
ax13.grid()
ax13.plot(sol.t/P, deltaxx_STM[2,:], 'C0')
ax13.plot(sol.t/P, deltaxx_NL[2,:], 'C1--')
ax13.set_ylabel('z, km')
ax13.set_xlabel('# orbits')
ax11.legend()
plt.tight_layout()


fig2 = plt.figure()

ax21 = plt.subplot(311)
ax21.grid()
ax21.plot(sol.t/P, deltaxx_STM[3,:], 'C0', label = 'STM')
ax21.plot(sol.t/P, deltaxx_NL[3,:], 'C1--', label = 'nonlinear')
plt.tick_params('x', labelbottom=False)
ax21.set_ylabel('dx, km/s')

ax22 = plt.subplot(312, sharex = ax21)
ax22.grid()
ax22.plot(sol.t/P, deltaxx_STM[4,:], 'C0')
ax22.plot(sol.t/P, deltaxx_NL[4,:], 'C1--')
plt.tick_params('x', labelbottom=False)
ax22.set_ylabel('dy, km/s')

ax23 = plt.subplot(313, sharex = ax21)
ax23.grid()
ax23.plot(sol.t/P, deltaxx_STM[5,:], 'C0')
ax23.plot(sol.t/P, deltaxx_NL[5,:], 'C1--')
ax23.set_ylabel('dz, km/s')
ax23.set_xlabel('# orbits')
ax21.legend()
plt.tight_layout()


fig3 = plt.figure()

ax31 = plt.subplot(211)
ax31.grid()
ax31.plot(sol.t/P, np.linalg.norm(ddelta[:3], axis = 0))
plt.tick_params('x', labelbottom=False)
ax31.set_ylabel('pos. err. norm, km')

ax32 = plt.subplot(212)
ax32.grid()
ax32.plot(sol.t/P, np.linalg.norm(ddelta[3:], axis = 0))
ax32.set_ylabel('vel. err. norm, km/s')
ax32.set_xlabel('# orbits')
plt.tight_layout()











#%%
toc = time.time()
print('execution took a total of {0:.2f} seconds'.format(toc-tic))








