# -*- coding: utf-8 -*-
"""
HW1P4.py:
    script for Stat OD HW 1 P 4
Created on Mon Jan 24 20:58:50 2022

@author: Samuel Albert
"""

import numpy as np
import pandas as pd
from scipy.stats import norm
import matplotlib.pyplot as plt
from scipy.integrate import solve_ivp

from src import dynamics as dyn
from src import measurements as mes
from src import astrodynamics as astro

plt.close('all')
# =============================================================================
# given information
# =============================================================================
# constants 
mu = 3.986004415e5
J2 = 1.08263e-3
J3 = 0 # only J2 for this problem
R = 6378
D = 86400
theta0 = 122

# stations
n = 3 # number of stations
coords = np.empty((n,2))
coords[:] = np.NaN
masks = np.empty(n)
masks[:] = np.NaN
IDs = [None]*n

# Station 1
coords[0,0] = 148.981944
coords[0,1] = -35.398333
masks[0] = 10
IDs[0] = 'Station 1'

# Station 2
coords[1,0] = 355.749444
coords[1,1] = 40.427222
masks[1] = 10
IDs[1] = 'Station 2'

# Station 3
coords[2,0] = 243.205
coords[2,1] = 35.247164
masks[2] = 10
IDs[2] = 'Station 3'

# given orbit elements
SMA = 10e3 # km
ECC = 0.001 # used for MATLAB grader
# ECC = 0.001 # for given ref. traj.
INC = 40 # deg
AOP = 80 # deg
RAAN = 40 # deg
TA0 = 0 # deg
P = 2 * np.pi * np.sqrt(SMA**3/mu)

# convert to inertial cartesian state
[r0vec, v0vec] = astro.kep2cart_TA(SMA, ECC, INC, AOP, RAAN, TA0, mu)

# # =============================================================================
# # Check function against MATLAB grader
# # =============================================================================
# t = 0
# obs = mes.compute_range_rangerate(np.append(r0vec, v0vec), coords, masks, theta0, t,
#                             R, D)
# print(obs[1,0])
# print(obs[1,1])

# =============================================================================
# Propagate orbit
# =============================================================================
t0 = 0
tf = P * 15 + 10
t_eval = np.arange(0, tf, 10)
xx0vec = np.append(np.append(r0vec, v0vec),J2)

sol = solve_ivp(lambda t, yy: dyn.keplerJ2_ODE(t, yy, mu, J2, R),
                (t0, tf), xx0vec, t_eval = t_eval,
                rtol = 1e-12, atol = 1e-12)
print(sol.message)

# =============================================================================
# Produce observations at every time step
# =============================================================================
obsList = []
IDs = ['Canberra', 'Madrid', 'Goldstone']
for i, t in enumerate(sol.t):
    mes.compute_range_rangerate(sol.y[:6,i], coords, masks, theta0, t, R, D,
                                obsList, IDs)
obsData = pd.DataFrame(obsList)
    
#%%
# =============================================================================
# Generate pseudodata from actual measurements
# =============================================================================
fTref = 8.44*1e9 # Hz
c = 299792458/1e3 # km/s

fshifts = [None]*3
RUs = [None]*3
for i in range(n):
    fshifts[i] = -2/c*fTref * obsData[obsData['ind'] == i]['rangerate']
    RUs[i] = 221/749 * fTref/c * obsData[obsData['ind'] == i]['range']

#%%
# =============================================================================
# Generate noisy range-rate measurements
# =============================================================================
noise1 = norm.rvs(loc = 0, scale = 0.5/1e6,
                  size = len(obsData[obsData['ind'] == 0]['t']))
noise2 = norm.rvs(loc = 0, scale = 0.5/1e6,
                  size = len(obsData[obsData['ind'] == 1]['t']))
noise3 = norm.rvs(loc = 0, scale = 0.5/1e6,
                  size = len(obsData[obsData['ind'] == 2]['t']))



#%%
# =============================================================================
# Plots
# =============================================================================
plt.close('all')
fig1 = plt.figure()
ax1 = plt.subplot(111)
ax1.plot(obsData[obsData['ind'] == 0]['t']/P,
         obsData[obsData['ind'] == 0]['range'], '.', label = 'Canberra') 
ax1.plot(obsData[obsData['ind'] == 1]['t']/P,
         obsData[obsData['ind'] == 1]['range'], '.', label = 'Madrid')
ax1.plot(obsData[obsData['ind'] == 2]['t']/P,
         obsData[obsData['ind'] == 2]['range'], '.', label = 'Goldstone')
ax1.grid()
ax1.legend()
ax1.set_xlabel('# orbits')
ax1.set_ylabel('range, km')

fig2 = plt.figure()
ax2 = plt.subplot(111)
ax2.plot(obsData[obsData['ind'] == 0]['t']/P,
         obsData[obsData['ind'] == 0]['rangerate'], '.', label = 'Canberra') 
ax2.plot(obsData[obsData['ind'] == 1]['t']/P,
         obsData[obsData['ind'] == 1]['rangerate'], '.', label = 'Madrid')
ax2.plot(obsData[obsData['ind'] == 2]['t']/P,
         obsData[obsData['ind'] == 2]['rangerate'], '.', label = 'Goldstone')
ax2.grid()
ax2.legend()
ax2.set_xlabel('# orbits')
ax2.set_ylabel('range-rate, km/s')

fig3 = plt.figure()
ax3 = plt.subplot(111)
ax3.plot(obsData[obsData['ind'] == 0]['t']/P,
         obsData[obsData['ind'] == 0]['EL'], '.', label = 'Canberra') 
ax3.plot(obsData[obsData['ind'] == 1]['t']/P,
         obsData[obsData['ind'] == 1]['EL'], '.', label = 'Madrid')
ax3.plot(obsData[obsData['ind'] == 2]['t']/P,
         obsData[obsData['ind'] == 2]['EL'], '.', label = 'Goldstone')
ax3.grid()
ax3.legend()
ax3.set_xlabel('# orbits')
ax3.set_ylabel('EL, deg')

fig4 = plt.figure()
ax4 = plt.subplot(111)
ax4.plot(obsData[obsData['ind'] == 0]['t']/P,
         fshifts[0], '.', label = 'Canberra')
ax4.plot(obsData[obsData['ind'] == 1]['t']/P,
         fshifts[1], '.', label = 'Madrid')
ax4.plot(obsData[obsData['ind'] == 2]['t']/P,
         fshifts[2], '.', label = 'Goldstone')
ax4.grid()
ax4.legend()
ax4.set_xlabel('# orbits')
ax4.set_ylabel('Doppler shift, Hz')
fig4.tight_layout()

fig5 = plt.figure()
ax5 = plt.subplot(111)
ax5.plot(obsData[obsData['ind'] == 0]['t']/P, RUs[0],
         '.', label = 'Canberra')
ax5.plot(obsData[obsData['ind'] == 1]['t']/P, RUs[1],
         '.', label = 'Madrid')
ax5.plot(obsData[obsData['ind'] == 2]['t']/P, RUs[2],
         '.', label = 'Goldstone')
ax5.grid()
ax5.legend()
ax5.set_xlabel('# orbits')
ax5.set_ylabel('2-way range, RU')

fig6 = plt.figure()
ax6 = plt.subplot(111)
ax6.plot(obsData[obsData['ind'] == 0]['t']/P,
         obsData[obsData['ind'] == 0]['rangerate'] + noise1, '.', label = 'Canberra') 
ax6.plot(obsData[obsData['ind'] == 1]['t']/P,
         obsData[obsData['ind'] == 1]['rangerate'] + noise2, '.', label = 'Madrid')
ax6.plot(obsData[obsData['ind'] == 2]['t']/P,
         obsData[obsData['ind'] == 2]['rangerate'] + noise3, '.', label = 'Goldstone')
ax6.grid()
ax6.legend()
ax6.set_xlabel('# orbits')
ax6.set_ylabel('range-rate, km/s')

fig7 = plt.figure()
ax7 = plt.subplot(111)
ax7.plot(obsData[obsData['ind'] == 0]['t']/P, noise1, '.', label = 'Canberra') 
ax7.plot(obsData[obsData['ind'] == 1]['t']/P, noise2, '.', label = 'Madrid')
ax7.plot(obsData[obsData['ind'] == 2]['t']/P, noise3, '.', label = 'Goldstone')
ax7.grid()
ax7.legend()
ax7.set_xlabel('# orbits')
ax7.set_ylabel('range-rate noise, km/s')













