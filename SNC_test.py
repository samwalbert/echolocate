# -*- coding: utf-8 -*-
"""
SNC_test.py:
    test for SNC code for Stat OD HW3 P1
Created on Fri Feb 18 15:57:34 2022

@author: Samuel Albert
"""

import numpy as np

from src import filters
from src import astrodynamics as astro

#%% form example ON DCM
rvec = np.array([5, 1, 1])
vvec = np.array([2, 2, 0])
ON = astro.getON(rvec, vvec)

#%% test function evaluations
Dt = 0.1
# Qct = 10 * np.eye(3)
Qct = np.diag([10, 15, 5])

evalQQ_N = filters.getQ_HW3_inertial()
QQ_N = evalQQ_N(Dt, Qct, ON)
evalQQ_O = filters.getQ_HW3_RIC()
QQ_O = evalQQ_O(Dt, Qct, ON)


print(np.linalg.norm(QQ_N - QQ_O))

